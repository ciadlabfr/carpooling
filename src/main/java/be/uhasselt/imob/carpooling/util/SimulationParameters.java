/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.util;

import java.io.File;
import java.net.URL;

/** Frame that permits to enter the simulation parameters.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public interface SimulationParameters {
	/** Path of the shape file of the road network from IMOB.
	 */
	public static final String IMOB_ROAD_NETWORK = "be/uhasselt/imob/carpooling/datainput/LINKS_VLAANDEREN_2007_IMOB3_5_SZ8.shp"; //$NON-NLS-1$

	/** Path of the shape file of the road network from IRTES-SET.
	 */
	public static final String IRTESSET_ROAD_NETWORK = "be/uhasselt/imob/carpooling/datainput/Belfort.shp"; //$NON-NLS-1$

	/** Path of the shape file of the road network from IRTES-SET.
	 */
	public static final String TEST_ROAD_NETWORK = "be/uhasselt/imob/carpooling/datainput/Test.shp"; //$NON-NLS-1$

	/** Path of the CSV file of the FEATHER data.
	 */
	public static final String FEATHER_DATA = "be/uhasselt/imob/carpooling/datainput/feather.csv"; //$NON-NLS-1$

	/** Number of people in the simulation.
	 */
	public static final int NB_PEOPLE = 1;

	/** Average fuel cost.
	 */
	public static final float FUEL_COST = 1.23f;

	/** Simulation step.
	 */
	public static final float SIMULATION_STEP = 30f;

	/** Distance under which the vehicles are assumed to be arrived in meters.
	 */
	public static final int ARRIVING_DISTANCE = 10;

	/** Cruise speed.
	 */
	public static final float CRUISE_SPEED = 45;

	/** Day count.
	 */
	public static final int DAY_COUNT = 1;

	/** Ego-centric factor for negociations
	 */
	public static final float EGO_CENTRIC_FACTOR = .5f;

	/** Replies the path to the road network.
	 * 
	 * @return the path to the road network.
	 */
	public URL getRoadNetwork();
	
	/** Replies if the dBase files should be read in parallel to the road network files.
	 * 
	 * @return <code>true</code> if the dBase file should be read; <code>false</code> if
	 * it must be ignored.
	 */
	public boolean getReadDBase();

	/** Replies the path to the FEATHETR CSV file.
	 * 
	 * @return the path to the FEATHETR CSV file.
	 */
	public URL getFeatherCSV();

	/** Replies the size of the population.
	 * 
	 * @return the size of the population.
	 */
	public int getPopulationSize();

	/** Replies the size of one simulation step during the driving simulation.
	 * 
	 * @return the size of one driving simulation step (in seconds).
	 */
	public float getDrivingSimulationStep();

	/** Replies the average cost of fuel.
	 * 
	 * @return the average cost of fuel.
	 */
	public float getAverageFuelCost();

	/** Replies the distance under which the vehicles are assumed
	 * to be arrived at a given position.
	 * @return the distance in meters.
	 */
	public float getVehicleArrivingDistance();
	
	/** Replies the directory where to put the outputs.
	 * 
	 * @return the output directory.
	 */
	public File getOutputDirectory();

	/** Replies the cruise speed for the vehicles.
	 * 
	 * @return the cruise speed in km/h.
	 */
	public float getCruiseSpeed();

	/** Replies the number of days to simulate.
	 * 
	 * @return the number of days.
	 */
	public int getNumberOfDays();
	
	/** Replies a factor that is representing the
	 * self-importance of an individual when it is negociating.
	 * @return the factor; {@code 0} if the negociation partners
	 * are more important, ie the individual is altruist;
	 * {@code 1} if the individual itself is
	 * always more important, ie. the individual is egocentric.
	 */
	public float getEgocentricFactor();

	/** Replies if the user interface should be displayed.
	 * 
	 * @return <code>true</code> if the user interface should be
	 * displayed, <code>false</code> otherwise.
	 */
	public boolean isUIDisplayed();

}
