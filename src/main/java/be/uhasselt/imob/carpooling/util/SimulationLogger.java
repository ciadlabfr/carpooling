/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.arakhne.afc.vmutil.FileSystem;
import org.janusproject.kernel.address.AgentAddress;

import be.uhasselt.imob.carpooling.kernel.time.DayOfWeek;
import be.uhasselt.imob.carpooling.kernel.time.Time;
import be.uhasselt.imob.carpooling.schedule.TimeWindow;
import be.uhasselt.imob.carpooling.schedule.Trip;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.math.MeasureUnitUtil;

/** Utility functions to store data.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class SimulationLogger {

	private static enum MainFileColumns {
		TIME("t"), //$NON-NLS-1$
		ID("Agent ID"), //$NON-NLS-1$
		ISDRIVER("Is driver"), //$NON-NLS-1$
		MODE("Transport mode"), //$NON-NLS-1$
		WANT_CARPOOLING("Want carpooling?"), //$NON-NLS-1$
		CARPOOL_NO_PARTNER("No carpooling partner"), //$NON-NLS-1$
		CARPOOL_NEGOCIATION_FAILURE("Negociation failure"), //$NON-NLS-1$
		PARTNERS("Carpooling partners"), //$NON-NLS-1$
		TRIP("Trip"), //$NON-NLS-1$
		TRIP_WINDOW("Negociated Window"), //$NON-NLS-1$
		TRAVEL_TIME("Travel time"); //$NON-NLS-1$

		public final String label;

		private MainFileColumns(String label) {
			this.label = label;
		}
	}
	
	private static enum ExecutionLogFileColumns {
		TIME("t"), //$NON-NLS-1$
		DURATION_NANOS("Duration (ns)"), //$NON-NLS-1$
		DURATION_MILLIS("Duration (ms)"); //$NON-NLS-1$

		public final String label;

		private ExecutionLogFileColumns(String label) {
			this.label = label;
		}
	}

	private static enum SynthesisFileColumns {
		DAY("Day"), //$NON-NLS-1$
		HOUR("Hour"), //$NON-NLS-1$
		DRIVERS("Drivers"), //$NON-NLS-1$
		PASSENGERS("Passengers"), //$NON-NLS-1$
		TRAVELLERS("Travellers"), //$NON-NLS-1$
		CARPOOL_FAILURES("Carpool failures"), //$NON-NLS-1$
		CARPOOLS("Carpools"); //$NON-NLS-1$

		public final String label;

		private SynthesisFileColumns(String label) {
			this.label = label;
		}
	}

	private static enum RoadFileColumns {
		VEHICLE("Vehicle"), //$NON-NLS-1$
		DAY("Day"), //$NON-NLS-1$
		HOUR("Hour"), //$NON-NLS-1$
		ROAD("Road"); //$NON-NLS-1$

		public final String label;

		private RoadFileColumns(String label) {
			this.label = label;
		}
	}

	private static File outputDirectory = null;
	private static PrintWriter writer = null;
	private static PrintWriter errorWriter = null;
	private static PrintWriter executionLog = null;

	private static final Map<DayOfWeek,int[][]> travellers = new TreeMap<DayOfWeek,int[][]>();
	private static final Map<String,Map<Time,RoadSegment>> roads = new TreeMap<String,Map<Time,RoadSegment>>();

	/** Initialize the simulation logger.
	 * 
	 * @param params are the parameters of the simulation
	 * @throws IOException 
	 */
	public static void initialize(SimulationParameters params) throws IOException {
		synchronized(SimulationLogger.class) {
			File topOutputDir = params.getOutputDirectory();
			makeOutputDirectory(topOutputDir);
			if (writer!=null) {
				writer.close();
				writer = null;
			}
			if (errorWriter!=null) {
				errorWriter.close();
				errorWriter = null;
			}
			File descFile = new File(outputDirectory, "scenario.txt");  //$NON-NLS-1$
			descFile.getParentFile().mkdirs();
			PrintWriter pw = new PrintWriter(new FileWriter(descFile));
			try {
				pw.print("Average fuel cost (currency): "); //$NON-NLS-1$
				pw.println(params.getAverageFuelCost());
				pw.print("Cruise speed (km/h): "); //$NON-NLS-1$
				pw.println(params.getCruiseSpeed());
				pw.print("Microsimulation step size (s): "); //$NON-NLS-1$
				pw.println(params.getDrivingSimulationStep());
				pw.print("Number of simulated days (#): "); //$NON-NLS-1$
				pw.println(params.getNumberOfDays());
				pw.print("Population (#): "); //$NON-NLS-1$
				pw.println(params.getPopulationSize());
				pw.print("Fearther file: "); //$NON-NLS-1$
				pw.println(params.getFeatherCSV());
				pw.print("Roads: "); //$NON-NLS-1$
				pw.println(params.getRoadNetwork());
				pw.print("Egocentric factor (0: alstruist, 1:egocentric): "); //$NON-NLS-1$
				pw.println(params.getEgocentricFactor());
			}
			finally {
				pw.close();
			}
		}
	}

	/** Close the simulation logger.
	 * @throws IOException 
	 */
	public static void close() throws IOException {
		synchronized(SimulationLogger.class) {
			if (writer!=null) {
				writer.close();
				writer = null;
			}
			if (errorWriter!=null) {
				errorWriter.close();
				errorWriter = null;
			}
			if (executionLog!=null) {
				executionLog.close();
				executionLog = null;
			}

			File output = new File(outputDirectory, "synthesis.csv"); //$NON-NLS-1$

			PrintWriter pw = new PrintWriter(new FileWriter(output));
			try {
				int i = 0;
				for(SynthesisFileColumns col : SynthesisFileColumns.values()) {
					if (i>0) pw.print("\t"); //$NON-NLS-1$
					pw.print(col.label);
					++i;
				}
				for(TransportMode mode : TransportMode.values()) {
					pw.print("\t"); //$NON-NLS-1$
					pw.print(mode.name());
				}
				pw.println();
				for(Entry<DayOfWeek,int[][]> e : travellers.entrySet()) {
					int[][] t = e.getValue();
					for(i=0; i<96; ++i) {
						String h = formatInt(i/4,2)+":"+formatInt((i%4)*15,2); //$NON-NLS-1$
						pw.print(e.getKey().toString());
						pw.print("\t"); //$NON-NLS-1$
						pw.print(h);
						int cc = 2;
						for(; cc<SynthesisFileColumns.values().length; ++cc) {
							pw.print("\t"); //$NON-NLS-1$
							pw.print(t[i][cc]);
						}
						for(int j=0; j<TransportMode.values().length; ++j) {
							pw.print("\t"); //$NON-NLS-1$
							pw.print(t[i][cc]);
							++cc;
						}
						pw.println();
					}
				}
			}
			finally {
				pw.close();
			}

			output = new File(outputDirectory, "roads.csv"); //$NON-NLS-1$
			pw = new PrintWriter(new FileWriter(output));
			try {
				int i = 0;
				for(RoadFileColumns col : RoadFileColumns.values()) {
					if (i>0) pw.print("\t"); //$NON-NLS-1$
					pw.print(col.label);
					++i;
				}
				pw.println();
				for(Entry<String,Map<Time,RoadSegment>> entry1 : roads.entrySet()) {
					String vehicle = entry1.getKey();
					for(Entry<Time,RoadSegment> entry2 : entry1.getValue().entrySet()) {
						pw.print(vehicle);
						pw.print("\t"); //$NON-NLS-1$
						pw.print(entry2.getKey().getDay().name());
						pw.print("\t"); //$NON-NLS-1$
						pw.print(formatInt(entry2.getKey().getHour(), 2)+":"+formatInt(entry2.getKey().getMinute(), 2)); //$NON-NLS-1$
						pw.print("\t"); //$NON-NLS-1$
						pw.print(entry2.getValue().getGeoId().toString());
						pw.println();
					}
				}
			}
			finally {
				pw.close();
			}

			outputDirectory = null;
			travellers.clear();
			roads.clear();
		}
	}

	private static String formatInt(int v, int length) {
		StringBuilder b = new StringBuilder(Integer.toString(v));
		while (b.length()<length) {
			b.insert(0, "0"); //$NON-NLS-1$
		}
		return b.toString();
	}

	private static void makeOutputDirectory(File topDirectory)  {
		DateFormat df = new SimpleDateFormat("yyyyMMdd"); //$NON-NLS-1$
		DateFormat hf = new SimpleDateFormat("HHmmss"); //$NON-NLS-1$
		Date now = new Date();
		outputDirectory = new File(topDirectory, df.format(now) + "_" + hf.format(now)); //$NON-NLS-1$
		outputDirectory.mkdirs();
	}
	
	private static PrintWriter ensureWriter() throws IOException {
		synchronized(SimulationLogger.class) {
			if (writer==null) {
				if (outputDirectory==null) {
					outputDirectory = FileSystem.getUserHomeDirectory();
				}
				File output = new File(outputDirectory, "simulation.csv"); //$NON-NLS-1$
				FileWriter fw = new FileWriter(output);
				writer = new PrintWriter(fw);
				int i=0;
				for(MainFileColumns c : MainFileColumns.values()) {
					if (i>0) writer.print("\t"); //$NON-NLS-1$
					writer.print(c.label);
					++i;
				}
				writer.println();
				writer.flush();
			}
			return writer;
		}
	}

	private static PrintWriter ensureExecutionLogWriter() throws IOException {
		synchronized(SimulationLogger.class) {
			if (executionLog==null) {
				if (outputDirectory==null) {
					outputDirectory = FileSystem.getUserHomeDirectory();
				}
				File output = new File(outputDirectory, "executionLog.csv"); //$NON-NLS-1$
				FileWriter fw = new FileWriter(output);
				executionLog = new PrintWriter(fw);
				int i=0;
				for(ExecutionLogFileColumns c : ExecutionLogFileColumns.values()) {
					if (i>0) executionLog.print("\t"); //$NON-NLS-1$
					executionLog.print(c.label);
					++i;
				}
				executionLog.println();
				executionLog.flush();
			}
			return executionLog;
		}
	}

	private static PrintWriter ensureErrorWriter() throws IOException {
		synchronized(SimulationLogger.class) {
			if (errorWriter==null) {
				if (outputDirectory==null) {
					outputDirectory = FileSystem.getUserHomeDirectory();
				}
				File output = new File(outputDirectory, "errors.csv"); //$NON-NLS-1$
				FileWriter fw = new FileWriter(output);
				errorWriter = new PrintWriter(fw);
				int i=0;
				for(MainFileColumns c : MainFileColumns.values()) {
					if (i>0) errorWriter.print("\t"); //$NON-NLS-1$
					errorWriter.print(c.label);
					++i;
				}
				errorWriter.println();
				errorWriter.flush();
			}
			return errorWriter;
		}
	}

	/** Error Log.
	 * 
	 * @param clock is the current time.
	 * @param address is the address of the agent.
	 * @param isDriver indicates if the agent is a driver.
	 * @param mode is the transport mode selected by the agent.
	 * @param wantCarpooling indicates if the agent has tries to carpool.
	 * @param isNoPartnerForCarpooling indicates if carpooling has failed due to lake of partners.
	 * @param isNegociationFailure indicates if carpooling has failed due to negociation failure.
	 * @param partners is the number of partners for the trip.
	 * @param trip is the trip.
	 * @param tripWindow is the trip window.
	 * @param travelTime is the travel time.
	 */
	public static void logError(
			Time clock,
			AgentAddress address,
			boolean isDriver,
			TransportMode mode,
			boolean wantCarpooling,
			boolean isNoPartnerForCarpooling,
			boolean isNegociationFailure,
			int partners,
			Trip trip,
			TimeWindow tripWindow,
			double travelTime) {
		try {
			logIn(
					ensureErrorWriter(),
					clock, address, isDriver, mode,
					wantCarpooling,
					isNoPartnerForCarpooling,
					isNegociationFailure,
					partners,
					trip, tripWindow,
					travelTime);
		}
		catch(Throwable e) {
			throw new Error(e);
		}
	}

	/** Standard Log.
	 * 
	 * @param clock is the current time.
	 * @param address is the address of the agent.
	 * @param isDriver indicates if the agent is a driver.
	 * @param mode is the transport mode selected by the agent.
	 * @param wantCarpooling indicates if the agent has tries to carpool.
	 * @param isNoPartnerForCarpooling indicates if carpooling has failed due to lake of partners.
	 * @param isNegociationFailure indicates if carpooling has failed due to negociation failure.
	 * @param partners is the number of partners for the trip.
	 * @param trip is the trip.
	 * @param tripWindow is the trip window.
	 * @param travelTime is the travel time.
	 */
	public static void log(
			Time clock,
			AgentAddress address,
			boolean isDriver,
			TransportMode mode,
			boolean wantCarpooling,
			boolean isNoPartnerForCarpooling,
			boolean isNegociationFailure,
			int partners,
			Trip trip,
			TimeWindow tripWindow,
			double travelTime) {
		try {
			logIn(
					ensureWriter(),
					clock, address, isDriver, mode,
					wantCarpooling,
					isNoPartnerForCarpooling,
					isNegociationFailure,
					partners,
					trip, tripWindow,
					travelTime);
		}
		catch(Throwable e) {
			throw new Error(e);
		}
	}

	/** Log.
	 * 
	 * @param theWriter
	 * @param clock is the current time.
	 * @param address is the address of the agent.
	 * @param isDriver indicates if the agent is a driver.
	 * @param mode is the transport mode selected by the agent.
	 * @param wantCarpooling indicates if the agent has tries to carpool.
	 * @param isNoPartnerForCarpooling indicates if carpooling has failed due to lake of partners.
	 * @param isNegociationFailure indicates if carpooling has failed due to negociation failure.
	 * @param partners is the number of partners for the trip.
	 * @param trip is the trip.
	 * @param tripWindow is the trip window.
	 * @param travelTime is the travel time.
	 */
	private static void logIn(
			PrintWriter theWriter,
			Time clock,
			AgentAddress address,
			boolean isDriver,
			TransportMode mode,
			boolean wantCarpooling,
			boolean isNoPartnerForCarpooling,
			boolean isNegociationFailure,
			int partners,
			Trip trip,
			TimeWindow tripWindow,
			double travelTime) {
		synchronized(SimulationLogger.class) {
			String[] values = new String[MainFileColumns.values().length];
			
			assert(mode!=null);
			
			assert(	mode!=TransportMode.CARPOOLING || partners>0);

			values[MainFileColumns.TIME.ordinal()] = clock.toString();
			values[MainFileColumns.ID.ordinal()] = address.toString(); 
			values[MainFileColumns.ISDRIVER.ordinal()] = Boolean.toString(isDriver);
			values[MainFileColumns.MODE.ordinal()] = mode.name();
			values[MainFileColumns.PARTNERS.ordinal()] = Integer.toString(partners);
			values[MainFileColumns.TRIP.ordinal()] = (trip!=null ? trip.toString() : ""); //$NON-NLS-1$
			values[MainFileColumns.TRIP_WINDOW.ordinal()] = (tripWindow!=null ? tripWindow.toString() : ""); //$NON-NLS-1$
			values[MainFileColumns.TRAVEL_TIME.ordinal()] = Double.toString(travelTime);
			values[MainFileColumns.CARPOOL_NEGOCIATION_FAILURE.ordinal()] = Boolean.toString(isNegociationFailure);
			values[MainFileColumns.CARPOOL_NO_PARTNER.ordinal()] = Boolean.toString(isNoPartnerForCarpooling);
			values[MainFileColumns.WANT_CARPOOLING.ordinal()] = Boolean.toString(wantCarpooling);

			for(int i=0; i<values.length; ++i) {
				if (i>0) theWriter.print("\t"); //$NON-NLS-1$
				theWriter.print(values[i]);
			}
			theWriter.println();
			theWriter.flush();

			TimeWindow tw = tripWindow;
			if (tw==null && trip!=null) {
				tw = trip.getTimeWindow();
			}
			if (tw!=null) {
				Time t = tw.preferredStart;
				int m = t.getMinute();
				if (m<15) m = 0;
				else if (m<30) m = 15;
				else if (m<45) m = 30;
				else m = 45;
				t = new Time(t.getDay(), t.getHour(), m);

				int[][] data = travellers.get(t.getDay());
				if (data==null) {
					data = new int[96][
					                   SynthesisFileColumns.values().length
					                   +TransportMode.values().length];
					travellers.put(t.getDay(), data);
				}
				
				while (!t.after(tw.preferredEnd)) {
					int index = t.getHour()*4+t.getMinute()/15;
					data[index][SynthesisFileColumns.DRIVERS.ordinal()] += 1;
					data[index][SynthesisFileColumns.PASSENGERS.ordinal()] += partners;
					data[index][SynthesisFileColumns.TRAVELLERS.ordinal()] += 1+partners;
					data[index][SynthesisFileColumns.CARPOOLS.ordinal()] += (mode==TransportMode.CARPOOLING) ? 1 : 0;
					data[index][SynthesisFileColumns.CARPOOL_FAILURES.ordinal()] += (isNegociationFailure) ? 1 : 0;
					data[index][mode.ordinal()+SynthesisFileColumns.values().length] += 1;
					t = t.add(15);
				}
			}
		}
	}
	
	/** Log the entry of a vehicle on the given road segment at the given time.
	 * 
	 * @param id is the identifier of the vehicle.
	 * @param segment is the road segment.
	 * @param entryTime is the entry time.
	 */
	public static void logPath(String id, RoadSegment segment, Time entryTime) {
		synchronized(SimulationLogger.class) {
			Map<Time,RoadSegment> segments = roads.get(id);
			if (segments==null) {
				segments = new TreeMap<Time,RoadSegment>();
				roads.put(id, segments);
			}
			segments.put(entryTime, segment);
		}
	}
	
	/** Log the duration of the execution of a step of the simulation.
	 * 
	 * @param t is the simulation time for which the duration was measured.
	 * @param duration is the duration in nanoseconds
	 */
	public static void logSimulationDuration(Time t, long duration) {
		try {
			PrintWriter theWriter = ensureExecutionLogWriter();
			String[] values = new String[ExecutionLogFileColumns.values().length];
			
			values[ExecutionLogFileColumns.TIME.ordinal()] = t.toString();
			values[ExecutionLogFileColumns.DURATION_NANOS.ordinal()] = Long.toString(duration);
			values[ExecutionLogFileColumns.DURATION_MILLIS.ordinal()] = Double.toString(
					MeasureUnitUtil.nano2milli(duration));
			
			for(int i=0; i<values.length; ++i) {
				if (i>0) theWriter.print("\t"); //$NON-NLS-1$
				theWriter.print(values[i]);
			}
			theWriter.println();
			theWriter.flush();
		}
		catch (IOException e) {
			throw new Error(e);
		}
		
	}

}
