/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.util;

/** Type of transport mean.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public enum TransportMode {

	/** Public Transport.
	 */
	PUBLIC_TRANSPORT,
	
	/** Single Vehicle.
	 */
	SOV,
	
	/** Carpooling.
	 */
	CARPOOLING;
	
	/** Replies if the mode concerns a car.
	 * 
	 * @return <code>true</code> if the mode concerns a car;
	 * otherwise <code>false</code>.
	 */
	public boolean isCarMode() {
		return this==SOV || this==CARPOOLING;
	}

}
