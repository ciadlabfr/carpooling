/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.schedule;

import java.io.Serializable;

import be.uhasselt.imob.carpooling.environment.Location;
import be.uhasselt.imob.carpooling.kernel.time.Time;

/** An episode.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class Episode implements Cloneable, Serializable {

	private static final long serialVersionUID = 7451017014939315456L;

	private final Trip trip;
	private final Activity activity;
	
	/**
	 * @param trip
	 * @param activity
	 */
	public Episode(Trip trip, Activity activity) {
		assert(trip!=null);
		assert(activity!=null);
		this.trip = trip;
		this.activity = activity;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Episode clone() {
		return new Episode(
				this.trip.clone(),
				this.activity.clone());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj==this) return true;
		if (obj instanceof Episode) {
			Episode e = (Episode)obj;
			return (this.activity.equals(e.activity))
					&&
					(this.trip.equals(e.trip));
		}
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int h = 1;
		h = h * 37 + this.activity.hashCode();
		h = h * 37 + this.trip.hashCode();
		return h;
	}
	
	/** Replies the trip in the episode.
	 * 
	 * @return the trip.
	 */
	public Trip getTrip() {
		return this.trip;
	}
	
	/** Replies the activity of the episode.
	 * 
	 * @return the activity.
	 */
	public Activity getActivity() {
		return this.activity;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return this.trip.toString()
				+":" //$NON-NLS-1$
				+this.activity.toString();
	}
	
	/** Replies the start time of the episode.
	 * 
	 * @return the start time of the episode; or <code>null</code>
	 * if the associated trip has no start time.
	 */
	public Time getStartTime() {
		return this.trip.getStartTime();
	}
	
	/** Replies the end time of the episode.
	 * 
	 * @return the end time of the episode; or <code>null</code>
	 * if the associated trip has no start time.
	 */
	public Time getEndTime() {
		return this.trip.getEndTime();
	}

	/** Replies the start location of the trip.
	 * 
	 * @return the start location of the trip; or <code>null</code>
	 * if the trip has no source.
	 */
	public Location getSource() {
		return this.trip.getSource();
	}
	
	/** Replies the end location of the trip.
	 * 
	 * @return the end location of the trip; or <code>null</code>
	 * if the trip has no destination.
	 */
	public Location getDestination() {
		return this.trip.getDestination();
	}

}
