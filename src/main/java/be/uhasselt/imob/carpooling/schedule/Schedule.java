/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.schedule;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.janusproject.kernel.address.AgentAddress;

/** This class provides an implementation
 * of a schedule for the agents.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class Schedule implements Serializable, Cloneable {

	private static final long serialVersionUID = -651304124566131422L;
	
	private AgentAddress owner;
	private List<Episode> episodes = new LinkedList<Episode>();
	
	/**
	 * @param owner
	 */
	public Schedule(AgentAddress owner) {
		this.owner = owner;
	}
	
	/** Replies the owner of the schedule.
	 * 
	 * @return the owner of the schedule.
	 */
	public AgentAddress getOwner() {
		return this.owner;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Schedule clone() {
		try {
			Schedule clone = (Schedule)super.clone();
			clone.episodes = new LinkedList<Episode>();
			clone.episodes.addAll(this.episodes);
			return clone;
		}
		catch (CloneNotSupportedException e) {
			throw new Error(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj==this) return true;
		if (obj instanceof Schedule) {
			Schedule s = (Schedule)obj;
			if (s.size()==size()) {
				for(int i=0; i<this.episodes.size(); ++i) {
					Episode e1 = this.episodes.get(i);
					Episode e2 = s.get(i);
					if (e1==null || e2==null || !e1.equals(e2)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int h = 1;
		for(Episode e : this.episodes) {
			h = h * 37 + e.hashCode();
		}
		return h;
	}
	
	/** Replies the sequence of episodes in the schedule.
	 * 
	 * @return the sequence of episodes.
	 */
	public List<Episode> getEpisodes() {
		return Collections.unmodifiableList(this.episodes);
	}
	
	/** Replies the number of episodes in this schedule.
	 * 
	 * @return the number of episodes in this schedule.
	 */
	public int size() {
		return this.episodes.size();
	}
	
	/** Replies the episodes at the given index.
	 * 
	 * @param index
	 * @return the episode at the given index, never <code>null</code>.
	 */
	public Episode get(int index) {
		return this.episodes.get(index);
	}
	
	/** Replies the index if the given episode in this
	 * schedule.
	 * 
	 * @param episode
	 * @return the index; or {@code -1} if the episode
	 * is not inside this schedule.
	 */
	public int indexOf(Episode episode) {
		return this.episodes.indexOf(episode);
	}
	
	/** Add the given episode at the end of the
	 * schedule.
	 * The new episode is added iff:<ul>
	 * <li>the end time of the existing last episode is equal to
	 * the start time of the new episode; and</li>
	 * <li>the destination of the existing last episode is
	 * equal to the source of the new episode.</li>
	 * </ul>
	 * 
	 * @param e
	 * @return <code>true</code> if the episode was successfully
	 * added; <code>false</code> if the start time of the new episode
	 * is not marching the end time of the previous episode in
	 * the trip.
	 */
	public boolean add(Episode e) {
		assert(e!=null);
		if (this.episodes.isEmpty()) {
			return this.episodes.add(e);
		}
		Episode lastEpisode = this.episodes.get(this.episodes.size()-1);
		assert(lastEpisode!=null);
		if (lastEpisode.getEndTime().equals(e.getStartTime())
			&&
			lastEpisode.getDestination().equals(e.getSource())) {
			if (this.episodes.add(e)) {
				return true;
			}
		}
		return false;
	}

	/** Remove the given episode and all
	 * the episodes that are after <var>e</var>.
	 * 
	 * @param e is the first episode to remove.
	 * @return the number of episodes that were removed.
	 */
	public int remove(Episode e) {
		assert(e!=null);
		int count = 0;
		boolean found = false;
		Iterator<Episode> it = this.episodes.iterator();
		while (it.hasNext()) {
			Episode c = it.next();
			if (found) {
				it.remove();
				++count;
			}
			else if (e.equals(c)) {
				found = true;
				it.remove();
				++count;
			}
		}
		return count;
	}
	
	/** Remove all the trip components.
	 */
	public void clear() {
		this.episodes.clear();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("{{"); //$NON-NLS-1$
		for(Episode e : this.episodes) {
			if (b.length()>2) {
				b.append(","); //$NON-NLS-1$
			}
			b.append(e.toString());
		}
		b.append("}}"); //$NON-NLS-1$
		return b.toString();
	}

}
