/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.schedule;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point2d;

import be.uhasselt.imob.carpooling.environment.Location;
import be.uhasselt.imob.carpooling.kernel.time.Time;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.gis.road.primitive.RoadNetwork;

/** A trip.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class Trip implements Cloneable, Serializable, Comparable<Trip> {

	private static final long serialVersionUID = -1786164490903147317L;

	/** Distance under which two locations are assumed to be closed.
	 */
	public static final float MAX_DISTANCE = 1000f;
	
	private transient List<TripComponent> components = new LinkedList<TripComponent>();
	
	private int timeError = 0;

	private transient SoftReference<RoadPath> path = null;
	
	/**
	 */
	public Trip() {
		//
	}
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
		
		out.writeInt(this.components.size());
		for(TripComponent c : this.components) {
			out.writeObject(c);
		}
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		this.components.clear();
		this.path = null;

		in.defaultReadObject();

		int nbComponents = in.readInt();
		for(int i=0; i<nbComponents; ++i) {
			Object o = in.readObject();
			if (o instanceof TripComponent) {
				this.components.add((TripComponent)o);
			}
			else {
				throw new IOException("illegal type"); //$NON-NLS-1$
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Trip clone() {
		try {
			Trip clone = (Trip)super.clone();
			clone.components = new LinkedList<TripComponent>();
			for(TripComponent c : this.components) {
				clone.components.add(c.clone());
			}
			if (this.path!=null) {
				RoadPath p = this.path.get();
				clone.path = (p==null) ? null : new SoftReference<RoadPath>(p.clone());
			}
			return clone;
		}
		catch (CloneNotSupportedException e) {
			throw new Error(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj==this) return true;
		if (obj instanceof Trip) {
			Trip t = (Trip)obj;
			if (t.size()==size()) {
				for(int i=0; i<this.components.size(); ++i) {
					TripComponent c1 = this.components.get(i);
					TripComponent c2 = t.get(i);
					if (c1==null || c2==null || !c1.equals(c2)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int h = 1;
		for(TripComponent c : this.components) {
			h = h * 37 + c.hashCode();
		}
		return h;
	}
	
	/** Replies the sequence of trip components in this trip.
	 * 
	 * @return the sequence of trip components.
	 */
	public List<TripComponent> getTripComponents() {
		return Collections.unmodifiableList(this.components);
	}
	
	/** Replies the number of components in this trip.
	 * 
	 * @return the number of components in this trip.
	 */
	public int size() {
		return this.components.size();
	}
	
	/** Replies the trip at the given rank.
	 * 
	 * @param rank
	 * @return the trip at the given rank, never <code>null</code>.
	 */
	public TripComponent get(int rank) {
		return this.components.get(rank);
	}
	
	/** Replies the rank if the given component in this
	 * trip.
	 * 
	 * @param component
	 * @return the rank; or {@code -1} if the component
	 * is not inside this trip.
	 */
	public int rankOf(TripComponent component) {
		return this.components.indexOf(component);
	}
	
	/** Add the given trip component at the end of the
	 * trip.
	 * The new trip component is added iff:<ul>
	 * <li>the end time of the existing last component is equal to
	 * the start time of the new component; and</li>
	 * <li>the destination of the existing last component is
	 * equal to the source destination of the new component.</li>
	 * </ul>
	 * 
	 * @param t
	 * @return <code>true</code> if the component was successfully
	 * added; <code>false</code> if the start time of the new trip
	 * is not marching the end time of the previous component in
	 * the trip.
	 */
	public boolean add(TripComponent t) {
		assert(t!=null);
		if (this.components.isEmpty()) {
			return this.components.add(t);
		}
		TripComponent lastComponent = this.components.get(this.components.size()-1);
		assert(lastComponent!=null);
		if (lastComponent.getEndTime().equals(t.getStartTime())
			&&
			lastComponent.getDestination().equals(t.getSource())) {
			if (this.components.add(t)) {
				this.path = null;
				return true;
			}
		}
		return false;
	}

	/** Remove the given trip component and all
	 * the trip components that are after <var>t</var>.
	 * 
	 * @param t is the first trip component to remove.
	 * @return the number of trip components that were removed.
	 */
	public int remove(TripComponent t) {
		assert(t!=null);
		int count = 0;
		boolean found = false;
		Iterator<TripComponent> it = this.components.iterator();
		while (it.hasNext()) {
			TripComponent c = it.next();
			if (found) {
				it.remove();
				++count;
			}
			else if (t.equals(c)) {
				found = true;
				it.remove();
				++count;
			}
		}
		if (count>0) {
			this.path = null;
		}
		return count;
	}
	
	/** Remove all the trip components.
	 */
	public void clear() {
		this.components.clear();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("{"); //$NON-NLS-1$
		for(TripComponent c : this.components) {
			if (b.length()>1) {
				b.append(","); //$NON-NLS-1$
			}
			b.append(c.toString());
		}
		b.append("}"); //$NON-NLS-1$
		return b.toString();
	}
	
	/** Replies the start time of the trip.
	 * 
	 * @return the start time of the trip; or <code>null</code>
	 * if this trip has no component.
	 */
	public Time getStartTime() {
		if (this.components.isEmpty()) return null;
		TripComponent c = this.components.get(0);
		assert(c!=null);
		return c.getStartTime();
	}
	
	/** Replies the end time of the trip.
	 * 
	 * @return the end time of the trip; or <code>null</code>
	 * if this trip has no component.
	 */
	public Time getEndTime() {
		if (this.components.isEmpty()) return null;
		TripComponent c = this.components.get(this.components.size()-1);
		assert(c!=null);
		return c.getEndTime();
	}

	/** Replies the start location of the trip.
	 * 
	 * @return the start location of the trip; or <code>null</code>
	 * if this trip has no component.
	 */
	public Location getSource() {
		if (this.components.isEmpty()) return null;
		TripComponent c = this.components.get(0);
		assert(c!=null);
		return c.getSource();
	}
	
	/** Replies the end location of the trip.
	 * 
	 * @return the end location of the trip; or <code>null</code>
	 * if this trip has no component.
	 */
	public Location getDestination() {
		if (this.components.isEmpty()) return null;
		TripComponent c = this.components.get(this.components.size()-1);
		assert(c!=null);
		return c.getDestination();
	}
	
	/** Set the error in the time window of the trip.
	 * 
	 * @param error is the error in minutes
	 */
	public void setTimeError(int error) {
		this.timeError = Math.max(error, 0);
	}
	
	/** Replies the time window of the trip.
	 * 
	 * @return the time window or <code>null</code> if the trip is empty.
	 */
	public TimeWindow getTimeWindow() {
		if (this.components.isEmpty()) return null;
		Time start = this.components.get(0).getStartTime();
		Time end = this.components.get(this.components.size()-1).getEndTime();
		return new TimeWindow(
				start, end,
				start.add(-this.timeError), end.add(this.timeError));
	}

	/** {@inheritDoc}
	 */
	@Override
	public int compareTo(Trip o) {
		if (o==this) return 0;
		if (o==null) return Integer.MAX_VALUE;
		Time a = getStartTime();
		Time b = o.getStartTime();
		int cmp = a.compareTo(b);
		if (cmp!=0) return cmp;
		a = getEndTime();
		b = o.getEndTime();
		cmp = a.compareTo(b);
		if (cmp!=0) return cmp;
		cmp = size() - o.size();
		if (cmp!=0) return cmp;
		Iterator<TripComponent> i1 = getTripComponents().iterator();
		Iterator<TripComponent> i2 = o.getTripComponents().iterator();
		while (i1.hasNext() && i2.hasNext()) {
			TripComponent t1 = i1.next();
			TripComponent t2 = i2.next();
			cmp = t1.compareTo(t2);
			if (cmp!=0) return cmp;
		}
		return 0;
	}
	
	/** Replies if the given trip is compatible with this trip.
	 * 
	 * @param trip
	 * @return <code>true</code> or <code>false</code>.
	 */
	public boolean isSimilar(Trip trip) {
		boolean sourceClose = isClose(getSource(), trip.getSource());
		boolean destinationClose = isClose(getDestination(), trip.getDestination());
		boolean timeClose = getTimeWindow().isSimilar(trip.getTimeWindow());
		
		return sourceClose && destinationClose && timeClose;
	}
	
	private static boolean isClose(Location a, Location b) {
		Point2d aa = a.getGISPosition().toPoint2D();
		Point2d bb = b.getGISPosition().toPoint2D();
		return aa.distance(bb) <= MAX_DISTANCE;
	}
	
	/** Replies the road path of the trip.
	 * 
	 * @param network
	 * @return a copy of the road path
	 */
	public RoadPath getRoadPath(RoadNetwork network) {
		RoadPath r = (this.path==null) ? null : this.path.get();
		if (r==null) {
			r = new RoadPath();
			for(TripComponent c : this.components) {
				RoadPath cp = c.getRoadPath(network);
				if (cp!=null && !cp.isEmpty()) {
					RoadPath.addPathToPath(r, cp);
				}
			}
			this.path = new SoftReference<RoadPath>(r);
		}
		return r.clone();
	}

}
