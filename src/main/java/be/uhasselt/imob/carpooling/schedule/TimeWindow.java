/* 
 * $Id$
 * 
 * Copyright (C) 2013
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.schedule;

import java.io.Serializable;

import fr.utbm.set.util.HashCodeUtil;

import be.uhasselt.imob.carpooling.kernel.time.Time;

/** Time window.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class TimeWindow implements Serializable, Cloneable {

	private static final long serialVersionUID = 7590990893840957749L;

	/** Preferred start time.
	 */
	public final Time preferredStart;

	/** Preferred end time.
	 */
	public final Time preferredEnd;

	/** Minimal enabled start time.
	 */
	public final Time minStart;

	/** Maximal enabled end time.
	 */
	public final Time maxEnd;

	/**
	 * @param prefStart
	 * @param prefEnd
	 * @param minStart
	 * @param maxEnd
	 */
	public TimeWindow(Time prefStart, Time prefEnd, Time minStart, Time maxEnd) {
		this.preferredStart = prefStart;
		this.preferredEnd = prefEnd;
		this.minStart = minStart;
		this.maxEnd = maxEnd;
	}

	@Override
	public TimeWindow clone() {
		try {
			return (TimeWindow)super.clone();
		}
		catch (CloneNotSupportedException e) {
			throw new Error(e);
		}
	}
	
	/** {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj==this) return true;
		if (obj instanceof TimeWindow) {
			TimeWindow tm = (TimeWindow)obj;
			return this.minStart.equals(tm.minStart)
					&& this.maxEnd.equals(tm.maxEnd)
					&& this.preferredStart.equals(tm.preferredStart)
					&& this.preferredEnd.equals(tm.preferredEnd);
		}
		return false;
	}

	/** {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int h = HashCodeUtil.hash(this.minStart);
		h = HashCodeUtil.hash(h, this.maxEnd);
		h = HashCodeUtil.hash(h, this.preferredStart);
		return HashCodeUtil.hash(h, this.preferredEnd);
	}
	
	/** Replies if the given time window is compatible with this time window.
	 * 
	 * @param t
	 * @return <code>true</code> or <code>false</code>.
	 */
	public boolean isSimilar(TimeWindow t) {
		return (this.preferredStart.compareTo(t.minStart)>=0)&&
				(this.preferredEnd.compareTo(t.maxEnd)<=0)&&
				(t.preferredStart.compareTo(this.minStart)>=0)&&
				(t.preferredEnd.compareTo(this.maxEnd)<=0);
	}

	/** {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("[["); //$NON-NLS-1$
		if (this.minStart!=null && this.minStart.before(this.preferredStart)) {
			s.append(this.minStart);
		}
		s.append(" ( "); //$NON-NLS-1$
		s.append(this.preferredStart);
		s.append(" >>> "); //$NON-NLS-1$
		s.append(this.preferredEnd);
		s.append(" ) "); //$NON-NLS-1$
		if (this.maxEnd!=null && this.maxEnd.after(this.preferredEnd)) {
			s.append(this.maxEnd);
		}
		s.append("]]"); //$NON-NLS-1$
		return s.toString();
	}
	
	/** Create the union of this window and the given window.
	 * 
	 * @param t
	 * @return the union
	 */
	public TimeWindow union(TimeWindow t) {
		Time min = (this.minStart.before(t.minStart)) ?
						this.minStart : t.minStart;
		Time max = (this.maxEnd.before(t.maxEnd)) ?
				this.maxEnd : t.maxEnd;
		Time prefStart = (this.preferredStart.before(t.preferredStart)) ?
				this.preferredStart : t.preferredStart;
		Time prefEnd = (this.preferredEnd.before(t.preferredEnd)) ?
				this.preferredEnd : t.preferredEnd;
		return new TimeWindow(prefStart, prefEnd, min, max);
	}
	
	/** Create the union of this window and the given window.
	 * 
	 * @param t
	 * @param interpolation is a value in {@code [0;1]} which
	 * permits to select the preferred start (resp. end)
	 * between the preferred starts (reps. ends) of the two windows.
	 * A value of {@code 0} is for the preferred value of this window.
	 * A value of {@code 1} is for the preferred value of <var>t</var>.
	 * @return the union
	 */
	public TimeWindow union(TimeWindow t, float interpolation) {
		Time min = (this.minStart.before(t.minStart)) ?
						this.minStart : t.minStart;
		Time max = (this.maxEnd.before(t.maxEnd)) ?
				this.maxEnd : t.maxEnd;
		Time prefStart = this.preferredStart.interpolate(t.preferredStart, interpolation);
		Time prefEnd = this.preferredEnd.interpolate(t.preferredEnd, interpolation);
		return new TimeWindow(prefStart, prefEnd, min, max);
	}

}
