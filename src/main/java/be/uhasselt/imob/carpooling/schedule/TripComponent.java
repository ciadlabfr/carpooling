/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.schedule;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import org.janusproject.kernel.address.AgentAddress;

import be.uhasselt.imob.carpooling.environment.Location;
import be.uhasselt.imob.carpooling.kernel.time.Time;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.gis.road.path.astar.RoadAStar;
import fr.utbm.set.gis.road.path.astar.RoadAStarDistanceHeuristic;
import fr.utbm.set.gis.road.primitive.RoadConnection;
import fr.utbm.set.gis.road.primitive.RoadNetwork;
import fr.utbm.set.graph.astar.AStarHeuristic;

/** Component of a trip.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class TripComponent implements Cloneable, Serializable, Comparable<TripComponent> {

	private static final long serialVersionUID = 7063663987600356513L;
	
	private final Location source;
	private final Location destination;
	private final Time startTime;
	private final Time endTime;
	private final TripMode tripMode;
	private Set<AgentAddress> participants = new TreeSet<AgentAddress>();
	private transient RoadPath path = null;
	
	/**
	 * @param source
	 * @param destination
	 * @param startTime
	 * @param endTime
	 * @param mode
	 * @param path
	 */
	public TripComponent(Location source, Location destination, Time startTime, Time endTime, TripMode mode, RoadPath path) {
		assert(startTime.compareTo(endTime)<=0);
		this.source = source;
		this.destination = destination;
		this.startTime = startTime;
		this.endTime = endTime;
		this.tripMode = mode;
		this.path = (path==null || path.isEmpty()) ? null : path;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripComponent clone() {
		TripComponent clone;
		try {
			clone = (TripComponent)super.clone();
		}
		catch (CloneNotSupportedException e) {
			throw new Error(e);
		}
		clone.participants = new TreeSet<AgentAddress>();
		clone.participants.addAll(this.participants);
		if (this.path!=null) {
			clone.path = (this.path==null) ? null : this.path.clone();
		}
		return clone;
	}
	
	/** Replies the source of the trip component.
	 * 
	 * @return the source of the trip component.
	 */
	public Location getSource() {
		return this.source;
	}

	/** Replies the destination of the trip component.
	 * 
	 * @return the destination of the trip component.
	 */
	public Location getDestination() {
		return this.destination;
	}
	
	/** Replies the start time.
	 *
	 * @return the start time.
	 */
	public Time getStartTime() {
		return this.startTime;
	}

	/** Replies the end time.
	 *
	 * @return the end time.
	 */
	public Time getEndTime() {
		return this.endTime;
	}
	
	/** Replies the preferred mode to use for the trip.
	 * 
	 * @return the mode; or <code>null</code> if there is no selected mode.
	 */
	public TripMode getPreferredTransportMode() {
		return this.tripMode;
	}

	/** Replies the participants.
	 * If the mode is not selected or the mode
	 * is not related to a car; then the list
	 * of participants is always empty.
	 * 
	 * @return the participants.
	 */
	public Set<AgentAddress> getParticipants() {
		return Collections.unmodifiableSet(this.participants);
	}

	/** Add a participant.
	 * 
	 * @param participant
	 */
	public void addParticipant(AgentAddress participant) {
		this.participants.add(participant);
	}

	/** Remove a participant.
	 * 
	 * @param participant
	 */
	public void removeParticipant(AgentAddress participant) {
		this.participants.remove(participant);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj==this) return true;
		if (obj instanceof TripComponent) {
			TripComponent o = (TripComponent)obj;
			return this.source.equals(o.getSource())
				&& this.destination.equals(o.getDestination())
				&& this.startTime.equals(o.getStartTime())
				&& this.endTime.equals(o.getEndTime());
		}
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int h = 1;
		h = h * 37 + this.source.hashCode();
		h = h * 37 + this.destination.hashCode();
		h = h * 37 + this.startTime.hashCode();
		h = h * 37 + this.endTime.hashCode();
		return h;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "[" //$NON-NLS-1$
				+this.source.toString()
				+"@" //$NON-NLS-1$
				+this.startTime.toString()
				+"]->[" //$NON-NLS-1$
				+this.destination.toString()
				+"@" //$NON-NLS-1$
				+this.endTime.toString()
				+"]"; //$NON-NLS-1$
	}

	/** {@inheritDoc}
	 */
	@Override
	public int compareTo(TripComponent o) {
		if (o==null) return Integer.MAX_VALUE;
		if (o==this) return 0;
		int cmp = this.startTime.compareTo(o.startTime);
		if (cmp!=0) return cmp;
		cmp = this.endTime.compareTo(o.endTime);
		if (cmp!=0) return cmp;
		cmp = this.source.getGISPosition().compareTo(o.source.getGISPosition());
		if (cmp!=0) return cmp;
		cmp = this.destination.getGISPosition().compareTo(o.destination.getGISPosition());
		if (cmp!=0) return cmp;
		cmp = this.tripMode.ordinal() - o.tripMode.ordinal();
		if (cmp!=0) return cmp;
		return 0;
	}
	
	/** Replies the road path for this trip component.
	 * 
	 * @param network
	 * @return the road path.
	 */
	public RoadPath getRoadPath(RoadNetwork network) {
		if (this.path==null) {
			AStarHeuristic<RoadConnection> heuristic = new RoadAStarDistanceHeuristic();
			RoadAStar astar = new RoadAStar(heuristic);
			this.path = astar.solve(this.source.getGISPosition().toPoint2D(), this.destination.getGISPosition().toPoint2D(), network);
			if (this.path==null) {
				this.path = new RoadPath();
			}
		}
		return this.path;
	}
}
