/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (SET-UTBM).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (SET-UTBM).
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for SET-UTBM: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.agent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.SortedSet;
import java.util.TreeSet;

import org.janusproject.kernel.address.AgentAddress;
import org.janusproject.kernel.agent.AgentActivationPrototype;
import org.janusproject.kernel.agentmemory.BlackBoardMemory;
import org.janusproject.kernel.crio.capacity.CapacityContext;
import org.janusproject.kernel.crio.capacity.CapacityImplementation;
import org.janusproject.kernel.crio.core.GroupAddress;
import org.janusproject.kernel.crio.core.RoleAddress;
import org.janusproject.kernel.crio.organization.Group;
import org.janusproject.kernel.probe.Watchable;
import org.janusproject.kernel.status.Status;

import be.uhasselt.imob.carpooling.capacity.TimeWindowEvaluator;
import be.uhasselt.imob.carpooling.capacity.TransportModeEvaluator;
import be.uhasselt.imob.carpooling.capacity.TripProposalEvaluator;
import be.uhasselt.imob.carpooling.environment.Environment;
import be.uhasselt.imob.carpooling.environment.SocialNetwork;
import be.uhasselt.imob.carpooling.kernel.time.CarpoolingTimeManager;
import be.uhasselt.imob.carpooling.kernel.time.NextDayArrived;
import be.uhasselt.imob.carpooling.organization.drive.Driver;
import be.uhasselt.imob.carpooling.organization.drive.DrivingOrganization;
import be.uhasselt.imob.carpooling.organization.negociation.CarpoolingNegociationOrganization;
import be.uhasselt.imob.carpooling.organization.negociation.Partner;
import be.uhasselt.imob.carpooling.schedule.TimeWindow;
import be.uhasselt.imob.carpooling.schedule.Trip;
import be.uhasselt.imob.carpooling.schedule.TripComponent;
import be.uhasselt.imob.carpooling.schedule.TripMode;
import be.uhasselt.imob.carpooling.util.AgentType;
import be.uhasselt.imob.carpooling.util.Gender;
import be.uhasselt.imob.carpooling.util.SimulationLogger;
import be.uhasselt.imob.carpooling.util.SimulationParameters;
import be.uhasselt.imob.carpooling.util.TransportMode;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.jasim.JasimConstants;
import fr.utbm.set.jasim.environment.interfaces.body.AgentBody;
import fr.utbm.set.jasim.environment.interfaces.body.factory.AgentBodyFactory;
import fr.utbm.set.jasim.environment.interfaces.body.influences.Influence1D5;
import fr.utbm.set.jasim.environment.interfaces.body.perceptions.Perception1D5;
import fr.utbm.set.jasim.environment.semantics.VehicleType;
import fr.utbm.set.jasim.janus.agent.JanusSituatedAgent;

/** Agent that is participating to the carpooling simulation.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
@AgentActivationPrototype(
		fixedParameters = {Gender.class, Integer.class, Boolean.class, Boolean.class, Environment.class, Collection.class}
)
public class CarpoolingAgent extends JanusSituatedAgent<Influence1D5<RoadSegment>,Perception1D5> {

	private static final long serialVersionUID = 2450375954592761067L;
	
	private static final int WAITING_TIMEOUT = 20;
	
	private LinkedList<Trip> myTrips = new LinkedList<Trip>();
	
	private AgentProfile description;
	private Environment environment;
	private AgentActivity activity = AgentActivity.S0_TRIP_REGISTRATION;
	
	private SocialNetwork socialNetwork = new SocialNetwork();
	
	private final SortedSet<TripDescription> tripsToExecute = new TreeSet<TripDescription>();
	private final SortedSet<TripDescription> executedTrips = new TreeSet<TripDescription>();
	private TripDescription tripDescription = null;
	private int waitingTimeout = WAITING_TIMEOUT;

	/** Path followed by the driver. It is introduced for the probing API.
	 * TODO: It should be replaced byb the Channel API.
	 */
	@Watchable
	public RoadPath followedPath = null;

	/** Current trip followed by the driver. It is introduced for the probing API.
	 * TODO: It should be replaced byb the Channel API.
	 */
	@Watchable
	public Trip trip = null;

	private final SimulationParameters simulationParameters;
	private final CarpoolingAgentBodyFactory bodyFactory;
	
	/**
	 * @param simulationParameters are the parameters of the simulation.
	 */
	public CarpoolingAgent(SimulationParameters simulationParameters, CarpoolingAgentBodyFactory bodyFactory) {
		super(false);
		this.simulationParameters = simulationParameters;
		this.bodyFactory = bodyFactory;
	}
	
	/** Replies the activity of the agent.
	 * 
	 * @return the activity of the agent.
	 */
	public AgentActivity getAgentActivity() {
		return this.activity;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Status activate(Object... parameters) {
		setMemory(new AgentMemory());
		getCapacityContainer().addCapacity(new TransportModeChooser());
		getCapacityContainer().addCapacity(new TMEvaluator());
		getCapacityContainer().addCapacity(new TPEvaluator());
		this.description = new AgentProfile((Gender)parameters[0], (Integer)parameters[1]);
		this.description.setDriverLicense((Boolean)parameters[2]);
		this.description.setHasVehicle((Boolean)parameters[3]);
		this.environment = (Environment)parameters[4];
		this.myTrips.addAll((Collection<? extends Trip>)parameters[5]);
		this.activity = AgentActivity.S0_TRIP_REGISTRATION;
		if (this.description.hasDriverLicense()) {
			this.description.setAgentType(AgentType.DRIVER);
		}
		else {
			this.description.setAgentType(AgentType.PASSENGER);
		}
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Status live() {
		Status s = super.live();
		if (s==null || s.isSuccess()) {
			switch(this.activity) {
			case S0_TRIP_REGISTRATION:
				registerTrips();
				this.activity = AgentActivity.S1_SELECTING_MODE;
				break;

			case S1_SELECTING_MODE:
				this.activity = selectTripAndMode();
				break;
			
			case S1_SEARCHING_PARTNER:
				assert(this.tripDescription!=null);
				if (this.tripDescription.isDriver) {
					this.activity = selectPartners();
				}
				else {
					this.activity = waitForProposal();
				}
				break;
				
			case S1_NEGOCIATING:
				if (!isPlayedRole(Partner.class)) {
					// The negociation is finished
					this.activity = finalizeNegociation();
				}
				break;
				
			case S2_CARPOOLING:
				this.activity = drive();
				break;
				
			case S2_SINGLE_TRAVELLING:
				this.activity = drive();
				break;
				
			case S3_FEEDBACKING:
				//TODO: Invoke the part of the model that computes the feedbacks and returns to the initial state.
				if (this.myTrips.isEmpty()) {
					// no more trips
					this.activity = null;
					stopAgent();
				}
				else {
					this.activity = AgentActivity.S1_SELECTING_MODE;
					sleepUntilNextDay();
				}
				break;
			default:
			}
			return null;
		}
		return s;
	}
	
	private AgentActivity finalizeNegociation() {
		if (this.tripDescription.negociatedWindow!=null
			&& !this.tripDescription.partners.isEmpty()) {
			// Success in negociation
			this.tripDescription.isCarpoolingFailure = null;
		}
		else {
			if (this.tripDescription.isCarpoolingFailure==null)
				this.tripDescription.isCarpoolingFailure = CarpoolingFailure.NO_PARTNER;
			this.tripDescription.isDriver = true;
			this.tripDescription.transportMode = TransportMode.SOV;
		}		
		if (this.tripDescription.isDriver)
			this.tripsToExecute.add(this.tripDescription);
		//TODO:else
			//this.executedTrips.add(this.tripDescription);
		this.tripDescription = null;
		return AgentActivity.S1_SELECTING_MODE;
	}
	
	private void stopAgent() {
		finalizeEndOfDay();
		killMe();
	}
	
	private void finalizeEndOfDay() {
		CarpoolingTimeManager tm = (CarpoolingTimeManager)getTimeManager();
		
		for(TripDescription td : this.executedTrips) {
			SimulationLogger.log(
					td.trip.getStartTime(),
					getAddress(),
					td.isDriver,
					td.transportMode,
					td.wantToCarpool,
					td.isCarpoolingFailure==CarpoolingFailure.NO_PARTNER,
					td.isCarpoolingFailure==CarpoolingFailure.NEGOCIATION_FAILURE,
					td.partners.size(),
					td.trip,
					td.negociatedWindow,
					td.travelDuration);
		}
		
		for(TripDescription td : this.tripsToExecute) {
			SimulationLogger.logError(
					td.trip.getStartTime(),
					getAddress(),
					td.isDriver,
					td.transportMode,
					td.wantToCarpool,
					td.isCarpoolingFailure==CarpoolingFailure.NO_PARTNER,
					td.isCarpoolingFailure==CarpoolingFailure.NEGOCIATION_FAILURE,
					td.partners.size(),
					td.trip,
					td.negociatedWindow,
					td.travelDuration);
		}

		this.executedTrips.clear();
		this.tripsToExecute.clear();
		this.followedPath = null;
		this.tripDescription = null;
	}
	
	/** This agent sleeps until the next day is
	 * arrived.
	 */
	private void sleepUntilNextDay() {
		finalizeEndOfDay();
		CarpoolingTimeManager tm = (CarpoolingTimeManager)getTimeManager();
		sleep(new NextDayArrived(tm.getCurrentDate()));
	}
	
	/** Register the trips of the agent.
	 */
	private void registerTrips() {
		for(Trip trip : this.myTrips) {
			this.environment.registerTrip(getAddress(),
					this.description.hasDriverLicense()/* && this.description.hasVehicle()*/ ? 
							AgentType.DRIVER
							: AgentType.PASSENGER, trip);
		}
	}
	
	private TransportMode getNextTripMode() {
		if (this.tripsToExecute.isEmpty()) return null;
		TripDescription d = this.tripsToExecute.first();
		return d.transportMode;
	}
	
	private TripDescription consumeNextTrip() {
		Iterator<TripDescription> it = this.tripsToExecute.iterator();
		TripDescription td = it.next();
		it.remove();
		return td;
	}

	private AgentActivity selectNextDayActivity() {
		TransportMode tm = getNextTripMode();
		if (tm==TransportMode.CARPOOLING)
			return AgentActivity.S2_CARPOOLING;
		if (tm==TransportMode.SOV)
			return AgentActivity.S2_SINGLE_TRAVELLING;
		return AgentActivity.S3_FEEDBACKING;
	}
	
	/** Select the trip and the traveling mode.
	 * 
	 * @return the next state to follow.
	 */
	private AgentActivity selectTripAndMode() {
		if (this.myTrips.isEmpty()) {
			// No more trip.
			return selectNextDayActivity();
		}
		
		this.tripDescription = new TripDescription(this.myTrips.removeFirst());
				
		TripComponent tc = this.tripDescription.trip.getTripComponents().get(0);
		TripMode mode = tc.getPreferredTransportMode();
		this.tripDescription.transportMode = null;
		switch(mode) {
		case CAR_PASSENGER:
			this.tripDescription.transportMode = TransportMode.CARPOOLING;
			break;
		case PUBLIC_TRANSPORT:
			this.tripDescription.transportMode = TransportMode.PUBLIC_TRANSPORT;
			break;
		case SLOW:
		case CAR:
		default:
			break;
		}
		
		if (this.tripDescription.transportMode==null) {
			float bestCost = Float.POSITIVE_INFINITY;
			
			TransportModeEvaluator evaluator = getCapacityContainer().selectImplementation(TransportModeEvaluator.class);
			
			evaluator.prepareEvaluations();
			
			for(TransportMode tmode : TransportMode.values()) {
				float cost = evaluator.evaluateCost(
						this.description.getAgentType(),
						tmode,
						this.environment);
				if (cost<bestCost) {
					bestCost = cost;
					this.tripDescription.transportMode = tmode;
				}
			}
		}
		
		if (this.tripDescription.transportMode == TransportMode.CARPOOLING) {
			this.tripDescription.wantToCarpool = true;
			this.tripDescription.isDriver = 
					mode!=TripMode.CAR_PASSENGER && 
					this.description.hasDriverLicense();
			return AgentActivity.S1_SEARCHING_PARTNER;
		}
		else if (this.tripDescription.transportMode == TransportMode.SOV) {
			this.tripDescription.wantToCarpool = false;
			this.tripDescription.isDriver = true;
			this.tripsToExecute.add(this.tripDescription);
			this.tripDescription = null;
			return AgentActivity.S1_SELECTING_MODE;
		}
		this.tripDescription.wantToCarpool = false;
		this.tripDescription.isDriver = false;
		this.executedTrips.add(this.tripDescription);
		this.tripDescription = null;
		return AgentActivity.S1_SELECTING_MODE;
	}
	
	/** Wait for the proposal from a driver agent.
	 * 
	 * @return the next state to follow.
	 */
	private AgentActivity waitForProposal() {
		GroupAddress adr = findOtherNegociationGroup();
		if (adr!=null) {
			this.tripDescription.negociationGroup = adr;
			requestRole(Partner.class, adr, false);
			return AgentActivity.S1_NEGOCIATING;
		}
		
		if (this.waitingTimeout<=0) {
			this.waitingTimeout = WAITING_TIMEOUT;
			this.tripDescription.isCarpoolingFailure = CarpoolingFailure.NO_PARTNER;
			if (this.description.hasDriverLicense()) {
				this.tripDescription.isDriver = true;
				this.tripDescription.transportMode = TransportMode.SOV;
				this.tripsToExecute.add(this.tripDescription);
			}
			else {
				this.tripDescription.isDriver = false;
				this.tripDescription.transportMode = TransportMode.PUBLIC_TRANSPORT;
				this.executedTrips.add(this.tripDescription);
			}
			this.tripDescription = null;
			return AgentActivity.S1_SELECTING_MODE;
		}
		--this.waitingTimeout;
		return AgentActivity.S1_SEARCHING_PARTNER;
	}

	private GroupAddress findOtherNegociationGroup() {
		assert(this.tripDescription!=null);
		for(GroupAddress adr : getExistingGroups(CarpoolingNegociationOrganization.class)) {
			if (!adr.equals(this.tripDescription.negociationGroup)) {
				Group g = getGroupObject(adr);
				Object value = g.getPublicUserData("trip"); //$NON-NLS-1$
				if (value instanceof Trip) {
					if (this.tripDescription.trip.isSimilar((Trip)value)) {
						return adr;
					}
				}
			}
		}
		return null;
	}
	
	/** Select the partners for carpooling.
	 * 
	 * @return the next state to follow.
	 */
	private AgentActivity selectPartners() {
		GroupAddress adr = findOtherNegociationGroup();
		if (adr!=null) {
			this.tripDescription.isDriver = false;
			this.tripDescription.partners.clear();
			this.tripDescription.negociationGroup = adr;
			this.tripDescription.isCarpoolingFailure = null;
			requestRole(Partner.class, this.tripDescription.negociationGroup, false);
			return AgentActivity.S1_NEGOCIATING;
		}
		
		this.tripDescription.partners.clear();
		
		/*// Do the local exploration
		for(Acquointance acq : this.socialNetwork.getAcquointances()) {
			// it is assumed that the social network contains only
			// partners that have compatible profiles
			AgentAddress ag = acq.getAgent();
			if (this.environment.hasCompatibleTrip(ag, this.trip)) {
				this.partners.add(ag);
			}
		}
		
		if (this.partners.isEmpty()) {
			// Do the global exploration
			for(AgentAddress ag : this.environment.getPartnersWithCompatibleTrips(this.trip)) {
				this.partners.add(ag);
			}
		}
		
		if (this.partners.isEmpty()) {
			this.isDriver = true;
			return AgentActivity.SINGLE_TRAVELLING;
		}*/
		
		this.tripDescription.negociationGroup = createGroup(CarpoolingNegociationOrganization.class);
		this.tripDescription.isCarpoolingFailure = null;
		requestRole(Partner.class, this.tripDescription.negociationGroup, true);
		
		return AgentActivity.S1_NEGOCIATING;
	}
	
	/** Run the driving simulation without carpoolers.
	 * 
	 * @return the next activity to follow.
	 */
	private AgentActivity  drive() {
		if (this.tripDescription==null) {
			// Start to drive
			this.tripDescription = consumeNextTrip();
			this.bodyFactory.create(this, this.tripDescription.trip);
			GroupAddress drivingSystem = getOrCreateGroup(DrivingOrganization.class);
			this.trip = this.tripDescription.trip;
			this.tripDescription.driverRole = requestRole(Driver.class, drivingSystem, this.tripDescription.trip, this.tripDescription.negociatedWindow, this.simulationParameters);
		}
		else if (!isPlayedRole(Driver.class)) {
			this.tripDescription.driverRole = null;
			setEnvironmentBody(null);
			this.executedTrips.add(this.tripDescription);
			this.tripDescription = null;
			this.trip = null;
			return selectNextDayActivity();
		}
		return this.activity;
	}
	
	/** Replies the next trip to execute.
	 * 
	 * @return the next trip to execute; or <code>null</code>.
	 */
	public Trip getTrip() {
		if (this.tripDescription!=null)
			return this.tripDescription.trip;
		return null;
	}

	/** {@inheritDoc}
	 */
	@Override
	public Class<? extends Perception1D5> getPreferredPerceptionType() {
		return Perception1D5.class;
	}

	/** {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Class getPreferredInfluenceType() {
		return Influence1D5.class;
	}

	/** {@inheritDoc}
	 */
	@Override
	protected AgentBody<Influence1D5<RoadSegment>, Perception1D5> createPersonalBody(
			AgentBodyFactory factory) {
		AgentBody<Influence1D5<RoadSegment>, Perception1D5> body = factory.createVehicleBody(
				this,
				Perception1D5.class,
				JasimConstants.DEFAULT_VEHICLE_LENGTH,
				JasimConstants.DEFAULT_VEHICLE_WIDTH,
				VehicleType.VEHICLETYPE_SINGLETON,
				JasimConstants.DEFAULT_VEHICLE_MAX_FORWARD_SPEED,
				JasimConstants.DEFAULT_VEHICLE_MAX_BACKWARD_SPEED,
				JasimConstants.DEFAULT_VEHICLE_MAX_ANGULAR_SPEED,
				JasimConstants.DEFAULT_VEHICLE_MAX_LINEAR_ACCELERATION,
				JasimConstants.DEFAULT_VEHICLE_MAX_LINEAR_DECELERATION,
				JasimConstants.DEFAULT_VEHICLE_MAX_ANGULAR_ACCELERATION,
				JasimConstants.DEFAULT_VEHICLE_MAX_ANGULAR_DECELERATION);
		factory.detachAllFrustums(body);
		factory.attachRectangularFrustum(
				body,
				JasimConstants.DEFAULT_VEHICLE_HEIGHT/2f,
				JasimConstants.DEFAULT_VEHICLE_FORWARD_PERCEPTION_DISTANCE,
				JasimConstants.DEFAULT_VEHICLE_WIDTH,
				JasimConstants.DEFAULT_VEHICLE_HEIGHT);
		return body;
	}
	
	/** Implementation of the capacity to select a transport mode.
	 * 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private static class TransportModeChooser extends CapacityImplementation implements TransportModeEvaluator {

		private float sovCost;
		private float cpCost;
		private float ptCost;
		
		/**
		 */
		public TransportModeChooser() {
			prepareEvaluations();
		}

		/** {@inheritDoc}
		 */
		@Override
		public void prepareEvaluations() {
			//TODO: Change the evaluation
			/*if (Math.random()<=.2f) {
				this.sovCost = (float)Math.random()*500f;
				this.cpCost = this.sovCost - 1;
				this.ptCost = this.sovCost + 1;
			}
			else {
				this.sovCost = (float)Math.random()*500f;
				this.cpCost = (float)Math.random()*250f;
				this.ptCost = (float)Math.random()*1000f;
			}*/
			this.cpCost = 1f;
			this.sovCost = 2f;
			this.ptCost = 2f;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public float evaluateCost(
				AgentType agentType,
				TransportMode transportType,
				Environment environment) {
			// FIXME: Compute the cost
			switch(transportType) {
			case PUBLIC_TRANSPORT:
				return this.ptCost;
			case CARPOOLING:
				return this.cpCost;
			case SOV:
				return this.sovCost;
			default:
			}
			return Float.POSITIVE_INFINITY;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void call(CapacityContext call) throws Exception {
			float cost = evaluateCost(
					call.getInputValueAt(0, AgentType.class),
					call.getInputValueAt(1, TransportMode.class),
					call.getInputValueAt(2, Environment.class));
			call.setOutputValues(Float.valueOf(cost));
		}
		
	}
	
	/** Implementation of the capacity to evaluate time windows of the trips.
	 * 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private static class TMEvaluator extends CapacityImplementation implements TimeWindowEvaluator {

		/**
		 */
		public TMEvaluator() {
			//
		}

		/** {@inheritDoc}
		 */
		@Override
		public TimeWindow evaluateTimeWindows(Trip trip, Collection<TimeWindow> windows) {
			TimeWindow tw = null;
			for(TimeWindow tm : windows) {
				if (tw!=null) tw = tm.union(tw);
				else tw = tm;
			}
			return tw;
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public void call(CapacityContext call) throws Exception {
			Trip t = call.getInputValueAt(0, Trip.class);
			Collection c = call.getInputValueAt(1, Collection.class);
			TimeWindow tm = evaluateTimeWindows(t, c);
			if (tm==null) {
				call.fail();
			}
			else {
				call.setOutputValues(tm);
			}
		}
		
	}
	
	/** Implementation of the capacity to evaluate time windows of the trips.
	 * 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private class TPEvaluator extends CapacityImplementation implements TripProposalEvaluator {

		/**
		 */
		public TPEvaluator() {
			//
		}

		/** {@inheritDoc}
		 */
		@Override
		public void call(CapacityContext call) throws Exception {
			Trip myTrip = call.getInputValueAt(0, Trip.class);
			Trip partnerTrip = call.getInputValueAt(1, Trip.class);
			TimeWindow tm = call.getInputValueAt(2, TimeWindow.class);
			tm = evaluateTrip(myTrip, partnerTrip, tm);
			if (tm==null) {
				call.fail();
			}
			else {
				call.setOutputValues(tm);
			}
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public TimeWindow evaluateTrip(Trip myTrip, Trip partnerTrip, TimeWindow window) {
			TimeWindow selectedWindow = window;
			if (selectedWindow==null) {
				selectedWindow = partnerTrip.getTimeWindow().clone();
			}
			TimeWindow myWindow = myTrip.getTimeWindow();
			selectedWindow = selectedWindow.union(myWindow,
					CarpoolingAgent.this.simulationParameters.getEgocentricFactor());
			return selectedWindow;
		}
		
	}

	/** Memory.
	 * 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private class AgentMemory extends BlackBoardMemory {

		/**
		 */
		public AgentMemory() {
			//
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public Object getMemorizedData(String id) {
			CarpoolingAgentMemoryCell cell = CarpoolingAgentMemoryCell.valueOf(id.toUpperCase());
			if (cell!=null) {
				switch(cell) {
				case ACTIVITY:
					return CarpoolingAgent.this.activity;
				case ENVIRONMENT:
					return CarpoolingAgent.this.environment;
				case PATH:
					return CarpoolingAgent.this.followedPath;
				case TRIP:
					return CarpoolingAgent.this.tripDescription.trip;
				case NEGOCIATED_WINDOW:
					return CarpoolingAgent.this.tripDescription.negociatedWindow;
				case NEGOCIATION_FAILURE:
					return CarpoolingAgent.this.tripDescription.isCarpoolingFailure;
				case CARPOOLING_PARTNERS:
					return CarpoolingAgent.this.tripDescription.partners;
				case TRAVEL_TIME:
					return CarpoolingAgent.this.tripDescription.travelDuration;
				default:
				}
			}
			return super.getMemorizedData(id);
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public boolean hasMemorizedData(String id) {
			CarpoolingAgentMemoryCell cell = CarpoolingAgentMemoryCell.valueOf(id.toUpperCase());
			if (cell!=null) {
				switch(cell) {
				case ACTIVITY:
					return CarpoolingAgent.this.activity!=null;
				case ENVIRONMENT:
					return CarpoolingAgent.this.environment!=null;
				case PATH:
					return CarpoolingAgent.this.followedPath!=null && !CarpoolingAgent.this.followedPath.isEmpty();
				case TRIP:
					return CarpoolingAgent.this.tripDescription!=null;
				case NEGOCIATED_WINDOW:
					return CarpoolingAgent.this.tripDescription!=null
							&& CarpoolingAgent.this.tripDescription.negociatedWindow!=null;
				case NEGOCIATION_FAILURE:
					return CarpoolingAgent.this.tripDescription.isCarpoolingFailure!=null;
				case CARPOOLING_PARTNERS:
					return !CarpoolingAgent.this.tripDescription.partners.isEmpty();
				case TRAVEL_TIME:
					return CarpoolingAgent.this.tripDescription!=null;
				default:
				}
			}
			return super.hasMemorizedData(id);
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public boolean putMemorizedData(String id, Object value) {
			CarpoolingAgentMemoryCell cell = CarpoolingAgentMemoryCell.valueOf(id.toUpperCase());
			if (cell!=null) {
				switch(cell) {
				case PATH:
					if (value instanceof RoadPath) {
						CarpoolingAgent.this.followedPath = (RoadPath)value;
						return true;
					}
					return false;
				case NEGOCIATED_WINDOW:
					if (value instanceof TimeWindow) {
						CarpoolingAgent.this.tripDescription.negociatedWindow = (TimeWindow)value;
						return true;
					}
					return false;
				case NEGOCIATION_FAILURE:
					if (value instanceof CarpoolingFailure) {
						CarpoolingAgent.this.tripDescription.isCarpoolingFailure = (CarpoolingFailure)value;
						return true;
					}
					return false;
				case CARPOOLING_PARTNERS:
					if (value instanceof Iterable) {
						CarpoolingAgent.this.tripDescription.partners.clear();
						for(Object o : (Iterable<?>)value) {
							if (o instanceof AgentAddress) {
								CarpoolingAgent.this.tripDescription.partners.add(
										(AgentAddress)o);
							}
						}
						return true;
					}
					return false;
				case TRAVEL_TIME:
					if (value instanceof Number) {
						CarpoolingAgent.this.tripDescription.travelDuration = ((Number)value).doubleValue();
						return true;
					}
					return false;
				case TRIP:
				case ACTIVITY:
				case ENVIRONMENT:
					return false;
				default:
				}
			}
			return super.putMemorizedData(id, value);
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public void removeMemorizedData(String id) {
			CarpoolingAgentMemoryCell cell = CarpoolingAgentMemoryCell.valueOf(id.toUpperCase());
			if (cell!=null) {
				switch(cell) {
				case PATH:
					CarpoolingAgent.this.followedPath = null;
					return;
				case NEGOCIATED_WINDOW:
					CarpoolingAgent.this.tripDescription.negociatedWindow = null;
					return;
				case NEGOCIATION_FAILURE:
					CarpoolingAgent.this.tripDescription.isCarpoolingFailure = null;
					return;
				case CARPOOLING_PARTNERS:
					CarpoolingAgent.this.tripDescription.partners.clear();
					return;
				case TRAVEL_TIME:
					CarpoolingAgent.this.tripDescription.travelDuration = 0;
					return;
				case TRIP:
				case ACTIVITY:
				case ENVIRONMENT:
					return;
				default:
				}
			}
			super.removeMemorizedData(id);
		}
		
	}
	
	/**
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	public static enum CarpoolingFailure  {

		/** Failure in negociation.
		 */
		NEGOCIATION_FAILURE,
		
		/** No partner.
		 */
		NO_PARTNER;
		
	}

	/**
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private static class TripDescription implements Comparable<TripDescription> {

		public final Trip trip;
		public TimeWindow negociatedWindow = null;
		public boolean isDriver = false;
		public boolean wantToCarpool = false;
		public CarpoolingFailure isCarpoolingFailure = null;
		public final Collection<AgentAddress> partners = new ArrayList<AgentAddress>();
		public RoleAddress driverRole = null;
		public GroupAddress negociationGroup = null;
		public TransportMode transportMode = null;
		public double travelDuration = 0;
		
		/**
		 * @param trip
		 */
		public TripDescription(Trip trip) {
			this.trip = trip;
		}

		/** {@inheritDoc}
		 */
		@Override
		public int compareTo(TripDescription o) {
			if (o==null) return Integer.MAX_VALUE;
			if (this==o) return 0;
			TimeWindow tw1;
			if (this.negociatedWindow!=null) {
				tw1 = this.negociatedWindow;
			}
			else {
				tw1 = this.trip.getTimeWindow();
			}
			TimeWindow tw2;
			if (o.negociatedWindow!=null) {
				tw2 = o.negociatedWindow;
			}
			else {
				tw2 = o.trip.getTimeWindow();
			}
			return tw1.preferredStart.compareTo(tw2.preferredStart);
		}
		
	}

}