/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.agent;

import be.uhasselt.imob.carpooling.util.AgentType;
import be.uhasselt.imob.carpooling.util.Gender;

/** This class describes the agent capabilities.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class AgentProfile {

	private final Gender gender;
	private final int age;
	
	private AgentType agentType = null;
	private boolean hasVehicle = false;
	private boolean hasDriverLicense = false;
	private boolean smoking;
	
	/**
	 * @param gender
	 * @param age
	 */
	public AgentProfile(Gender gender, int age) {
		assert(gender!=null);
		assert(age>=1);
		this.gender = gender;
		this.age = age;
	}
	
	/** Replies the gender of the agent.
	 * 
	 * @return the gender of the agent.
	 */
	public Gender getGender() {
		return this.gender;
	}
	
	/** Replies the age of the agent.
	 * 
	 * @return the age of the agent.
	 */
	public int getAge() {
		return this.age;
	}
	
	/**
	 * @param agentType the agentType to set
	 */
	public void setAgentType(AgentType agentType) {
		this.agentType = agentType;
	}
	
	/**
	 * @return the agentType
	 */
	public AgentType getAgentType() {
		return this.agentType;
	}
	
	/**
	 * @param hasVehicle
	 */
	public void setHasVehicle(boolean hasVehicle) {
		this.hasVehicle = hasVehicle;
	}
	
	/**
	 * @return <code>true</code> if the agent owns a vehicle;
	 * <code>false</code> otherwise.
	 */
	public boolean hasVehicle() {
		return this.hasVehicle;
	}
	
	/**
	 * @param hasDriverLicense
	 */
	public void setDriverLicense(boolean hasDriverLicense) {
		this.hasDriverLicense = hasDriverLicense;
	}
	
	/**
	 * @return <code>true</code> if the agent owns a driver license;
	 * <code>false</code> otherwise.
	 */
	public boolean hasDriverLicense() {
		return this.hasDriverLicense;
	}

	/**
	 * @param isSmoking
	 */
	public void setSmoking(boolean isSmoking) {
		this.smoking = isSmoking;
	}
	
	/**
	 * @return <code>true</code> if the agent is smoking;
	 * <code>false</code> otherwise.
	 */
	public boolean isSmoking() {
		return this.smoking;
	}

}
