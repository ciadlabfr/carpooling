/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.agent;

/** List of the activities of the agents.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public enum AgentActivity {

	// INITIALISATION
	
	/** Agent is registering its periodic trips.
	 */
	S0_TRIP_REGISTRATION,

	// BEGIN OF DAY
	
	/** Agent is selecting its mode.
	 */
	S1_SELECTING_MODE,
	
	/** Searching carpooling partner.
	 */
	S1_SEARCHING_PARTNER,
	
	/** Negociating the carpooling activity.
	 */
	S1_NEGOCIATING,
	
	// TRAVELLING
	
	/** Proceed the carpooling.
	 */
	S2_CARPOOLING,
	
	/** Proceed the single travelling.
	 */
	S2_SINGLE_TRAVELLING,

	// END OF DAY

	/** Waiting for feedback on carpooling.
	 */
	S3_FEEDBACKING;

	/** Replies if the activity is at the begin of the day.
	 * 
	 * @return <code>true</code> if the activity is at the begin of the day.
	 */
	public boolean isBeginOfDayActivity() {
		return (this==S1_NEGOCIATING || this==S1_SEARCHING_PARTNER || this==S1_SELECTING_MODE);
	}
	
	/** Replies if the activity is at the begin of the day.
	 * 
	 * @return <code>true</code> if the activity is at the begin of the day.
	 */
	public boolean isDayActivity() {
		return (this==S2_CARPOOLING || this==S2_SINGLE_TRAVELLING);
	}

	/** Replies if the activity is at the begin of the day.
	 * 
	 * @return <code>true</code> if the activity is at the begin of the day.
	 */
	public boolean isEndOfDayActivity() {
		return (this==S3_FEEDBACKING);
	}

}
