/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.agent;

/** List of the specific cells of memory supported by a carpooling agent.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public enum CarpoolingAgentMemoryCell {

	/** Agent activity.
	 */
	ACTIVITY,
	
	/** Environment.
	 */
	ENVIRONMENT,
	
	/** Followed path.
	 */
	PATH,
	
	/** Current trip.
	 */
	TRIP,
	
	/** Negociated trip window.
	 */
	NEGOCIATED_WINDOW,
	
	/** Negociation failure.
	 */
	NEGOCIATION_FAILURE,

	/** Carpooling partners.
	 */
	CARPOOLING_PARTNERS,
	
	/** Travel time;
	 */
	TRAVEL_TIME;
	
}
