/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling;

import org.arakhne.afc.progress.Progression;
import org.arakhne.afc.ui.swing.progress.ProgressBarModel;
import org.arakhne.afc.vmutil.locale.Locale;
import org.janusproject.kernel.Kernel;
import org.janusproject.kernel.agent.Kernels;
import org.janusproject.kernel.logger.LoggerUtil;
import org.janusproject.kernel.probe.IndividualProbe;

import be.uhasselt.imob.carpooling.agent.CarpoolingAgent;
import be.uhasselt.imob.carpooling.agent.CarpoolingAgentBodyFactory;
import be.uhasselt.imob.carpooling.datainput.DataInput;
import be.uhasselt.imob.carpooling.datainput.DataSet;
import be.uhasselt.imob.carpooling.datainput.GenericDataInput;
import be.uhasselt.imob.carpooling.datainput.IndividualDailyDescription;
import be.uhasselt.imob.carpooling.environment.DefaultEnvironment;
import be.uhasselt.imob.carpooling.kernel.agent.CarpoolingKernelAgentFactory;
import be.uhasselt.imob.carpooling.ui.GISViewer;
import be.uhasselt.imob.carpooling.ui.LightProgress;
import be.uhasselt.imob.carpooling.ui.SimulationController;
import be.uhasselt.imob.carpooling.ui.SimulationParameterFrame;
import be.uhasselt.imob.carpooling.util.SimulationLogger;
import fr.utbm.set.geom.bounds.bounds1d5.BoundingRect1D5;
import fr.utbm.set.geom.system.CoordinateSystem2D;
import fr.utbm.set.geom.system.CoordinateSystem3D;
import fr.utbm.set.geom.system.CoordinateSystemConstants;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.jasim.environment.interfaces.internalevents.JasimSimulationListener;
import fr.utbm.set.jasim.environment.model.influencereaction.EnvironmentalAction1D5;
import fr.utbm.set.jasim.environment.model.world.Entity1D5;
import fr.utbm.set.jasim.environment.model.world.MobileEntity1D5;
import fr.utbm.set.ui.window.JProgressBarDialog;

/** This is the main class of the carpooling simulator.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class CarpoolingSimulator {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// Force the use of the GIS coordinate system
		CoordinateSystem2D.setDefaultCoordinateSystem(CoordinateSystemConstants.GIS_2D);
		CoordinateSystem3D.setDefaultCoordinateSystem(CoordinateSystemConstants.GIS_3D);

		SimulationParameterFrame parameters = new SimulationParameterFrame();
		parameters.setVisible(true);
		if (parameters.isCanceled()) System.exit(0); // exit application
		parameters.saveUserPreferences();

		// Initialize outputs
		SimulationLogger.initialize(parameters);

		JProgressBarDialog progressDialog = new JProgressBarDialog(null,
				Locale.getString("LAUNCHING_PROGRESS")); //$NON-NLS-1$
		ProgressBarModel globalProgress = new ProgressBarModel(
				progressDialog.getProgressBar());
		progressDialog.setProgressBarModel(globalProgress);
		progressDialog.centerDialog();
		progressDialog.setAlwaysOnTop(true);
		progressDialog.setModal(false);
		progressDialog.setVisible(true);

		globalProgress.setProperties(0, 0, 500000, false);
		
		// Read the data input
		Progression loadingProgress = globalProgress.subTask(300000);
		DataInput di = new GenericDataInput();
		//DataInput di = new DebugDataInput();
		DataSet dataSet = di.read(parameters, loadingProgress);
		globalProgress.ensureNoSubTask();

		// Show the GIS viewer
		SimulationController viewer;
		JasimSimulationListener<EnvironmentalAction1D5, Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>> listener;
		if (parameters.isUIDisplayed()) {
			GISViewer gisviewer = GISViewer.show(dataSet.roadNetwork);
			viewer = gisviewer;
			listener = gisviewer.getSimulationListener();
		}
		else {
        	LoggerUtil.setLoggingEnable(false); // Disable logging
			LightProgress p = new LightProgress(dataSet.startDate, dataSet.endDate);
			viewer = p;
			listener = p;
			p.setVisible(true);
		}

		// Launching agents
		Progression launchingProgress = globalProgress.subTask(200000);
		Kernels.setPreferredKernelFactory(new CarpoolingKernelAgentFactory(
				dataSet.startDate, dataSet.endDate,
				parameters.getDrivingSimulationStep(),
				dataSet.roadNetwork,
				listener));
		Kernel kernel = Kernels.get();
		{
			IndividualProbe p = kernel.getProbeManager().createProbe(kernel.getAddress());
			CarpoolingAgentBodyFactory bodyFactory;
			do {
				bodyFactory = p.getProbeValue("bodyFactory", CarpoolingAgentBodyFactory.class); //$NON-NLS-1$
				Thread.yield();
			}
			while (bodyFactory==null);
			p.releaseProbe();
			
			DefaultEnvironment environment = new DefaultEnvironment(
					parameters.getAverageFuelCost(),
					dataSet.roadNetwork);

			launchingProgress.setProperties(0, 0, 
					dataSet.population.size(), false,
					Locale.getString("LAUNCHING_AGENTS")); //$NON-NLS-1$

			for(IndividualDailyDescription dt : dataSet.population) {
				if (!dt.trips.isEmpty()) {
					kernel.submitLightAgent(
							new CarpoolingAgent(parameters, bodyFactory),
							dt.name,
							dt.gender,
							dt.age,
							dt.isDriver,
							dt.hasCar,
							environment,
							dt.trips);
				}
				launchingProgress.increment();
			}
			
			launchingProgress.end();
		}

		globalProgress.end();
		progressDialog.setProgressBarModel(null);
		
		viewer.showController();

		// Wait for termination
		kernel.waitUntilTermination();

		viewer.hideController();

		SimulationLogger.close();

		System.exit(0);
	}
		
}
