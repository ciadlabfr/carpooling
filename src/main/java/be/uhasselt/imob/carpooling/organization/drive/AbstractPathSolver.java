/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.organization.drive;

import fr.utbm.set.geom.object.Direction1D;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.gis.road.SubRoadNetwork;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.gis.road.primitive.RoadConnection;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.jasim.environment.interfaces.body.perceptions.GroundPerception1D5;

/**
 * Pemits to update the path followed by a driver.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public abstract class AbstractPathSolver implements PathSolver {

	/** {@inheritDoc}
	 */
	@Override
	public final boolean updatePath(RoadPath path, Point1D5 currentPosition, GroundPerception1D5 groundPerception, Direction1D viewDirection) {
		boolean removeFirst = false;
		RoadSegment currentSegment = (RoadSegment)currentPosition.getSegment();
		RoadConnection entryPoint = viewDirection.isSegmentDirection() ? currentSegment.getBeginPoint() : currentSegment.getEndPoint();

		if (path.isEmpty()) {
			path.add(currentSegment, entryPoint);
		}
		else {
			// Remove the first segment on the path if it is leaved away.
			{
				RoadSegment sgmt = path.getFirstSegment();
				RoadConnection pt = path.getFirstPoint();
				if (!sgmt.equals(currentSegment) || !pt.equals(entryPoint)) {
					path.removeBefore(currentSegment, entryPoint);
					removeFirst = true;
				}
			}

			// Update the last segment on the path, if necessary
			{
				RoadSegment lastSegment = path.getLastSegment();
				RoadConnection finalPoint = path.getLastPoint();

				SubRoadNetwork perceivedRoads = groundPerception.getRoads();

				RoadSegment theSegment = perceivedRoads.getRoadSegment(lastSegment.getGeoId());

				if (theSegment!=null) {

					RoadConnection theConnection;
					if (lastSegment.getBeginPoint().equals(finalPoint)) {
						theConnection = theSegment.getBeginPoint();
					}
					else {
						theConnection = theSegment.getEndPoint();
					}

					if (!theConnection.isFinalConnectionPoint()) {
						RoadSegment selectedSegment = selectSegment(
								theSegment,
								theConnection);

						if (selectedSegment!=null) {
							path.add(selectedSegment);
						}
					}

				}

			}
		}

		// Treat the cul-de-sac ways
		if (path.size()==1 && path.isCulDeSacWay() && isTurnBackAtCulDeSac()) {
			path.add(currentSegment);
		}		
		return removeFirst;
	}

	/** Replies if this solver allow to turn back when reached a cul-de-sac.
	 * 
	 * @return <code>true</code> if this solver is allowed to turn bak when reached a
	 * cul-de-sac way, otherwise <code>false</code>.
	 */
	protected abstract boolean isTurnBackAtCulDeSac();

	/** Select a segment to follow.
	 *
	 * @param lastSegment is the last reached segment in the path.
	 * @param connection is the connection from which a road segment may be selected. 
	 * @return the selected segment or <code>null</code>.
	 */
	protected abstract RoadSegment selectSegment(
			RoadSegment lastSegment,
			RoadConnection connection);


}