/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.organization.drive;

import java.util.Iterator;

import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;

import fr.utbm.set.attr.AttributeException;
import fr.utbm.set.geom.GeometryUtil;
import fr.utbm.set.geom.bounds.bounds1d5.Bounds1D5;
import fr.utbm.set.geom.object.Direction1D;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.gis.road.path.CrossRoad;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.gis.road.primitive.RoadConnection;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.jasim.environment.interfaces.body.perceptions.KinematicEntity;
import fr.utbm.set.jasim.environment.interfaces.body.perceptions.Perception1D5;
import fr.utbm.set.jasim.environment.interfaces.body.perceptions.Perceptions;
import fr.utbm.set.math.MathUtil;

/**
 * Pemits to update the speed/acceleration for the car of a driver according to
 * the IDM model.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class IDMSpeedSolver implements SpeedSolver {

    /**
     * Global max speed for this vehicle
     */
    protected double globalMaxSpeed;

    /**
     * Max accel of the vehicle
     */
    protected double maxAcceleration;
    /**
     * Max deceleration of the vehicle
     */
    protected double maxDeceleration; // FIXME: should not be max, but
                                      // "comfortable" deceleration for IDM

    /**
     * Minimal distance between this vehicle and the others
     */
    protected double minimalDistance;

    /**
     * Reaction time of the driver
     */
    protected double reactionTime;

    /**
     * Path the vehicle/driver follows
     */
    protected RoadPath path;

    /**
     * Bounds of the vehicle
     */
    protected Bounds1D5<?> vehicleBounds;

    /**
     * Coefficient applied to the turn radius computed to tweak it
     */
    protected double radiusCoeff = 0.1d;

    protected double IDM_DELTA = 4d;

    // DEBUG
    public double desiredSpeed;
    public double obstacleDistance;
    public double obstacleSpeed;
    public double radiusOfTurn;
    public double idmS;
    public double acceleration;

    public IDMSpeedSolver(RoadPath path, Bounds1D5<?> vehicleBounds, double maxVehicleSpeed, double maxAcceleration,
            double maxDeceleration, double minimalDistance, double reactionTime) {

        this.path = path;
        this.globalMaxSpeed = maxVehicleSpeed;
        this.maxAcceleration = maxAcceleration;
        this.maxDeceleration = maxDeceleration;
        this.minimalDistance = minimalDistance;
        this.reactionTime = reactionTime;
        this.vehicleBounds = vehicleBounds;
    }

    @Override
    public double computeAcceleration(Perceptions<Perception1D5> perceptions, Point1D5 currentPosition, Direction1D currentDirection,
            double currentSpeed, Point1D5 destinationPosition) {

        RoadSegment rs = (RoadSegment) currentPosition.getSegment();
        Float maxspeed = (float) this.globalMaxSpeed;
        try {
        	if(rs.hasAttribute("segmentSpeed")){
        		maxspeed = rs.getAttributeAsFloat("segmentSpeed");
        	}
		} catch (AttributeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    		
        double desiredSpeed = Math.min(maxspeed,  this.getMaxSpeedForNextTurn(currentPosition, currentSpeed, currentDirection));

        // initialize with destination distance
        double obstacleDistance = destinationPosition.distance(currentPosition);
        double obstacleSpeed = 0d;
        
        // FIXME : check if perceptions are ordered

        for (Iterator<Perception1D5> iter = perceptions.getDynamicPercepts(); iter.hasNext();) {
            Perception1D5 obstacle = iter.next();

            if (obstacle.getPerceivedObjectCurvilineDistance() < obstacleDistance && isInWay(obstacle)) {
                obstacleDistance = obstacle.getPerceivedObjectCurvilineDistance();

                if (mustYieldPriority(obstacle)) {
                    obstacleSpeed = 0d;
                } else {
                    obstacleSpeed = ((KinematicEntity) obstacle.getPerceivedObject()).getLinearSpeed();
                }
            }
        }

        for (Iterator<Perception1D5> iter = perceptions.getStaticPercepts(); iter.hasNext();) {
            Perception1D5 obstacle = iter.next();

            if (obstacle.getPerceivedObjectCurvilineDistance() < obstacleDistance && isInWay(obstacle)) {
                obstacleDistance = obstacle.getPerceivedObjectCurvilineDistance();

                if (mustYieldPriority(obstacle)) {
                    obstacleSpeed = 0d;
                } else {
                    obstacleSpeed = ((KinematicEntity) obstacle.getPerceivedObject()).getLinearSpeed();
                }
            }
        }

        // FIXME
        this.desiredSpeed = desiredSpeed;
        this.obstacleDistance = obstacleDistance;
        this.obstacleSpeed = obstacleSpeed;

        return this.computeIDMAcceleration(currentSpeed, desiredSpeed, obstacleSpeed, obstacleDistance);
    }

    public double getMaxSpeedForNextTurn(Point1D5 currentPosition, double currentSpeed, Direction1D currentDirection) {
        if (this.path == null || this.path.size() < 2) {
            return this.globalMaxSpeed;
        }

        RoadSegment firstSegment = this.path.get(0);
        RoadSegment secondSegment = this.path.get(1);

        double radius;

        if (firstSegment.equals(secondSegment)) {
            radius = 180d;
        } else {
            RoadConnection connection = firstSegment.getSharedConnectionWith(secondSegment);

            assert connection != null : "no shared connection between both segments";

            Point2d p0 = firstSegment.getOtherSidePoint(connection).getPoint();
            Point2d p1 = connection.getPoint();
            Point2d p2 = secondSegment.getOtherSidePoint(connection).getPoint();

            radius = Math.abs(GeometryUtil.signedAngle(p0.x - p1.x, p0.y - p1.y, p2.x - p1.x, p2.y - p1.y));
        }

        // FIXME
        this.radiusOfTurn = radius;

        // http://fr.wikipedia.org/wiki/Signalisation_verticale_des_virages_en_France#Vitesse_dans_le_virage
        double result = Math.min(102d / (1d + 346d / Math.pow(radius * 180 / Math.PI, 1.5)), this.globalMaxSpeed);

        return result;
    }

    /**
     * Utility method so we don't have to pass all constant parameters for the
     * vehicle to IntelligentDriverModel each time
     * 
     * @param currentSpeed
     * @param desiredSpeed
     * @param obstacleSpeed
     * @param obstacleDistance
     * @return the acceleration the vehicle should have with these parameters
     */
    private double computeIDMAcceleration(double currentSpeed, double desiredSpeed, double obstacleSpeed,
            double obstacleDistance) {

        if (desiredSpeed == 0)
            return 0;

        double acceleration;

        if (obstacleDistance >= 200) {
            // Free Driver Model
            acceleration = this.maxAcceleration * (1 - Math.pow(currentSpeed / desiredSpeed, IDM_DELTA));

            // FIXME
            this.idmS = Double.NaN;
        } else {
            // Intelligent Driver Model
            double s = (this.minimalDistance + currentSpeed * (this.reactionTime + (currentSpeed - obstacleSpeed)))
                    / (2 * obstacleDistance * Math.sqrt(this.maxAcceleration * this.maxDeceleration));

            acceleration = -this.maxAcceleration * Math.pow(s / obstacleDistance, 2);

            // FIXME
            this.idmS = s;
        }

        // FIXME
        this.acceleration = acceleration;

        double result = MathUtil.clamp(acceleration, -this.maxDeceleration, this.maxAcceleration);

        return result;
    }

    /**
     * Computes if the given perception contains a vehicle for which we must
     * yield the right of way
     * 
     * @param p the perception
     * @return <code>true</code> if we must yield the right of way to the
     *         vehicle of the perception
     */
    private boolean mustYieldPriority(Perception1D5 p) {
        if (p.isInFront() && !p.isSameDirection()) {
            RoadPath pathToVehicle = p.getPathToPerceivedObject(RoadPath.class);
            CrossRoad otherVehicleCrossroad = pathToVehicle.getFirstJunctionPoint();
            CrossRoad thisCrossroad = this.path.getFirstJunctionPoint();

            // The next intersection of the desired path must always be the same
            // as the one of the path to the "obstacle" vehicle
            // The entering segments must also be the same
            assert pathToVehicle.getFirstCrossRoad().equals(path.getFirstCrossRoad());
            assert thisCrossroad.enteringSegment.equals(otherVehicleCrossroad.enteringSegment);

            if (this.getAngle(thisCrossroad.enteringSegment, thisCrossroad.exitingSegment) > this.getAngle(
                    otherVehicleCrossroad.enteringSegment, otherVehicleCrossroad.exitingSegment)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the angle between two connected segments
     * 
     * @param firstSegment the first segment
     * @param secondSegment the second segment
     * @return the signed angle between the two, in degrees
     */
    private double getAngle(RoadSegment firstSegment, RoadSegment secondSegment) {
        RoadConnection connection = firstSegment.getSharedConnectionWith(secondSegment);
        assert connection != null;

        Point2d connecPt = connection.getPoint();
        Point2d firstPt = firstSegment.getOtherSidePoint(connection).getPoint();
        Point2d lastPt = secondSegment.getOtherSidePoint(connection).getPoint();

        return GeometryUtil.signedAngle(connecPt.x - firstPt.x, connecPt.y - firstPt.y, lastPt.x - connecPt.x, lastPt.y - connecPt.y);
    }

    /**
     * Returns if the given perception's object is in front and in the same row
     * as us, meaning that we will have to break/avoid it
     * 
     * @param vehicleBounds
     * @param p the perception
     * @return <code>true</code> if the perception's object is in our way
     */
    private boolean isInWay(Perception1D5 p) {
        return p.isInFront() && lateralIntersects(p.getPerceivedObject().getBounds1D5());
    }

    /**
     * Computes if given bounds intersect laterally with our bounds
     * 
     * @param vehicleBounds
     * @param b other bounds
     * @return <code>true</code> if the bounds intersect laterally
     */
    private boolean lateralIntersects(Bounds1D5<?> b) {
        // TODO: optimized ? perhaps better to work with a Bounds1D5 object ?
        double halfWidth = b.getLateralSize() / 2d;
        double leftBound = b.getJutting() - halfWidth;
        double rightBound = b.getJutting() + halfWidth;

        return vehicleBounds.intersects(new Point1D5(b.getSegment(), vehicleBounds.getCenterY(), leftBound))
                || vehicleBounds.intersects(new Point1D5(b.getSegment(), vehicleBounds.getCenterY(), rightBound));
    }
}