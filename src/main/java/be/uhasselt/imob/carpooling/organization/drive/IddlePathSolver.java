/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.organization.drive;

import fr.utbm.set.geom.object.Direction1D;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.gis.road.primitive.RoadConnection;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.jasim.environment.interfaces.body.perceptions.GroundPerception1D5;


/**
 * Update the path by removing only the segments that are encountered.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class IddlePathSolver implements PathSolver {

	/**
	 */
	public IddlePathSolver() {
		//
	}

	/** {@inheritDoc}
	 */
	@Override
	public boolean updatePath(RoadPath path, Point1D5 currentPosition, GroundPerception1D5 groundPerception, Direction1D viewDirection) {
		RoadSegment currentSegment = (RoadSegment)currentPosition.getSegment();
		RoadConnection entryPoint = viewDirection.isSegmentDirection() ? currentSegment.getBeginPoint() : currentSegment.getEndPoint();

		if (path.isEmpty()) {
			path.add(currentSegment, entryPoint);
		}
		else {
			// Remove the first segment on the path if it is leaved away.
			RoadSegment sgmt = path.getFirstSegment();
			RoadConnection pt = path.getFirstPoint();
			if (!sgmt.equals(currentSegment) || !pt.equals(entryPoint)) {
				boolean success = path.removeBefore(currentSegment, entryPoint);
				assert(success) : "invalid path update"; //$NON-NLS-1$
				return true;
			}
		}
		return false;
	}

}