/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.organization.drive;

import fr.utbm.set.geom.object.Direction1D;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.jasim.environment.interfaces.body.perceptions.GroundPerception1D5;

/**
 * Pemits to update the path followed by a driver.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public interface PathSolver {

	/** Compute the path of the solver and reply it.
	 *  
	 * @param path is the path instance to update.
	 * @param currentPosition
	 * @param groundPerception
	 * @param viewDirection
	 * @return <code>true</code> if the first segment was removed from the path;
	 * <code>false</code> otherwise.
	 */
	public boolean updatePath(RoadPath path, Point1D5 currentPosition, GroundPerception1D5 groundPerception, Direction1D viewDirection);

}