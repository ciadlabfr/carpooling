/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.organization.negociation;

import org.janusproject.kernel.message.Message;

import be.uhasselt.imob.carpooling.schedule.TimeWindow;
import be.uhasselt.imob.carpooling.schedule.Trip;

/**
 * Proposal message.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class ProposalMessage extends Message {
	
	private static final long serialVersionUID = -958402342852097602L;

	/** Trip.
	 */
	public final Trip trip;
	
	/** Time window.
	 */
	public final TimeWindow timeWindow;

	/**
	 * @param trip
	 * @param timeWindow
	 */
	public ProposalMessage(Trip trip, TimeWindow timeWindow) {
		this.trip = trip;
		this.timeWindow = timeWindow;
	}
	
}
