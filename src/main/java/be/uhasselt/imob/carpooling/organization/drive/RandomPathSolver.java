/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.organization.drive;

import org.janusproject.kernel.util.random.RandomNumber;

import fr.utbm.set.gis.road.primitive.RoadConnection;
import fr.utbm.set.gis.road.primitive.RoadSegment;


/**
 * Update a path randomly.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class RandomPathSolver extends AbstractPathSolver {

	/**
	 */
	public RandomPathSolver() {
		//
	}

	@Override
	protected boolean isTurnBackAtCulDeSac() {
		return true;
	}

	@Override
	protected RoadSegment selectSegment(
			RoadSegment lastSegment,
			RoadConnection connection) {
		RoadSegment sgmt;
		do {
			int n = RandomNumber.nextInt(connection.getConnectedSegmentCount());
			sgmt = connection.getConnectedSegment(n);
		}
		while (sgmt.equals(lastSegment));
		return sgmt;
	}

}