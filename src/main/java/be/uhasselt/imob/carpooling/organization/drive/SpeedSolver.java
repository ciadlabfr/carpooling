/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.organization.drive;

import fr.utbm.set.geom.object.Direction1D;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.jasim.environment.interfaces.body.perceptions.Perception1D5;
import fr.utbm.set.jasim.environment.interfaces.body.perceptions.Perceptions;

/**
 * Pemits to update the speed/acceleration for the car of a driver.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public interface SpeedSolver {

    /**
     * Compute the instant acceleration
     * 
     * @param perceptionList is the list of the perceived objects around the
     *            car.
     * @param path is the path to follow.
     * @param pos is the current car position.
     * @param currentSpeed is the current car speed.
     * @return the acceleration.
     */
    public double computeAcceleration(Perceptions<Perception1D5> perceptions, Point1D5 currentPosition, Direction1D currentDirection,
            double currentSpeed, Point1D5 destinationPosition);

}