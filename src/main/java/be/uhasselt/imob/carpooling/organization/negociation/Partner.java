/* 
 * $Id$
 * 
 * Copyright (C) 2013
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.organization.negociation;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.janusproject.kernel.address.AgentAddress;
import org.janusproject.kernel.crio.capacity.CapacityContext;
import org.janusproject.kernel.crio.core.HasAllRequiredCapacitiesCondition;
import org.janusproject.kernel.crio.core.Role;
import org.janusproject.kernel.crio.core.RoleAddress;
import org.janusproject.kernel.crio.role.RoleActivationPrototype;
import org.janusproject.kernel.crio.role.RolePlayingEvent;
import org.janusproject.kernel.crio.role.RolePlayingListener;
import org.janusproject.kernel.message.Message;
import org.janusproject.kernel.status.Status;

import be.uhasselt.imob.carpooling.agent.CarpoolingAgent.CarpoolingFailure;
import be.uhasselt.imob.carpooling.agent.CarpoolingAgentMemoryCell;
import be.uhasselt.imob.carpooling.capacity.TimeWindowEvaluator;
import be.uhasselt.imob.carpooling.capacity.TripProposalEvaluator;
import be.uhasselt.imob.carpooling.schedule.TimeWindow;
import be.uhasselt.imob.carpooling.schedule.Trip;

/**
 * Partner in the negotiation organization.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
@RoleActivationPrototype(
		fixedParameters=Boolean.class
		)
public class Partner extends Role {
	
	private static final int WAITING_TIMEOUT = 10;
	
	private final RolePlayingListener listener = new RolePlayingListener() {
		@SuppressWarnings("synthetic-access")
		@Override
		public void roleTaken(RolePlayingEvent event) {
			if (Partner.class.equals(event.getRole())
				&& !event.getRoleAddress().equals(getAddress())) {
				sendProposal(event.getRoleAddress());
			}
		}
		
		@SuppressWarnings("synthetic-access")
		@Override
		public void roleReleased(RolePlayingEvent event) {
			if (Partner.class.equals(event.getRole())
				&& !event.getRoleAddress().equals(getAddress())) {
				removePartner(event.getRoleAddress());
			}
		}
	};
	
	private boolean isInitiator = false;
	private Trip trip = null;
	private TimeWindow timeWindow = null;

	private final Set<AgentAddress> notifiedPartners = new TreeSet<AgentAddress>();
	private final Set<RoleAddress> notifiablePartners = new TreeSet<RoleAddress>();
	private Map<RoleAddress,TimeWindow> proposals = null;
	
	private int waitingDuration = WAITING_TIMEOUT;

	private int stepInProtocol = -1;
	
	/**
	 */
	@SuppressWarnings("unchecked")
	public Partner() {
		super();
		addObtainCondition(new HasAllRequiredCapacitiesCondition(Arrays.asList(TimeWindowEvaluator.class, TripProposalEvaluator.class)));
	}
	
	/** {@inheritDoc}
	 */
	@Override
	public Status activate(Object... params) {
		this.isInitiator = (Boolean)params[0];
		
		this.trip = (Trip)getMemorizedData(CarpoolingAgentMemoryCell.TRIP.name());
		if (this.isInitiator) {
			this.proposals = new TreeMap<RoleAddress,TimeWindow>();
			getCurrentGroup().setPublicUserData("trip", this.trip); //$NON-NLS-1$
		}
		
		this.stepInProtocol = 0;
		
		getCurrentGroup().addRolePlayingListener(this.listener);
				
		return super.activate(params);
	}
	
	/** {@inheritDoc}
	 */
	@Override
	public Status end() {
		getCurrentGroup().removeRolePlayingListener(this.listener);
		return super.end();
	}

	/** {@inheritDoc}
	 */
	@Override
	public Status live() {
		if (this.isInitiator) {
			runInitiatorLifeLine();
		}
		else {
			runPartnerLifeLine();
		}
		return null;
	}
	
	private synchronized void sendProposal(RoleAddress role) {
		if (!this.notifiedPartners.contains(role.getPlayer())
			&& !this.notifiablePartners.contains(role.getPlayer())) {
			this.notifiablePartners.add(role);
			this.waitingDuration = WAITING_TIMEOUT;
		}
	}
	
	private synchronized void removePartner(RoleAddress role) {
		if (this.proposals!=null) this.proposals.remove(role);
		if (this.notifiedPartners!=null) this.notifiedPartners.add(role.getPlayer());
		this.waitingDuration = WAITING_TIMEOUT;
	}

	private TimeWindow getPreferredTimeWindow() {
		TimeWindow tw = this.timeWindow;
		if (tw==null) {
			tw = this.trip.getTimeWindow();
		}
		return tw;
	}
	
	@SuppressWarnings("unchecked")
	private synchronized void runInitiatorLifeLine() {
		if (!this.notifiablePartners.isEmpty()) {
			Iterator<RoleAddress> iterator = this.notifiablePartners.iterator();
			while (iterator.hasNext()) {
				RoleAddress ra = iterator.next();
				iterator.remove();
				ProposalMessage msg = new ProposalMessage(this.trip, getPreferredTimeWindow());
				sendMessage(ra, msg);
				this.waitingDuration = WAITING_TIMEOUT;
			}
		}
		switch(this.stepInProtocol) {
		case 0: // Wait for proposal answers
			for(Message msg : getMailbox()) {
				if (msg instanceof RefusalMessage) {
					removePartner((RoleAddress)msg.getSender());
				}
				else if (msg instanceof AcceptanceMessage) {
					this.proposals.put((RoleAddress)msg.getSender(), getPreferredTimeWindow());
				}
				else if (msg instanceof ProposalMessage) {
					this.proposals.put((RoleAddress)msg.getSender(), ((ProposalMessage)msg).timeWindow);
				}
				else {
					throw new IllegalStateException();
				}
			}
			if (!this.proposals.isEmpty() && 
				this.proposals.size()>=this.notifiedPartners.size()) {
				// All proposals received, wait for new member arrival
				if (this.waitingDuration<0) {
					this.stepInProtocol = 1;
				}
				else {
					--this.waitingDuration;
				}
			}
			else if (this.waitingDuration<0) {
				// No activity in the negociation group.
				if (this.proposals.isEmpty()) {
					this.stepInProtocol = -1;
					removeMemorizedData(CarpoolingAgentMemoryCell.NEGOCIATED_WINDOW.name());
					removeMemorizedData(CarpoolingAgentMemoryCell.CARPOOLING_PARTNERS.name());
					putMemorizedData(CarpoolingAgentMemoryCell.NEGOCIATION_FAILURE.name(),
							CarpoolingFailure.NO_PARTNER);
					leaveMe();
				}
				else {
					this.stepInProtocol = 1;
				}
			}
			else {
				--this.waitingDuration;
			}
			break;
		case 1: // All answers received, evaluate the time windows
			try {
				CapacityContext result = executeCapacityCall(TimeWindowEvaluator.class, this.trip, this.proposals.values());
				assert(result!=null);
				if (result.isFailed()) {
					this.stepInProtocol = 2;
				}
				else {
					this.timeWindow = (TimeWindow)result.getOutputValue();
					assert(this.timeWindow!=null);
					this.stepInProtocol = 3;
				}
			}
			catch(Exception e) {
				throw new RuntimeException(e);
			}
			break;
		case 2: // There is no solution according to the proposed time windows
			for(RoleAddress partner : this.proposals.keySet()) {
				sendMessage(partner, new RejectTripMessage(this.trip));
			}
			this.stepInProtocol = -1;
			removeMemorizedData(CarpoolingAgentMemoryCell.NEGOCIATED_WINDOW.name());
			removeMemorizedData(CarpoolingAgentMemoryCell.CARPOOLING_PARTNERS.name());
			putMemorizedData(CarpoolingAgentMemoryCell.NEGOCIATION_FAILURE.name(),
					CarpoolingFailure.NEGOCIATION_FAILURE);
			leaveMe();
			break;
		case 3: // Prepare for tripping
			Collection<AgentAddress> partners = (Collection<AgentAddress>)getMemorizedData(
					CarpoolingAgentMemoryCell.CARPOOLING_PARTNERS.name());
			partners.clear();
			for(RoleAddress partner : this.proposals.keySet()) {
				sendMessage(partner, new JoinTripMessage(this.trip, this.timeWindow));
				partners.add(partner.getPlayer());
			}
			putMemorizedData(CarpoolingAgentMemoryCell.NEGOCIATED_WINDOW.name(), 
					this.timeWindow);
			removeMemorizedData(CarpoolingAgentMemoryCell.NEGOCIATION_FAILURE.name());
			this.stepInProtocol = -1;
			leaveMe();
			break;
		default:
			throw new IllegalStateException();
		}
	}
	
	private void runPartnerLifeLine() {
		switch(this.stepInProtocol) {
		case 0: // wait for proposal
			for(ProposalMessage msg : getMessages(ProposalMessage.class)) {
				try {
					CapacityContext result = executeCapacityCall(
							TripProposalEvaluator.class,
							this.trip,
							msg.trip, msg.timeWindow);
					assert(result!=null);
					if (result.isFailed()) {
						sendMessage((RoleAddress)msg.getSender(), new RefusalMessage());
						this.stepInProtocol = -1;
						leaveMe();
					}
					else {
						TimeWindow tm = (TimeWindow)result.getOutputValue();
						if (tm.equals(msg.timeWindow)) {
							sendMessage((RoleAddress)msg.getSender(), new AcceptanceMessage());
						}
						else {
							sendMessage((RoleAddress)msg.getSender(), new ProposalMessage(msg.trip, tm));
						}
						this.stepInProtocol = 1;
					}
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
				break; // break the loop
			}
			break;
		case 1: // Wait for trip negotiation result
			for(Message msg : getMessages()) {
				if (msg instanceof JoinTripMessage) {
					JoinTripMessage jtm = (JoinTripMessage)msg;
					/*putMemorizedData(
							CarpoolingAgentMemoryCell.TRIP.name(),
							jtm.trip);*/
					putMemorizedData(
							CarpoolingAgentMemoryCell.NEGOCIATED_WINDOW.name(),
							jtm.timeWindow);
					this.stepInProtocol = -1;
					leaveMe();
				}
				else if (msg instanceof RejectTripMessage) {
					//removeMemorizedData("trip"); //$NON-NLS-1$
					removeMemorizedData(CarpoolingAgentMemoryCell.NEGOCIATED_WINDOW.name());
					this.stepInProtocol = -1;
					leaveMe();
				}
			}
			break;
		default:
			throw new IllegalStateException();
		}
	}

}
