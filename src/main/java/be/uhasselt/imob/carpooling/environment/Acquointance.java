/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.environment;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.janusproject.kernel.address.AgentAddress;

/** This class describes a relationship with another agent.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class Acquointance implements Comparable<Acquointance> {

	private final AgentAddress agent;
	private final AcquointanceType type;
	private Map<AgentAddress,Float> recommendations = null;
	
	/**
	 * @param agent is the agent supported by this acquoitance.
	 * @param type is the type of the acquoitance.
	 */
	Acquointance(AgentAddress agent, AcquointanceType type) {
		assert(agent!=null);
		assert(type!=null);
		this.agent = agent;
		this.type = type;
	}
	
	/** Replies the address of the agent supported
	 * by this acquoitance.
	 * @return the address of the agent
	 */
	public AgentAddress getAgent() {
		return this.agent;
	}
	
	/** Replies the type of the acquoitance.
	 * 
	 * @return the type of the acquoitance.
	 */
	public AcquointanceType getType() {
		return this.type;
	}
	
	/** Replies the recommenders.
	 * 
	 * @return the recommenders.
	 */
	public Set<AgentAddress> recommendedBy() {
		if (this.recommendations==null)
			return Collections.emptySet();
		return this.recommendations.keySet();
	}
	
	/** Replies the highest recommendation score.
	 * 
	 * @return the highest recommendation score, or 
	 * {@code 0} if no recommendation.
	 */
	public float getHighestScore() {
		float c = Float.NaN;
		if (this.recommendations!=null) {
			for(Float f : this.recommendations.values()) {
				if (Float.isNaN(c) || f.floatValue()>c) {
					c = f.floatValue();
				}
			}
		}
		if (Float.isNaN(c)) return 0;
		return c;
	}
	
	/** Replies the lowest recommendation score.
	 * 
	 * @return the lowest recommendation score, or 
	 * {@code 0} if no recommendation.
	 */
	public float getLowestScore() {
		float c = Float.NaN;
		if (this.recommendations!=null) {
			for(Float f : this.recommendations.values()) {
				if (Float.isNaN(c) || f.floatValue()<c) {
					c = f.floatValue();
				}
			}
		}
		if (Float.isNaN(c)) return 0;
		return c;
	}
	
	/** Replies the recommendations.
	 * 
	 * @return the recommendations.
	 */
	public Map<AgentAddress,Float> getRecommendations() {
		if (this.recommendations==null)
			return Collections.emptyMap();
		return Collections.unmodifiableMap(this.recommendations);
	}
	
	/** Replies the recommendation for a recommender.
	 * 
	 * @param recommender
	 * @return the score of {@code 0} if no recommendation.
	 */
	public float getRecommendation(AgentAddress recommender) {
		if (this.recommendations==null) return 0f;
		Float score = this.recommendations.get(recommender);
		if (score==null) return 0f;
		return score.floatValue();
	}

	/** Add a recommendation for the the acquointance.
	 * 
	 * @param recommender
	 * @param score
	 */
	public void addRecommendation(AgentAddress recommender, float score) {
		assert(recommender!=null);
		if (this.recommendations==null) {
			this.recommendations = new TreeMap<AgentAddress,Float>();
		}
	}

	/** Remove a recommendation for the the acquointance.
	 * 
	 * @param recommender
	 */
	public void removeRecommendation(AgentAddress recommender) {
		if (this.recommendations!=null) {
			this.recommendations.remove(recommender);
			if (this.recommendations.isEmpty()) {
				this.recommendations = null;
			}
		}
	}
	
	/** Remove all the recommendations.
	 */
	public void clearRecommendations() {
		this.recommendations = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo(Acquointance o) {
		if (o==null) return Integer.MAX_VALUE;
		int r = this.type.ordinal() - o.getType().ordinal();
		if (r!=0) return r;
		return this.agent.compareTo(o.getAgent());
	}

}
