/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.environment;

import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

import org.janusproject.kernel.address.AgentAddress;

/** This class describes a social network for an agent.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class SocialNetwork {

	private TreeSet<Acquointance> acquoitances = new TreeSet<Acquointance>();
	
	/**
	 */
	public SocialNetwork() {
		//
	}
	
	/** Replies the acquointances.
	 * 
	 * @return the acquointances.
	 */
	public Set<Acquointance> getAcquointances() {
		return Collections.unmodifiableSet(this.acquoitances);
	}
	
	/** Add an acquointance.
	 * 
	 * @param agent is the agent 
	 * @param type is the type of relation with the agent.
	 */
	public void addAcquointance(AgentAddress agent, AcquointanceType type) {
		Acquointance n = new Acquointance(agent, type);
		if (!this.acquoitances.contains(n)) {
			this.acquoitances.add(n);
		}
	}
	
	/** Remove an acquointance.
	 * 
	 * @param agent is the agent 
	 * @param type is the type of relation with the agent.
	 */
	public void removeAcquoitance(AgentAddress agent, AcquointanceType type) {
		this.acquoitances.remove(new Acquointance(agent, type));
	}

	/** Remove all the acquointances for the given agent.
	 * 
	 * @param agent is the agent 
	 */
	public void removeAcquoitances(AgentAddress agent) {
		Iterator<Acquointance> it = this.acquoitances.iterator();
		boolean found = false;
		while (it.hasNext()) {
			Acquointance a = it.next();
			if (a.getAgent().equals(agent)) {
				found = true;
				it.remove();
			}
			else if (found) {
				return; // according to the order of the acquoitances
			}
		}
	}

	/** Remove all the acquointances of the given type.
	 * 
	 * @param type is the type of relation with the agents.
	 */
	public void removeAcquoitances(AcquointanceType type) {
		Iterator<Acquointance> it = this.acquoitances.iterator();
		while (it.hasNext()) {
			Acquointance a = it.next();
			if (a.getType()==type) {
				it.remove();
			}
		}
	}
	
	/** Replies all the acquoitances of a given type.
	 * 
	 * @param type
	 * @return the iterator on the acquoitances.
	 */
	public Iterator<Acquointance> iterator(AcquointanceType type) {
		return new AcquointanceTypeIterator(this.acquoitances.iterator(), type);
	}

	/** Replies all the acquoitances for a given agent.
	 * 
	 * @param agent
	 * @return the iterator on the acquoitances.
	 */
	public Iterator<Acquointance> iterator(AgentAddress agent) {
		return new AcquointanceAgentIterator(this.acquoitances.iterator(), agent);
	}

	/** 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private static class AcquointanceTypeIterator implements Iterator<Acquointance> {

		private final Iterator<Acquointance> iterator;
		private final AcquointanceType type;
		private Acquointance next;
		
		/**
		 * @param iterator
		 * @param type
		 */
		public AcquointanceTypeIterator(Iterator<Acquointance> iterator, AcquointanceType type) {
			this.iterator = iterator;
			this.type = type;
			searchNext();
		}
		
		private void searchNext() {
			this.next = null;
			while (this.iterator.hasNext()) {
				Acquointance a = this.iterator.next();
				if (a.getType()==this.type) {
					this.next = a;
					return;
				}
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			return this.next!=null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Acquointance next() {
			Acquointance n = this.next;
			if (n==null) throw new NoSuchElementException();
			searchNext();
			return n;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}
	
	/** 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private static class AcquointanceAgentIterator implements Iterator<Acquointance> {

		private final Iterator<Acquointance> iterator;
		private final AgentAddress agent;
		private Acquointance next;
		
		/**
		 * @param iterator
		 * @param agent
		 */
		public AcquointanceAgentIterator(Iterator<Acquointance> iterator, AgentAddress agent) {
			this.iterator = iterator;
			this.agent = agent;
			searchNext();
		}
		
		private void searchNext() {
			this.next = null;
			while (this.iterator.hasNext()) {
				Acquointance a = this.iterator.next();
				if (a.getAgent().equals(this.agent)) {
					this.next = a;
					return;
				}
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			return this.next!=null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Acquointance next() {
			Acquointance n = this.next;
			if (n==null) throw new NoSuchElementException();
			searchNext();
			return n;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}

}
