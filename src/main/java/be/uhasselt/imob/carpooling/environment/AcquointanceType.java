/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.environment;

/** This class describes a relationship with another agent.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public enum AcquointanceType {

	/** The acquoitance is a colleague.
	 */
	COLLEAGUE,
	
	/** The acquoitance is a neightbor around the home.
	 */
	HOME_NEIGHTBOR,
	
	/** The acquoitance is a neightbor around the work location.
	 */
	WORK_NEIGHTBOR,

	/** The acquoitance is a friend.
	 */
	FRIEND,

	/** The acquoitance is a carpooling partner.
	 */
	PARTNER,

	/** The acquoitance is a member of the family.
	 */
	FAMILY;
	
}
