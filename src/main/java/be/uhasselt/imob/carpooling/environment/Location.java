/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.environment;

import java.io.Serializable;

import fr.utbm.set.geom.object.Point1D5;

/** This class describes a location in the environment.
 * The Location class is a read-only object.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class Location implements Cloneable, Serializable {

	private static final long serialVersionUID = 1530257543058752083L;

	private final Point1D5 gisPosition; 
	
	/**
	 * @param gisPosition is the position of the location on a GIS map.
	 */
	public Location(Point1D5 gisPosition) {
		this.gisPosition = gisPosition;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Location clone() {
		try {
			return (Location)super.clone();
		}
		catch (CloneNotSupportedException e) {
			throw new Error(e);
		}
	}
	
	/** Replies the position on a GIS map.
	 * 
	 * @return the GIs position.
	 */
	public Point1D5 getGISPosition() {
		return new Point1D5(this.gisPosition);
	}

	/** {@inheritDoc}
	 */
	@Override
	public String toString() {
		return this.gisPosition.toPoint2D().toString();
	}
	
}
