/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.environment;

import org.janusproject.kernel.address.AgentAddress;

import be.uhasselt.imob.carpooling.schedule.Trip;
import be.uhasselt.imob.carpooling.util.AgentType;
import fr.utbm.set.gis.road.primitive.RoadNetwork;

/**
 * This interface describes the environment
 * in which the agents are located.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public interface Environment {
	
	/** Replies the road network.
	 * 
	 * @return the road network.
	 */
	public RoadNetwork getRoadNetwork();

	/** Replies the fuel cost.
	 * 
	 * @return the fuel cost.
	 */
	public float getFuelCost();
	
	/** Register a trip in the public medias.
	 *
	 * @param agent
	 * @param type
	 * @param trip
	 */
	public void registerTrip(AgentAddress agent, AgentType type, Trip trip);

	/** Replies the agents that have a registered trip that is compatible with
	 * the given trip.
	 *
	 * @param trip
	 * @return the partners
	 */
	public Iterable<AgentAddress> getPartnersWithCompatibleTrips(Trip trip);

	/** Replies if the given agent has a registered trip which is compatible
	 * with the given trip.
	 *
	 * @param agent
	 * @param trip
	 * @return <code>true</code> if the agent has a compatible trip.
	 */
	public boolean hasCompatibleTrip(AgentAddress agent, Trip trip);

}
