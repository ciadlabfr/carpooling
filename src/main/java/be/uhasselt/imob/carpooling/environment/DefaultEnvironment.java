/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.environment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.janusproject.kernel.address.AgentAddress;

import be.uhasselt.imob.carpooling.schedule.Trip;
import be.uhasselt.imob.carpooling.util.AgentType;
import fr.utbm.set.gis.road.primitive.RoadNetwork;

/**
 * This interface describes the environment
 * in which the agents are located.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class DefaultEnvironment  implements Environment {
	
	private final float fuelCost;
	
	private final RoadNetwork roadNetwork;
	
	private final Map<AgentAddress,SortedSet<Trip>> driverTrips = new TreeMap<AgentAddress,SortedSet<Trip>>();
	private final Map<AgentAddress,SortedSet<Trip>> passengerTrips = new TreeMap<AgentAddress,SortedSet<Trip>>();
	
	/**
	 * @param fuelCost is the cost of the fuel.
	 * @param roadNetwork is the current road network.
	 */
	public DefaultEnvironment(float fuelCost, RoadNetwork roadNetwork) {
		this.fuelCost = fuelCost;
		this.roadNetwork = roadNetwork;
	}

	/** {@inheritDoc}
	 */
	@Override
	public float getFuelCost() {
		return this.fuelCost;
	}

	/** {@inheritDoc}
	 */
	@Override
	public void registerTrip(AgentAddress agent, AgentType type, Trip trip) {
		if (trip!=null && agent!=null && type!=null) {
			SortedSet<Trip> trips = null;
			switch(type) {
			case DRIVER:
				trips = this.driverTrips.get(agent);
				if (trips==null) {
					trips = new TreeSet<Trip>();
					this.driverTrips.put(agent, trips);
				}
				break;
			case PASSENGER:
				trips = this.passengerTrips.get(agent);
				if (trips==null) {
					trips = new TreeSet<Trip>();
					this.passengerTrips.put(agent, trips);
				}
				break;
			default:
			}
			if (trips!=null) {
				trips.add(trip);
			}
		}
	}

	/** {@inheritDoc}
	 */
	@Override
	public Iterable<AgentAddress> getPartnersWithCompatibleTrips(Trip trip) {
		List<AgentAddress> list = new ArrayList<AgentAddress>();
		for(Entry<AgentAddress,SortedSet<Trip>> e : this.passengerTrips.entrySet()) {
			for(Trip t : e.getValue()) {
				if (t.isSimilar(trip)) {
					list.add(e.getKey());
					break;
				}
			}
		}
		return list;
	}

	/** {@inheritDoc}
	 */
	@Override
	public boolean hasCompatibleTrip(AgentAddress agent, Trip trip) {
		SortedSet<Trip> trips = this.passengerTrips.get(agent);
		if (trips!=null) {
			for(Trip t : trips) {
				if (t.isSimilar(trip)) return true;
			}
		}
		return false;
	}

	/** {@inheritDoc}
	 */
	@Override
	public RoadNetwork getRoadNetwork() {
		return this.roadNetwork;
	}
	
}
