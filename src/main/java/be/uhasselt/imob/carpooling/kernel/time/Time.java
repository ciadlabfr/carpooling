/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.kernel.time;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

import fr.utbm.set.math.MeasureUnitUtil;

/** Describe a time in a day.
 * The Time class is a read-only object.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class Time implements Comparable<Time>, Cloneable, Serializable {

	private static final long serialVersionUID = 2493281306184388398L;
	
	private final DayOfWeek day;
	private final int hour;
	private final int min;

	/**
	 * @param day
	 * @param hour is the hour in [0;23] 
	 * @param min is the minute in [0:59]
	 */
	public Time(DayOfWeek day, int hour, int min) {
		assert(day!=null);
		DayOfWeek d = day;
		int h = hour;
		int m = min;
		
		while (m>=60) {
			m -= 60;
			++h;
		}
		while (m<0) {
			m = (60+m);
			--h;
		}
		
		while (h>=24) {
			h -= 24;
			d = d.next();
		}
		while (h<0) {
			h = (24+h);
			d = d.previous();
		}
		
		this.day = d;
		this.hour = h;
		this.min = min;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Time clone() {
		try {
			return (Time)super.clone();
		}
		catch (CloneNotSupportedException e) {
			throw new Error(e);
		}
	}
	
	/** Replies the day of the week.
	 * 
	 * @return the day of the week.
	 */
	public DayOfWeek getDay() {
		return this.day;
	}
	
	/** Replies the hour.
	 * 
	 * @return the hour.
	 */
	public int getHour() {
		return this.hour;
	}

	/** Replies the minute.
	 * 
	 * @return the minute.
	 */
	public int getMinute() {
		return this.min;
	}
	
	/** Replies if the given time is before this time in the week.
	 * 
	 * @param t
	 * @return <code>true</code> if <code>{@link #compareTo(Time)}&lt;0</code>;
	 * otherwise <code>false</code>.
	 */
	public boolean before(Time t) {
		return compareTo(t) < 0;
	}

	/** Replies if the given time is after this time in the week.
	 * 
	 * @param t
	 * @return <code>true</code> if <code>{@link #compareTo(Time)}&gt;0</code>;
	 * otherwise <code>false</code>.
	 */
	public boolean after(Time t) {
		return compareTo(t) > 0;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj==this) return true;
		if (obj instanceof Time) {
			return compareTo((Time)obj) == 0;
		}
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int h = 1;
		h = h * 37 + this.day.hashCode();
		h = h * 37 + this.hour;
		h = h * 37 + this.min;
		return h;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo(Time t) {
		assert(t!=null);
		int r = this.day.compareTo(t.getDay());
		if (r!=0) return r;
		r = this.hour - t.getHour();
		if (r!=0) return r;
		return this.min - t.getMinute();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return this.day.toString()
				+"@" //$NON-NLS-1$
				+toString(this.hour)
				+":" //$NON-NLS-1$
				+toString(this.min);
	}
	
	private static String toString(int v) {
		StringBuilder b = new StringBuilder();
		b.append(Integer.toString(v));
		while (b.length()<2) {
			b.insert(0, "0"); //$NON-NLS-1$
		}
		return b.toString();
	}
	
	/** Add the given amount of minutes to the current time.
	 * 
	 * @param mins
	 * @return the new minutes.
	 */
	public Time add(int mins) {
		GregorianCalendar cal = toCalendar();
		cal.add(Calendar.MINUTE, mins);
		int m = cal.get(Calendar.MINUTE);
		int h = cal.get(Calendar.HOUR_OF_DAY);
		DayOfWeek d = DayOfWeek.valueOf(cal);
		return new Time(d, h, m);
	}
	
	/** Compute the number of minutes between this time and
	 * the given time. 
	 * 
	 * @param t
	 * @return the minutes.
	 */
	public double distance(Time t) {
		GregorianCalendar c1 = toCalendar();
		GregorianCalendar c2 = t.toCalendar();
		long diff = Math.abs(c1.getTimeInMillis() - c2.getTimeInMillis());
		return MeasureUnitUtil.milli2unit(diff) / 60.;
	}

	/** Replies a calendar representation of this time.
	 * 
	 * @return the calendar.
	 */
	public GregorianCalendar toCalendar() {
		return new GregorianCalendar(2013, 2, this.day.ordinal()+4, this.hour, this.min);
	}

	/** Replies a calendar representation of this time.
	 * 
	 * @param year is the year.
	 * @param month is the month, 0 for january...
	 * @param day is the day in the month.
	 * @return the calendar.
	 */
	public Calendar toCalendar(int year, int month, int day) {
		return new GregorianCalendar(year, month, day, this.hour, this.min);
	}

	/** Compute the time between this time and the given time.
	 * A value of {@code 0} for interpolation is for this time.
	 * A value of {@code 1} for interpolation is for the <var>t</var>.
	 * 
	 * @param t
	 * @param interpolation is a value in {@code [0;1]}.
	 * @return the time
	 */
	public Time interpolate(Time t, float interpolation) {
		long t1 = this.day.ordinal()*1440l + this.hour*60l + this.min;
		long t2 = t.day.ordinal()*1440l + t.hour*60l + t.min;
		float i = (interpolation<0f) ? 0f : (interpolation>1f ? 1f : interpolation);
		long s = t1 + (long)((t2-t1)*i);
		int day = (int)(s / 1440l);
		s -= day * 1400l;
		int hour = (int)(s / 60l);
		s -= hour * 60l;
		return new Time(DayOfWeek.values()[day], hour, (int)s);
	}
	
}
