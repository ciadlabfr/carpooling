/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.kernel.agent;

import java.util.Calendar;
import java.util.EventListener;

import org.janusproject.kernel.agent.AgentActivator;
import org.janusproject.kernel.agent.KernelAgent;
import org.janusproject.kernel.agent.KernelAgentFactory;

import fr.utbm.set.geom.bounds.bounds1d5.BoundingRect1D5;
import fr.utbm.set.gis.road.primitive.RoadNetwork;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.jasim.environment.interfaces.internalevents.JasimSimulationListener;
import fr.utbm.set.jasim.environment.model.influencereaction.EnvironmentalAction1D5;
import fr.utbm.set.jasim.environment.model.world.Entity1D5;
import fr.utbm.set.jasim.environment.model.world.MobileEntity1D5;

/** Factory for a kernel agent that is able to synchronized
 * the heavy and lights agents according to
 * the carpooling application time schedule.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class CarpoolingKernelAgentFactory implements KernelAgentFactory {

	private final Calendar startDate;
	private final Calendar endDate;
	private final float timeStep;
	private final RoadNetwork roadNetwork;
	private final JasimSimulationListener<EnvironmentalAction1D5, Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>> listener;

	/**
	 * @param startDate is the starting date for the simulation.
	 * @param endDate is the endinging date for the simulation.
	 * @param timeStep is the duration of a step during the micro-simulation (in seconds).
	 * @param roadNetwork is the road network to use for simulation.
	 * @param listener is the listener on the environment's events.
	 */
	public CarpoolingKernelAgentFactory(Calendar startDate, Calendar endDate, float timeStep, RoadNetwork roadNetwork, JasimSimulationListener<EnvironmentalAction1D5, Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>> listener) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.timeStep = timeStep;
		this.roadNetwork = roadNetwork;
		this.listener = listener;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public KernelAgent newInstance(
			Boolean commitSuicide,
			AgentActivator activator,
			EventListener startUpListener,
			String applicationName) throws Exception {
		return new CarpoolingKernelAgent(
				this.startDate,
				this.endDate,
				this.timeStep,
				this.roadNetwork,
				this.listener,
				activator,
				startUpListener);
	}
	
}
