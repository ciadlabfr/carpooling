/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.kernel.time;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.janusproject.kernel.time.KernelTimeManager;

import fr.utbm.set.event.EventListenerCollection;
import fr.utbm.set.jasim.environment.time.AbstractClock;
import fr.utbm.set.jasim.environment.time.Clock;
import fr.utbm.set.jasim.environment.time.TimeManager;
import fr.utbm.set.jasim.environment.time.TimeManagerEvent;
import fr.utbm.set.jasim.environment.time.TimeManagerListener;

/** This class provides the tools to manage the time during
 * a carpoolig simulation.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class CarpoolingTimeManager extends AbstractClock implements KernelTimeManager, TimeManager {

	private final float timeStep; 	
	private final EventListenerCollection listeners = new EventListenerCollection();
	private final Calendar currentDate;
	private final Calendar lastDate;
	
	private long step = 0;

	/**
	 * @param startDate is the first date of the carpooling simulation.
	 * @param endDate is the last date of the carpooling simulation.
	 * @param timeStep is the duration of a step during the micro-simulation (in seconds).
	 */
	public CarpoolingTimeManager(Calendar startDate, Calendar endDate, float timeStep) {
		super(TimeUnit.SECONDS);
		this.timeStep = timeStep;
		this.currentDate = (Calendar)startDate.clone();
		this.lastDate = (Calendar)endDate.clone();
	}

	/** Replies if the date for the end of simulation was reached.
	 * 
	 * @return <code>true</code> if the simulation is supported to stop
	 * according to the dates; <code>false</code> otherwise.
	 */
	public boolean isSimulationTerminated() {
		return this.currentDate.after(this.lastDate);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized Date getCurrentDate() {
		return this.currentDate.getTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final float getCurrentTime() {
		return getCurrentTime(TimeUnit.MILLISECONDS);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized float getCurrentTime(TimeUnit unit) {
		long ms = this.currentDate.getTimeInMillis();
		switch(unit) {
		case DAYS:
			return ms/86400000f;
		case HOURS:
			return ms/3600000f;
		case MINUTES:
			return ms/60000f;
		case SECONDS:
			return ms/1000f;
		case MILLISECONDS:
			return ms;
		case MICROSECONDS:
			return ms*1000f;
		case NANOSECONDS:
			return ms*1000000f;
		default:
		}
		throw new IllegalArgumentException();
	}

	/** Move the time to the next day.
	 */
	public synchronized void moveToNextDay() {
		this.currentDate.add(Calendar.DAY_OF_MONTH, 1);
		this.currentDate.add(Calendar.HOUR, 0);
		this.currentDate.add(Calendar.MINUTE, 0);
		this.currentDate.add(Calendar.SECOND, 0);
		this.currentDate.add(Calendar.MILLISECOND, 0);
		++this.step;
		fireDateChanged();
	}

	/** Replies the current day of week.
	 * 
	 * @return the current day of week.
	 */
	public synchronized DayOfWeek getDayOfWeek() {
		return DayOfWeek.valueOf(this.currentDate);
	}

	/** Replies the current week time.
	 * 
	 * @return the current week time.
	 */
	public synchronized Time getWeekTime() {
		int hour = this.currentDate.get(Calendar.HOUR_OF_DAY);
		int minute = this.currentDate.get(Calendar.MINUTE);
		return new Time(getDayOfWeek(), hour, minute);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized String toString() {
		return this.currentDate.getTime().toString();
	}

	/** {@inheritDoc}
	 */
	@Override
	public void onEnvironmentBehaviourStarted() {
		// The environment model is starting its execution.
		// The agent behaviors are freezed during this execution.
	}

	/** {@inheritDoc}
	 */
	@Override
	public void onEnvironmentBehaviourFinished() {
		// The environment model is finishing its execution.
		// The agent behaviors are freezed during this execution.
		++this.step;
		this.currentDate.add(Calendar.MILLISECOND, (int)(this.timeStep*1000f));
		fireDateChanged();
	}

	private void fireDateChanged() {
		TimeManagerEvent event = new TimeManagerEvent(this);
		for(TimeManagerListener listener : this.listeners.getListeners(TimeManagerListener.class)) {
			listener.onSimulationClockChanged(event);
		}
	}
	
	/** {@inheritDoc}
	 */
	@Override
	public void addTimeManagerListener(TimeManagerListener listener) {
		this.listeners.add(TimeManagerListener.class, listener);
	}

	/** {@inheritDoc}
	 */
	@Override
	public void removeTimeManagerListener(TimeManagerListener listener) {
		this.listeners.remove(TimeManagerListener.class, listener);
	}

	/** {@inheritDoc}
	 */
	@Override
	public double getSimulationTime(TimeUnit desired_unit) {
		return getCurrentTime(desired_unit);
	}

	/** {@inheritDoc}
	 */
	@Override
	public double getSimulationStepDuration(TimeUnit desired_unit) {
		switch(desired_unit) {
		case DAYS:
			return this.timeStep/(24f*3600f);
		case HOURS:
			return this.timeStep/3600f;
		case MINUTES:
			return this.timeStep/60f;
		case SECONDS:
			return this.timeStep;
		case MILLISECONDS:
			return this.timeStep*1000f;
		case MICROSECONDS:
			return this.timeStep*1000000f;
		case NANOSECONDS:
			return this.timeStep*1000000000f;
		default:
		}
		return 0;
	}

	/** {@inheritDoc}
	 */
	@Override
	public long getSimulationStepCount() {
		return this.step;
	}

	/** {@inheritDoc}
	 */
	@Override
	public TimeUnit getTimeUnit() {
		return getClockTimeUnit();
	}

	/** {@inheritDoc}
	 */
	@Override
	public Clock getClock() {
		return this;
	}

}
