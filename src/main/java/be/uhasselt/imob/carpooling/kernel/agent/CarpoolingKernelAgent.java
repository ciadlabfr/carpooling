/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.kernel.agent;

import java.util.Calendar;
import java.util.Collections;
import java.util.EventListener;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import org.janusproject.kernel.address.Address;
import org.janusproject.kernel.agent.Agent;
import org.janusproject.kernel.agent.AgentActivator;
import org.janusproject.kernel.agent.KernelAgent;
import org.janusproject.kernel.channels.Channel;
import org.janusproject.kernel.channels.ChannelInteractable;
import org.janusproject.kernel.probe.Watchable;
import org.janusproject.kernel.status.Status;
import org.janusproject.kernel.util.random.RandomNumber;

import be.uhasselt.imob.carpooling.agent.AgentActivity;
import be.uhasselt.imob.carpooling.agent.CarpoolingAgent;
import be.uhasselt.imob.carpooling.agent.CarpoolingAgentBodyFactory;
import be.uhasselt.imob.carpooling.kernel.time.CarpoolingTimeManager;
import be.uhasselt.imob.carpooling.kernel.time.Time;
import be.uhasselt.imob.carpooling.schedule.Trip;
import be.uhasselt.imob.carpooling.util.SimulationLogger;
import fr.utbm.set.geom.bounds.bounds1d5.BoundingRect1D5;
import fr.utbm.set.geom.object.Direction1D;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.gis.road.primitive.RoadNetwork;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.jasim.JasimConstants;
import fr.utbm.set.jasim.environment.interfaces.body.AgentBody1D5;
import fr.utbm.set.jasim.environment.interfaces.body.factory.AgentBodyFactory;
import fr.utbm.set.jasim.environment.interfaces.body.factory.BodyDescription;
import fr.utbm.set.jasim.environment.interfaces.body.factory.FrustumDescription;
import fr.utbm.set.jasim.environment.interfaces.body.influences.Influence1D5;
import fr.utbm.set.jasim.environment.interfaces.body.perceptions.Perception1D5;
import fr.utbm.set.jasim.environment.interfaces.internalevents.JasimSimulationListener;
import fr.utbm.set.jasim.environment.model.SituatedEnvironment1D5;
import fr.utbm.set.jasim.environment.model.SituatedEnvironmentDescription;
import fr.utbm.set.jasim.environment.model.place.DefaultPlaceDescription;
import fr.utbm.set.jasim.environment.model.place.Place1D5;
import fr.utbm.set.jasim.environment.model.world.DynamicEntityManager;
import fr.utbm.set.jasim.environment.model.world.Entity1D5;
import fr.utbm.set.jasim.environment.model.world.MobileEntity1D5;

/** This class provides the tools to manage the time during
 * a carpooling simulation.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class CarpoolingKernelAgent extends KernelAgent implements ChannelInteractable {

	private SituatedEnvironment1D5<BoundingRect1D5<RoadSegment>,BoundingRect1D5<RoadSegment>> environment;

	private final ExecutionChrono simulationListener = new ExecutionChrono();
	
	/** Public for probing.
	 */
	@Watchable
	public BodyFactory bodyFactory;
	
	private RoadNetwork roadNetwork;
	private boolean simulationStarted;
	private boolean simulationUnderProgress;
	private UUID worldId;
	private final AtomicBoolean runFast = new AtomicBoolean(true);
	
	/**
	 * @param startDate is the first date in the simulation.
	 * @param endDate is the last date in the simulation.
	 * @param timeStep is the duration of a step during the micro-simulation (in seconds).
	 * @param roadNetwork is the road network to use.
	 * @param listener is the listener on environment's events.
	 * @param activator is the activator to use.
	 * @param startUpListener is a listener on kernel events which may be added at startup.
	 */
	public CarpoolingKernelAgent(
			Calendar startDate,
			Calendar endDate,
			float timeStep,
			RoadNetwork roadNetwork,
			JasimSimulationListener<?,?,?> listener,
			AgentActivator activator,
			EventListener startUpListener) {
		super(
				activator,
				null,
				new CarpoolingTimeManager(startDate, endDate, timeStep),
				startUpListener,
				UUID.randomUUID(),
				roadNetwork,
				listener);
	}
	
	/** {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Status activate(Object... parameters) {
		this.simulationStarted = false;
		this.simulationUnderProgress = false;
		this.environment = new SituatedEnvironment1D5(BoundingRect1D5.class);
		this.worldId = (UUID)parameters[0];
		this.roadNetwork = (RoadNetwork)parameters[1];
		
		this.bodyFactory = new BodyFactory();
		
		JasimSimulationListener listener = (JasimSimulationListener)parameters[2];
		if (listener!=null)
			this.environment.addJasimSimulationListener(listener);
		
		// Create the description of the world
		DefaultPlaceDescription<Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>> placeDescription = new DefaultPlaceDescription<Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>>(
				this.worldId);
		placeDescription.setWorldModel(RoadNetwork.class, this.roadNetwork);
		
		SituatedEnvironmentDescription<Entity1D5<BoundingRect1D5<RoadSegment>>,MobileEntity1D5<BoundingRect1D5<RoadSegment>>> description = new SituatedEnvironmentDescription<Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>>(
				UUID.randomUUID(), (CarpoolingTimeManager)getTimeManager());
		description.addPlace(placeDescription);
		
		this.environment.activate(description, null);
		
		return super.activate(parameters);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Status live() {
		Status status = super.live();
		if ((status==null || status.isSuccess())) {
			//
			// Test if all the agents have run their behavior for the current day.
			// If yes, move the time to the next day.
			// This action will wake up the agents.
			//
			
			boolean hasActiveAgent = false;
			int activeAgents = 0;
			int enableSimulation = 0;
			
			Iterator<Agent> iterator = getAgentRepository().values().iterator();
			Agent agent;
			
			while (iterator.hasNext()) {
				agent = iterator.next();
				if (agent!=this) {
					this.simulationStarted = true;
					if (!agent.isSleeping() && !agent.getState().isMortuary()) {
						hasActiveAgent = true;
						if (agent instanceof CarpoolingAgent) {
							AgentActivity activity = ((CarpoolingAgent)agent).getAgentActivity();
							if (activity!=null && activity.isDayActivity()) {
								++enableSimulation;
							}
							if (activity!=null && !activity.isEndOfDayActivity()) {
								++activeAgents;
							}
						}
					}
				}
			}
			
			if (this.simulationStarted) {
				if (!hasActiveAgent) {
					if (this.simulationUnderProgress) {
						stopMicroSimulation();
					}
					CarpoolingTimeManager tm = (CarpoolingTimeManager)getTimeManager();
					tm.moveToNextDay();
					print(tm.getClock().toString());
					if (tm.isSimulationTerminated()) {
						killAll();
					}
				}
				else if (enableSimulation>=activeAgents && !this.simulationUnderProgress) {
					startMicroSimulation();
				}
				else if (enableSimulation<activeAgents && this.simulationUnderProgress) {
					stopMicroSimulation();
				}
				else if (this.simulationUnderProgress) {
					runMicroSimulation();
					if (!this.runFast.get()) {
						getKernelContext().getKernel().pause();
					}
				}
			}
		}
		return status;
	}
	
	private void startMicroSimulation() {
		this.simulationUnderProgress = true;
		this.simulationListener.start((CarpoolingTimeManager)this.environment.getSimulationClock());
		this.environment.behaviour();
	}
	
	private void stopMicroSimulation() {
		DynamicEntityManager<MobileEntity1D5<BoundingRect1D5<RoadSegment>>> manager = this.environment.getDynamicEntityManager();
		Iterator<? extends MobileEntity1D5<BoundingRect1D5<RoadSegment>>> entities = this.environment.getPlaceObject(this.worldId).getWorldModel().getMobileEntities();
		MobileEntity1D5<BoundingRect1D5<RoadSegment>> entity;
		while (entities.hasNext()) {
			entity = entities.next();
			manager.unregisterMobileEntity(entity);
		}
		manager.commit();
		this.simulationUnderProgress = false;
		this.simulationListener.stop();
	}

	private void runMicroSimulation() {
		this.simulationListener.restart();
		this.environment.behaviour();
	}

	/** {@inheritDoc}
	 */
	@Override
	public Set<? extends Class<? extends Channel>> getSupportedChannels() {
		return Collections.singleton(CarpoolingKernelChannel.class);
	}

	/** {@inheritDoc}
	 */
	@Override
	public <C extends Channel> C getChannel(Class<C> channelClass, Object... params) {
		if (CarpoolingKernelChannel.class.isAssignableFrom(channelClass)) {
			return channelClass.cast(new CarpoolingChannel());
		}
		return null;
	}
		
	/**
	 * @author $Author: sgalland$
	 * @version $Name$ $Revision$ $Date$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private class CarpoolingChannel implements CarpoolingKernelChannel {
		
		/**
		 */
		public CarpoolingChannel() {
			//
		}

		/** {@inheritDoc}
		 */
		@Override
		public Address getChannelOwner() {
			return CarpoolingKernelAgent.this.getAddress();
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public boolean isRunFast() {
			return CarpoolingKernelAgent.this.runFast.get();
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public void runFast() {
			CarpoolingKernelAgent.this.runFast.set(true);
			getKernelContext().getKernel().resume();
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public void runStepByStep() {
			CarpoolingKernelAgent.this.runFast.set(false);
			getKernelContext().getKernel().resume();
		}
		
	}

	/**
	 * @author $Author: sgalland$
	 * @version $Name$ $Revision$ $Date$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private class BodyFactory implements CarpoolingAgentBodyFactory {
		
		private final BodyDescription bodyDescription;
		
		/**
		 */
		public BodyFactory() {
			this.bodyDescription = BodyDescription.createVehicleBodyDescription(
					null,
					JasimConstants.DEFAULT_VEHICLE_LENGTH,
					JasimConstants.DEFAULT_VEHICLE_WIDTH,
					JasimConstants.DEFAULT_VEHICLE_HEIGHT);
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings({ "unchecked", "synthetic-access" })
		@Override
		public void create(
				CarpoolingAgent agent, Trip trip) {
			Place1D5<BoundingRect1D5<RoadSegment>,BoundingRect1D5<RoadSegment>> place;
			DynamicEntityManager<MobileEntity1D5<BoundingRect1D5<RoadSegment>>> manager;

			place = CarpoolingKernelAgent.this.environment.getPlaceObject(CarpoolingKernelAgent.this.worldId);
			manager = CarpoolingKernelAgent.this.environment.getDynamicEntityManager();

			if (trip!=null) {
				Point1D5 position = trip.getSource().getGISPosition();
				assert(position.getSegment()!=null);
				
				// Select an initial direction
				Direction1D direction = RandomNumber.nextBoolean() ? Direction1D.REVERTED_DIRECTION : Direction1D.SEGMENT_DIRECTION;
				
				// Update the position to be on the right side of the road
				RoadSegment segment = (RoadSegment)position.getSegment();
				Direction1D laneDirection = segment.getLaneDirection(0);
				double shift;
				if (laneDirection!=direction) {
					shift = segment.getLaneCenter(segment.getLaneCount()-1);
				}
				else {
					shift = segment.getLaneCenter(0);
				}
				position.setJuttingDistance(shift);
				
				AgentBodyFactory factory = place.getAgentBodyFactory(
						position,
						direction,
						this.bodyDescription,
						Collections.<FrustumDescription>emptyList());
				AgentBody1D5<Influence1D5<RoadSegment>,Perception1D5,BoundingRect1D5<RoadSegment>> body = (AgentBody1D5<Influence1D5<RoadSegment>,Perception1D5,BoundingRect1D5<RoadSegment>>)agent.createBody(factory);
				body.setPlace(place); 
				manager.registerMobileEntity(body);
			}
		}
		
	}

	/**
	 * @author $Author: sgalland$
	 * @version $Name$ $Revision$ $Date$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private class ExecutionChrono {

		private CarpoolingTimeManager manager = null;
		private Time simulationStep = null;
		private long startTime = 0;
		
		public ExecutionChrono() {
			//
		}
		
		public void start(CarpoolingTimeManager tm) {
			this.manager = tm;
			this.simulationStep = tm.getWeekTime();
			this.startTime = System.nanoTime();
		}
		
		public void stop() {
			long duration = System.nanoTime() - this.startTime;
			SimulationLogger.logSimulationDuration(this.simulationStep, duration);
			this.simulationStep = null;
		}

		public void restart() {
			Time t = this.manager.getWeekTime();
			if (!t.equals(this.simulationStep)) {
				long duration = System.nanoTime() - this.startTime;
				SimulationLogger.logSimulationDuration(this.simulationStep, duration);
				this.simulationStep = t;
				this.startTime = System.nanoTime();
			}
		}

	}

}