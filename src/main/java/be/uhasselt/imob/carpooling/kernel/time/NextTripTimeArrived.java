/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.kernel.time;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.janusproject.kernel.condition.AbstractCondition;
import org.janusproject.kernel.condition.ConditionFailure;
import org.janusproject.kernel.condition.TimeCondition;
import org.janusproject.kernel.condition.TimeConditionParameterProvider;

/** This is a time-based condition which
 * is satisfacting when the next trip is arrived.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class NextTripTimeArrived extends AbstractCondition<TimeConditionParameterProvider> implements TimeCondition, ConditionFailure {

	private static final long serialVersionUID = 457185757364260033L;
	
	private final Time startTime;
	
	/**
	 * @param startAt
	 */
	public NextTripTimeArrived(Time startAt) {
		this.startTime = startAt;
	}
	
	private static Time getTime(Date current) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(current);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		return new Time(DayOfWeek.valueOf(cal), hour, minute);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean evaluate(TimeConditionParameterProvider provider) {
		Date current = provider.getCurrentDate();
		if (current!=null) {
			Time currentTime = getTime(current);
			if (currentTime.compareTo(this.startTime) >= 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConditionFailure evaluateFailure(TimeConditionParameterProvider provider) {
		Date current = provider.getCurrentDate();
		if (current!=null) {
			Time currentTime = getTime(current);
			if (currentTime.compareTo(this.startTime) >= 0) {
				return null;
			}
		}
		return this;
	}
	
}
