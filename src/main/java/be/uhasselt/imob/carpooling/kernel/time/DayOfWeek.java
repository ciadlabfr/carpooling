/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.kernel.time;

import java.util.Calendar;

/** Days of the week.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public enum DayOfWeek {

	/** Monday.
	 */
	MONDAY,

	/** Tuesday.
	 */
	TUESDAY,

	/** Wednesday.
	 */
	WEDNESDAY,

	/** Thursday.
	 */
	THURSDAY,

	/** Friday.
	 */
	FRIDAY,
	
	/** Saturday.
	 */
	SATURDAY,
	
	/** Sunday.
	 */
	SUNDAY;

	/** Replies the day of the week for the specified calendar.
	 * 
	 * @param calendar
	 * @return the day of the week.
	 */
	public static DayOfWeek valueOf(Calendar calendar) {
		switch(calendar.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.MONDAY:
			return MONDAY;
		case Calendar.TUESDAY:
			return TUESDAY;
		case Calendar.WEDNESDAY:
			return WEDNESDAY;
		case Calendar.THURSDAY:
			return THURSDAY;
		case Calendar.FRIDAY:
			return FRIDAY;
		case Calendar.SATURDAY:
			return SATURDAY;
		case Calendar.SUNDAY:
			return SUNDAY;
		default:
		}
		throw new IllegalArgumentException();
	}
	
	/** Replies the next day.
	 * 
	 * @return the next day.
	 */
	public DayOfWeek next() {
		switch(this) {
		case MONDAY:
			return TUESDAY;
		case TUESDAY:
			return WEDNESDAY;
		case WEDNESDAY:
			return THURSDAY;
		case THURSDAY:
			return FRIDAY;
		case FRIDAY:
			return SATURDAY;
		case SATURDAY:
			return SUNDAY;
		case SUNDAY:
			return MONDAY;
		default:
		}
		throw new IllegalArgumentException();
	}

	/** Replies the previous day.
	 * 
	 * @return the previous day.
	 */
	public DayOfWeek previous() {
		switch(this) {
		case MONDAY:
			return SUNDAY;
		case TUESDAY:
			return MONDAY;
		case WEDNESDAY:
			return TUESDAY;
		case THURSDAY:
			return WEDNESDAY;
		case FRIDAY:
			return THURSDAY;
		case SATURDAY:
			return FRIDAY;
		case SUNDAY:
			return SATURDAY;
		default:
		}
		throw new IllegalArgumentException();
	}

}
