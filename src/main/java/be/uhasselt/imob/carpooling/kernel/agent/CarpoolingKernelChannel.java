/*
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.kernel.agent;

import org.janusproject.kernel.channels.Channel;

/** Channel to interact with the carpooling simulator kernel.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public interface CarpoolingKernelChannel extends Channel {

	/** Replies if the kernel is running fast.
	 * 
	 * @return <code>true</code> if the kernel is running fast;
	 * <code>false</code> if it is running step-by-step.
	 */
	public boolean isRunFast();
	
	/** Run the fast mode.
	 */
	public void runFast();

	/** Run the step-by-step mode.
	 */
	public void runStepByStep();

}
