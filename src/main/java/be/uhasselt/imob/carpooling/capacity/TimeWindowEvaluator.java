/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.capacity;

import java.util.Collection;

import org.janusproject.kernel.crio.capacity.Capacity;

import be.uhasselt.imob.carpooling.schedule.TimeWindow;
import be.uhasselt.imob.carpooling.schedule.Trip;

/**
 * The capacity to evaluate the validity of time windows
 * for a trip.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public interface TimeWindowEvaluator extends Capacity {

	/** Evaluate the cost to use a transport mode.
	 * 
	 * @param trip is the trip concerned by the evaluation.
	 * @param windows are the time windows to evaluate.
	 * @return the selected time window or <code>null</code> if
	 * the given time windows are incompatible.
	 */
	public TimeWindow evaluateTimeWindows(Trip trip, Collection<TimeWindow> windows);
	
}
