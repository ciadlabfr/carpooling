/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.capacity;

import org.janusproject.kernel.crio.capacity.Capacity;

import be.uhasselt.imob.carpooling.environment.Environment;
import be.uhasselt.imob.carpooling.util.AgentType;
import be.uhasselt.imob.carpooling.util.TransportMode;

/**
 * Describe the capacity of an agent to evaluate a
 * transport mode.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public interface TransportModeEvaluator extends Capacity {

	/** Invoked to prepare for a new set of evaluations.
	 */
	public void prepareEvaluations();
	
	/** Evaluate the cost to use a transport mode.
	 * 
	 * @param agentType is the type of the caller.
	 * @param transportType is the mode to evaluate.
	 * @param environment is the current state of the environment.
	 * @return the evaluated cost.
	 */
	public float evaluateCost(AgentType agentType, TransportMode transportType, Environment environment);
	
}
