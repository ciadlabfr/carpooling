/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.capacity;

import org.janusproject.kernel.crio.capacity.Capacity;

import be.uhasselt.imob.carpooling.schedule.TimeWindow;
import be.uhasselt.imob.carpooling.schedule.Trip;

/**
 * The capacity to evaluate the trip proposal.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public interface TripProposalEvaluator extends Capacity {

	/** Evaluate the cost to use a transport mode.
	 * 
	 * @param myTrip is the my trip concerned by the evaluation.
	 * @param partnerTrip is the trip of my partner concerned by the evaluation.
	 * @param proposedWindow is the time window proposed by my partner, or <code>null</code>.
	 * @return the preferred time window or <code>null</code> if the
	 * trip is not acceptable.
	 */
	public TimeWindow evaluateTrip(Trip myTrip, Trip partnerTrip, TimeWindow proposedWindow);
	
}
