/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.ui;

import fr.utbm.set.geom.bounds.Bounds;
import fr.utbm.set.jasim.environment.interfaces.internalevents.EnvironmentalAction;
import fr.utbm.set.jasim.environment.interfaces.internalevents.JasimSimulationListener;
import fr.utbm.set.jasim.environment.model.SituatedEnvironment;
import fr.utbm.set.jasim.environment.model.world.MobileEntity;
import fr.utbm.set.jasim.environment.model.world.WorldEntity;

/**
 * A layer which is displaying JaSIM simulation results.
 * 
 * @param <SB> is the type of the bounds for static entities.
 * @param <MB> is the type of the bounds for mobile entities.
 * @param <SE> is the type of the static entities.
 * @param <ME> is the type of the mobile entities.
 * @param <EA> is the type of the supported environmental actions.
 * @param <ENV> is the type of the environment.
 * @author St&eacute;phane GALLAND &lt;stephane.galland@utbm.fr&gt;
 * @version $Name$ $Revision$ $Date$
 */
public interface JasimSimulatorLayer<SB extends Bounds<?,?,?>,
								   MB extends Bounds<?,?,?>,
								   SE extends WorldEntity<SB>,
								   ME extends MobileEntity<MB>,
								   EA extends EnvironmentalAction,
								   ENV extends SituatedEnvironment<EA,SE,ME,?>>
extends JasimSimulationListener<EA,SE,ME> {

	/** Replies the count of agents in the simulation.
	 * 
	 * @return the count of agents in the simulation.
	 */
	public int getAgentCount();

}