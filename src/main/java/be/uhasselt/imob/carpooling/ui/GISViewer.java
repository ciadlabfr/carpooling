/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.UUID;

import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.vecmath.Point2d;

import org.arakhne.afc.vmutil.locale.Locale;
import org.janusproject.kernel.Kernel;
import org.janusproject.kernel.address.AgentAddress;
import org.janusproject.kernel.agent.Kernels;
import org.janusproject.kernel.agent.ProbeManager;
import org.janusproject.kernel.crio.core.AddressUtil;
import org.janusproject.kernel.probe.IndividualProbe;

import be.uhasselt.imob.carpooling.kernel.agent.CarpoolingKernelChannel;
import be.uhasselt.imob.carpooling.kernel.time.CarpoolingTimeManager;
import be.uhasselt.imob.carpooling.kernel.time.Time;
import be.uhasselt.imob.carpooling.schedule.Trip;
import fr.utbm.set.attr.HeapAttributeProvider;
import fr.utbm.set.geom.bounds.bounds1d5.BoundingRect1D5;
import fr.utbm.set.geom.bounds.bounds2d.CombinableBounds2D;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.gis.mapelement.MapElement;
import fr.utbm.set.gis.maplayer.MapElementLayer;
import fr.utbm.set.gis.maplayer.MapLayer;
import fr.utbm.set.gis.maplayer.MultiMapLayer;
import fr.utbm.set.gis.road.StandardRoadNetwork;
import fr.utbm.set.gis.road.layer.RoadNetworkLayer;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.gis.road.primitive.RoadConnection;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.gis.ui.layerpanel.DrawingErrorListener;
import fr.utbm.set.gis.ui.layerpanel.JLayerPanel;
import fr.utbm.set.gis.ui.view.GISDrawable;
import fr.utbm.set.gis.ui.view.GISDrawer;
import fr.utbm.set.gis.ui.view.GISDrawerRepository;
import fr.utbm.set.gis.ui.view.GISGraphics2D;
import fr.utbm.set.jasim.environment.interfaces.body.AgentBody1D5;
import fr.utbm.set.jasim.environment.interfaces.internalevents.EntityActionEvent;
import fr.utbm.set.jasim.environment.interfaces.internalevents.EntityLifeEvent;
import fr.utbm.set.jasim.environment.interfaces.internalevents.JasimSimulationListener;
import fr.utbm.set.jasim.environment.interfaces.internalevents.SimulationInitEvent;
import fr.utbm.set.jasim.environment.interfaces.internalevents.SimulationStopEvent;
import fr.utbm.set.jasim.environment.model.influencereaction.EnvironmentalAction1D5;
import fr.utbm.set.jasim.environment.model.world.Entity1D5;
import fr.utbm.set.jasim.environment.model.world.MobileEntity1D5;
import fr.utbm.set.jasim.environment.time.Clock;
import fr.utbm.set.ui.IconSize;
import fr.utbm.set.ui.PredefinedIcon;
import fr.utbm.set.util.FlagContainer;

/** Viewer for the GIS maps.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class GISViewer  extends JFrame
implements DrawingErrorListener,
			MouseMotionListener,
			MouseListener,
			JasimSimulationListener<EnvironmentalAction1D5, Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>>,
			SimulationController {
	
	private static final long serialVersionUID = 3081040128026688052L;

	/** Show the viewer with the road network inside.
	 * 
	 * @param roadNetwork
	 * @return the opened viewer.
	 */
	public static GISViewer show(StandardRoadNetwork roadNetwork) {
		RoadNetworkLayer roadLayer = new RoadNetworkLayer(roadNetwork);
		JasimSimulatorLayer1D5 vehicleLayer = new JasimSimulatorLayer1D5(roadNetwork);
		GISViewer viewer = new GISViewer(roadLayer, vehicleLayer);
		viewer.setVisible(true);
		return viewer;
	}
	
	private static void print(String label, RoadConnection con) {
		if (con!=null) {
			System.out.println(label+": "+con.toString()); //$NON-NLS-1$
			System.out.println("connected with: "); //$NON-NLS-1$
			for(RoadSegment sgmt : con.getConnectedSegments()) {
				System.out.println("> "+sgmt.getGeoId().toString()); //$NON-NLS-1$
			}
		}
		else {
			System.out.println(label+": null"); //$NON-NLS-1$
		}
	}
	
	private static void print(RoadSegment segment) {
		assert(segment!=null);
		System.out.println("SEGMENT: "+segment.toString()); //$NON-NLS-1$
		print("start at", segment.getBeginPoint()); //$NON-NLS-1$
		print("end at", segment.getEndPoint()); //$NON-NLS-1$
	}

	private final JButton startButton;
	private final JButton stepButton;
	private final MultiMapLayer<MapLayer> layer;
	private final JasimSimulatorLayer1D5 simulationLayer;
	private final JLabel statusBar = new JLabel();
	private final JLayerPanel layerPanel;
	private MapElement selectedElement = null;
	private final DefaultListModel agentListModel = new DefaultListModel();
	private final JList agentList = new JList(this.agentListModel);
	private final DataLayer dataLayer = new DataLayer();
	
	private String statusBarGisX = ""; //$NON-NLS-1$
	private String statusBarGisY = ""; //$NON-NLS-1$
	private String statusBarMouseX = ""; //$NON-NLS-1$
	private String statusBarMouseY = ""; //$NON-NLS-1$
	private String statusBarClock = ""; //$NON-NLS-1$

	/**
	 * @param roadLayer
	 * @param simulationLayer
	 */
	private GISViewer(RoadNetworkLayer roadLayer, JasimSimulatorLayer1D5 simulationLayer) {
		super(Locale.getString("TITLE")); //$NON-NLS-1$
		this.simulationLayer = simulationLayer;
		this.layer = new MultiMapLayer<MapLayer>();
		this.layer.addMapLayer(roadLayer);
		this.layer.addMapLayer(this.dataLayer);
		this.layer.addMapLayer(simulationLayer);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(800, 600));
		
		JPanel toolBar = new JPanel();
		toolBar.setLayout(new BoxLayout(toolBar, BoxLayout.X_AXIS));
		this.startButton = new JButton(PredefinedIcon.MEDIA_PLAY.getIcon(IconSize.getToolBarSize()));
		toolBar.add(this.startButton);
		this.startButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				runSimulation();
			}
		});
		this.stepButton = new JButton(PredefinedIcon.MEDIA_STEP_FORWARD.getIcon(IconSize.getToolBarSize()));
		toolBar.add(this.stepButton);
		this.stepButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stepSimulation();
			}
		});

		this.layerPanel = new JLayerPanel(this.layer);
		this.layerPanel.setBackground(Color.LIGHT_GRAY);
		this.layerPanel.addDrawingErrorListener(this);
		this.agentList.setCellRenderer(new CellRenderer());
		this.agentList.addListSelectionListener(new ListSelectionListener() {
			@SuppressWarnings("synthetic-access")
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateSelectedEntity();
			}
		});
		add(new JScrollPane(this.layerPanel), BorderLayout.CENTER);
		add(this.statusBar, BorderLayout.SOUTH);
		JPanel topPane = new JPanel(new BorderLayout());
		add(topPane, BorderLayout.NORTH);
		topPane.add(new JScrollPane(this.agentList), BorderLayout.CENTER);
		topPane.add(toolBar, BorderLayout.NORTH);

		pack();

		this.layerPanel.addMouseListener(this);
		this.layerPanel.addMouseMotionListener(this);
	}
	
	private void updateSelectedEntity() {
		AgentAddress addr = null;
		MobileEntity1D5<?> selected = (MobileEntity1D5<?>)this.agentList.getSelectedValue();
		if (selected instanceof AgentBody1D5<?,?,?>) {
			AgentBody1D5<?,?,?> body = (AgentBody1D5<?,?,?>)selected;
			UUID id = body.getIdentifier();
			addr = AddressUtil.createAgentAddress(id);
		}
		this.dataLayer.setProbe(addr);
	}
	

	/** {@inheritDoc}
	 */
	@Override
	public void showController() {
		// Do nothing because the controller is shown by show() and
		// the simulation launching is done with a button.
	}

	/** {@inheritDoc}
	 */
	@Override
	public void hideController() {
		setVisible(false);
		dispose();
	}

	/** Replies the listener on the simulation events supported
	 * by this viewer.
	 * 
	 * @return the listener on the simulation events.
	 */
	public JasimSimulationListener<EnvironmentalAction1D5, Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>> getSimulationListener() {
		return new SimulationListener();
	}

	/** {@inheritDoc}
	 */
	@Override
	public void onDrawingErrorOccurs(JLayerPanel panel, Throwable error) {
		error.printStackTrace();
	}

	/** {@inheritDoc}
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		//
	}

	private static String format(double v) {
		return String.format("%.3f", Double.valueOf(v)); //$NON-NLS-1$
	}

	private static String format(int v) {
		return Integer.toString(v);
	}

	private void updateStatusBar(double gisX, double gisY, int mouseX, int mouseY) {
		this.statusBarGisX = format(gisX);
		this.statusBarGisY = format(gisY);
		this.statusBarMouseX = format(mouseX);
		this.statusBarMouseY = format(mouseY);
		this.statusBar.setText(Locale.getString("POSITION", //$NON-NLS-1$
				this.statusBarGisX, this.statusBarGisY,
				this.statusBarMouseX, this.statusBarMouseY,
				this.statusBarClock)); 
	}

	private void updateStatusBar(Time clock) {
		this.statusBarClock = clock.toString();
		this.statusBar.setText(Locale.getString("POSITION", //$NON-NLS-1$
				this.statusBarGisX, this.statusBarGisY,
				this.statusBarMouseX, this.statusBarMouseY,
				this.statusBarClock)); 
	}

	/** {@inheritDoc}
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		double gx = this.layerPanel.pixel2logical_x(e.getX());
		double gy = this.layerPanel.pixel2logical_y(e.getY());
		updateStatusBar(
				gx, gy,
				e.getX(), e.getY());
	}
	
	/** {@inheritDoc}
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {
			double gx = this.layerPanel.pixel2logical_x(e.getX());
			double gy = this.layerPanel.pixel2logical_y(e.getY());
			Point2d mouse = new Point2d(gx,gy);
			MapElement nearest = null;
			double distance = Double.POSITIVE_INFINITY;
			Iterator<MapElement> elementIterator = elements();
			while(elementIterator.hasNext()) {
				MapElement elt = elementIterator.next();
				double dist = elt.distance(mouse);
				if (dist<distance) {
					distance = dist;
					nearest = elt;
				}
			}
			if (nearest!=null) {
				System.out.println(nearest.getGeoId().toString());
				if (nearest instanceof RoadSegment) {
					RoadSegment roadSegment = (RoadSegment)nearest;
					print(roadSegment);
				}
				if (this.selectedElement!=null) {
					this.selectedElement.unsetFlag(FlagContainer.FLAG_SELECTED);
				}
				this.selectedElement = nearest;
				if (this.selectedElement!=null) {
					this.selectedElement.setFlag(FlagContainer.FLAG_SELECTED);
				}
				repaint();
			}
		}
		finally {
			setCursor(Cursor.getDefaultCursor());
		}
	}

	/** {@inheritDoc}
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		double gx = this.layerPanel.pixel2logical_x(e.getX());
		double gy = this.layerPanel.pixel2logical_y(e.getY());
		updateStatusBar(
				gx, gy,
				e.getX(), e.getY());
	}

	/** {@inheritDoc}
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		//
	}

	/** {@inheritDoc}
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		//
	}

	/** {@inheritDoc}
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		//
	}

	/** {@inheritDoc}
	 */
	@Override
	public void simulationInitiated(SimulationInitEvent<Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
		synchronized(getTreeLock()) {
			this.agentListModel.clear();
		}
	}

	/** {@inheritDoc}
	 */
	@Override
	public void simulationStopped(SimulationStopEvent event) {
		CarpoolingTimeManager tm = (CarpoolingTimeManager)event.getTime();
		updateStatusBar(tm.getWeekTime());
		synchronized(getTreeLock()) {
			this.agentListModel.clear();
		}
	}

	/** {@inheritDoc}
	 */
	@Override
	public void entitiesArrived(EntityLifeEvent<MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
		synchronized(getTreeLock()) {
			for(MobileEntity1D5<?> entity : event.getEntities()) {
				this.agentListModel.addElement(entity);
			}
		}
		CarpoolingTimeManager tm = (CarpoolingTimeManager)event.getTime();
		updateStatusBar(tm.getWeekTime());
	}

	/** {@inheritDoc}
	 */
	@Override
	public void entitiesDisappeared(EntityLifeEvent<MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
		synchronized(getTreeLock()) {
			for(MobileEntity1D5<?> entity : event.getEntities()) {
				this.agentListModel.removeElement(entity);
			}
		}
		CarpoolingTimeManager tm = (CarpoolingTimeManager)event.getTime();
		updateStatusBar(tm.getWeekTime());
	}

	/** {@inheritDoc}
	 */
	@Override
	public void entityActionsApplied(EntityActionEvent<EnvironmentalAction1D5> event) {
		CarpoolingTimeManager tm = (CarpoolingTimeManager)event.getTime();
		updateStatusBar(tm.getWeekTime());
	}

	/** {@inheritDoc}
	 */
	@Override
	public void simulationIddle(Clock clock) {
		CarpoolingTimeManager tm = (CarpoolingTimeManager)clock;
		updateStatusBar(tm.getWeekTime());
	}

	/** Replies the map elements displayed by this viewer.
	 * 
	 * @return the map elements displayed by this viewer.
	 */
	public Iterator<MapElement> elements() {
		return new ElementIterator(this.layer);
	}

	/** Launch the simulation to the end.
	 */
	protected void runSimulation() {
		GISViewer.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		Icon runIcon = PredefinedIcon.TASK_RUN.getIcon(IconSize.getToolBarSize());
		
		GISViewer.this.startButton.setEnabled(false);
		GISViewer.this.stepButton.setEnabled(false);

		GISViewer.this.startButton.setIcon(runIcon);
		GISViewer.this.stepButton.setIcon(runIcon);

		Kernel k = Kernels.get();
		k.launchDifferedExecutionAgents();
		CarpoolingKernelChannel channel = k.getChannelManager().getChannel(k.getAddress(), CarpoolingKernelChannel.class);
		channel.runFast();
		
		GISViewer.this.startButton.setIcon(
				PredefinedIcon.MEDIA_PLAY.getIcon(IconSize.getToolBarSize()));
		GISViewer.this.stepButton.setIcon(
				PredefinedIcon.MEDIA_STEP_FORWARD.getIcon(IconSize.getToolBarSize()));

		GISViewer.this.setCursor(Cursor.getDefaultCursor());
	}

	/** Launch the simulation and run one step.
	 */
	protected void stepSimulation() {
		GISViewer.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		Icon runIcon = PredefinedIcon.TASK_RUN.getIcon(IconSize.getToolBarSize());
		
		GISViewer.this.startButton.setEnabled(false);
		GISViewer.this.stepButton.setEnabled(false);

		GISViewer.this.startButton.setIcon(runIcon);
		GISViewer.this.stepButton.setIcon(runIcon);

		Kernel k = Kernels.get();
		k.launchDifferedExecutionAgents();
		CarpoolingKernelChannel channel = k.getChannelManager().getChannel(k.getAddress(), CarpoolingKernelChannel.class);
		channel.runStepByStep();

		GISViewer.this.startButton.setIcon(
				PredefinedIcon.MEDIA_PLAY.getIcon(IconSize.getToolBarSize()));
		GISViewer.this.stepButton.setIcon(
				PredefinedIcon.MEDIA_STEP_FORWARD.getIcon(IconSize.getToolBarSize()));

		GISViewer.this.startButton.setEnabled(true);
		GISViewer.this.stepButton.setEnabled(true);

		GISViewer.this.setCursor(Cursor.getDefaultCursor());
	}
	
	/**
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private static class ElementIterator implements Iterator<MapElement> {

		private final LinkedList<MapLayer> candidates = new LinkedList<MapLayer>();
		private Iterator<? extends MapElement> elementIterator = null;
		private MapElement next = null;

		public ElementIterator(MapLayer topCandidate) {
			this.candidates.add(topCandidate);
			searchNext();
		}

		private void searchNext() {
			this.next = null;
			while (!this.candidates.isEmpty() && (this.elementIterator==null || !this.elementIterator.hasNext())) {
				this.elementIterator = null;
				while (!this.candidates.isEmpty() && this.elementIterator==null) {
					MapLayer layer = this.candidates.removeFirst();
					if (layer instanceof MultiMapLayer<?>) {
						this.candidates.addAll(0, ((MultiMapLayer<?>)layer).getAllMapLayers());
					}
					else if (layer instanceof MapElementLayer<?>) {
						this.elementIterator = ((MapElementLayer<?>)layer).getAllMapElements().iterator();
					}
				}
			}
			if (this.elementIterator!=null && this.elementIterator.hasNext()) {
				this.next = this.elementIterator.next();
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			return this.next!=null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public MapElement next() {
			MapElement e = this.next;
			if (e==null) throw new NoSuchElementException();
			searchNext();
			return e;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

	}
	
	/**
	 * @author $Author: sgalland$
	 * @version $Name$ $Revision$ $Date$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private class SimulationListener implements JasimSimulationListener<EnvironmentalAction1D5, Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>> {

		/**
		 */
		public SimulationListener() {
			//
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public void simulationInitiated(SimulationInitEvent<Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
			GISViewer.this.simulationLayer.simulationInitiated(event);
			GISViewer.this.simulationInitiated(event);
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public void simulationStopped(SimulationStopEvent event) {
			GISViewer.this.simulationLayer.simulationStopped(event);
			GISViewer.this.simulationStopped(event);
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public void entitiesArrived(
				EntityLifeEvent<MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
			GISViewer.this.simulationLayer.entitiesArrived(event);
			GISViewer.this.entitiesArrived(event);
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public void entitiesDisappeared(
				EntityLifeEvent<MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
			GISViewer.this.simulationLayer.entitiesDisappeared(event);
			GISViewer.this.entitiesDisappeared(event);
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public void entityActionsApplied(
				EntityActionEvent<EnvironmentalAction1D5> event) {
			GISViewer.this.simulationLayer.entityActionsApplied(event);
			GISViewer.this.entityActionsApplied(event);
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public void simulationIddle(Clock clock) {
			GISViewer.this.simulationLayer.simulationIddle(clock);
			GISViewer.this.simulationIddle(clock);
		}
		
	}

	/**
	 * @author $Author: sgalland$
	 * @version $Name$ $Revision$ $Date$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private static class DataLayer extends MapLayer implements GISDrawable {

		private IndividualProbe agentProbe = null;
		
		/**
		 */
		public DataLayer() {
			super(UUID.randomUUID(), new HeapAttributeProvider());
		}

		public synchronized void setProbe(AgentAddress addr) {
			if (addr==null && this.agentProbe==null) return;
			if (addr!=null && this.agentProbe!=null && this.agentProbe.getWatchedObject().equals(addr)) return;
			ProbeManager pm = Kernels.get().getProbeManager();
			if (this.agentProbe!=null) {
				pm.release(this.agentProbe);
			}
			if (addr!=null) {
				this.agentProbe = pm.createProbe(addr);
			}
			else {
				this.agentProbe = null;
			}
			repaint();
		}

		/** {@inheritDoc}
		 */
		@Override
		protected CombinableBounds2D calcBounds() {
			return null;
		}

		/** {@inheritDoc}
		 */
		@Override
		public synchronized void draw(GISGraphics2D gcontext) {
			if (this.agentProbe!=null) {
				GISDrawer drawer = GISDrawerRepository.getSingleton().getDefaultDrawer();
				RoadPath path = this.agentProbe.getProbeValue("followedPath", RoadPath.class); //$NON-NLS-1$
				if (path!=null && !path.isEmpty()) {
					boolean altColor = true;
					for(RoadSegment sgmt : path) {
						drawer.draw(sgmt, gcontext, altColor ? Color.RED : Color.ORANGE);
						altColor = !altColor;
					}
				}
				Trip trip = this.agentProbe.getProbeValue("trip", Trip.class); //$NON-NLS-1$
				if (trip!=null && trip.size()>0) {
					double size = gcontext.pixel2logical_size(10);
					Point1D5 p1d5 = trip.getDestination().getGISPosition();
					if (p1d5!=null) {
						Point2d p = p1d5.toPoint2D();
						gcontext.setColor(Color.RED);
						gcontext.drawOval(p.x - size, p.y - size, 2*size, 2*size);
					}
				}
			}
 		}
		
	}
	
	/**
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private static class CellRenderer extends DefaultListCellRenderer {
	
		private static final long serialVersionUID = 3345094923530018405L;

		/**
		 */
		public CellRenderer() {
			//
		}

		/** {@inheritDoc}
		 */
		@Override
		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			Object v = value;
			if (value instanceof MobileEntity1D5<?>) {
				v = ((MobileEntity1D5<?>)value).getIdentifier();
			}
			return super.getListCellRendererComponent(
					list, v, index, isSelected,
					cellHasFocus);
		}
		
	}
	
}
