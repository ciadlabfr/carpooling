/* 
 * $Id$
 * 
 * Copyright (C) 2013
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.ui;

import java.util.Calendar;

import org.janusproject.kernel.Kernel;
import org.janusproject.kernel.agent.Kernels;

import be.uhasselt.imob.carpooling.kernel.agent.CarpoolingKernelChannel;
import be.uhasselt.imob.carpooling.kernel.time.CarpoolingTimeManager;
import fr.utbm.set.geom.bounds.bounds1d5.BoundingRect1D5;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.jasim.environment.interfaces.internalevents.EntityActionEvent;
import fr.utbm.set.jasim.environment.interfaces.internalevents.EntityLifeEvent;
import fr.utbm.set.jasim.environment.interfaces.internalevents.JasimSimulationListener;
import fr.utbm.set.jasim.environment.interfaces.internalevents.SimulationInitEvent;
import fr.utbm.set.jasim.environment.interfaces.internalevents.SimulationStopEvent;
import fr.utbm.set.jasim.environment.model.influencereaction.EnvironmentalAction1D5;
import fr.utbm.set.jasim.environment.model.world.Entity1D5;
import fr.utbm.set.jasim.environment.model.world.MobileEntity1D5;
import fr.utbm.set.jasim.environment.time.Clock;
import fr.utbm.set.ui.window.JProgressBarDialog;

/** This is the main class of the carpooling simulator.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class LightProgress extends JProgressBarDialog
implements JasimSimulationListener<EnvironmentalAction1D5, Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>>,
			SimulationController {

	private static final long serialVersionUID = -5581129368435638542L;

	private final int startT;
	private final int endT;

	/**
	 * @param startDate
	 * @param endDate
	 */
	public LightProgress(Calendar startDate, Calendar endDate) {
		this.startT = (int)(startDate.getTimeInMillis()/1000);
		this.endT = (int)(endDate.getTimeInMillis()/1000);
		getProgressBarModel().setRangeProperties(
				this.startT,
				1,
				this.startT,
				this.endT,
				false);
		getProgressBar().setStringPainted(true);
		setIndeterminate(true);
		setTitle("Simulation under progress...");
		pack();
		centerDialog();
		setModal(false);
	}

	private void update(Clock clock) {
		CarpoolingTimeManager tm = (CarpoolingTimeManager)clock;
		int t = (int)(tm.getCurrentDate().getTime()/1000);
		getProgressBarModel().setValue(t);
		getProgressBar().setString(tm.getWeekTime().toString());
	}

	/** {@inheritDoc}
	 */
	@Override
	public void simulationInitiated(SimulationInitEvent<Entity1D5<BoundingRect1D5<RoadSegment>>, MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
		setIndeterminate(false);
	}

	/** {@inheritDoc}
	 */
	@Override
	public void simulationStopped(SimulationStopEvent event) {
		//	
	}

	/** {@inheritDoc}
	 */
	@Override
	public void entitiesArrived(EntityLifeEvent<MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
		update(event.getTime());
	}

	/** {@inheritDoc}
	 */
	@Override
	public void entitiesDisappeared(EntityLifeEvent<MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
		update(event.getTime());
	}

	/** {@inheritDoc}
	 */
	@Override
	public void entityActionsApplied(EntityActionEvent<EnvironmentalAction1D5> event) {
		update(event.getTime());
	}

	/** {@inheritDoc}
	 */
	@Override
	public void simulationIddle(Clock clock) {
		update(clock);
	}

	/** {@inheritDoc}
	 */
	@Override
	public void showController() {
		setVisible(true);
		// Start the simulator, as fast as possible
		Kernel k = Kernels.get();
		k.launchDifferedExecutionAgents();
		CarpoolingKernelChannel channel = k.getChannelManager().getChannel(k.getAddress(), CarpoolingKernelChannel.class);
		channel.runFast();
	}

	/** {@inheritDoc}
	 */
	@Override
	public void hideController() {
		setVisible(false);
		dispose();
	}

}
