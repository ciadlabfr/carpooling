/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.ui;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Dimension2D;
import java.awt.geom.Rectangle2D;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import java.util.UUID;

import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;

import org.arakhne.afc.text.TextUtil;

import fr.utbm.set.geom.GeometryUtil;
import fr.utbm.set.geom.bounds.bounds1d5.BoundingRect1D5;
import fr.utbm.set.geom.bounds.bounds1d5.Bounds1D5;
import fr.utbm.set.geom.bounds.bounds2d.CombinableBounds2D;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.geom.object.Segment1D;
import fr.utbm.set.gis.road.RoadPolyline;
import fr.utbm.set.gis.road.primitive.RoadConnection;
import fr.utbm.set.gis.road.primitive.RoadNetwork;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.gis.ui.LayerUIConstants;
import fr.utbm.set.gis.ui.view.AbstractGISDrawer;
import fr.utbm.set.gis.ui.view.GISDrawerRepository;
import fr.utbm.set.gis.ui.view.GISGraphics2D;
import fr.utbm.set.jasim.JasimConstants;
import fr.utbm.set.jasim.environment.interfaces.internalevents.EntityActionEvent;
import fr.utbm.set.jasim.environment.interfaces.internalevents.EntityLifeEvent;
import fr.utbm.set.jasim.environment.model.SituatedEnvironment1D5;
import fr.utbm.set.jasim.environment.model.influencereaction.EnvironmentalAction1D5;
import fr.utbm.set.jasim.environment.model.world.Entity1D5;
import fr.utbm.set.jasim.environment.model.world.MobileEntity1D5;
import fr.utbm.set.jasim.environment.time.Clock;
import fr.utbm.set.math.MeasureUnitUtil;
import fr.utbm.set.ui.Dimension2DDouble;
import fr.utbm.set.ui.LODGraphics2D.TextAlignment;
import fr.utbm.set.util.error.ErrorLogger;

/**
 * A layer which is displaying JaSIM 1.5D simulation results.
 * 
 * @author St&eacute;phane GALLAND &lt;stephane.galland@utbm.fr&gt;
 * @version $Name$ $Revision$ $Date$
 */
public class JasimSimulatorLayer1D5
extends AbstractJasimSimulatorStandaloneLayer<BoundingRect1D5<RoadSegment>,
BoundingRect1D5<RoadSegment>,
Entity1D5<BoundingRect1D5<RoadSegment>>,
MobileEntity1D5<BoundingRect1D5<RoadSegment>>,
EnvironmentalAction1D5,
SituatedEnvironment1D5<BoundingRect1D5<RoadSegment>,
BoundingRect1D5<RoadSegment>>> {

	/** Color to draw the bounds of the vehicle symbols.
	 */
	public static final Color VEHICLE_BOUND_COLOR = Color.DARK_GRAY;

	/** Color to draw the vehicle directions.
	 */
	public static final Color VEHICLE_DIRECTION_COLOR = Color.RED;

	/** Radius of the vehicle symbols in meters.
	 */
	public static final float DEFAULT_VEHICLE_RADIUS = 5f;

	/** Diameter of the vehicle symbols in meters.
	 */
	public static final float DEFAULT_VEHICLE_DIAMETER = DEFAULT_VEHICLE_RADIUS * 2f;

	/** Size under which a lower precision diagram is allowed (in pixels).
	 */
	public static final float DEFAULT_VEHICLE_DRAW_PRECISION = 10;

	static {
		// Register the default JaSIM Simulator drawer.
		GISDrawerRepository.getSingleton().registerDrawer(JasimSimulatorLayer1D5.class, new JasimSimulatorDrawer());
	}

	private final List<UUID> entityIds = new LinkedList<UUID>();
	private final Map<UUID,EntityDescriptor> entities = new TreeMap<UUID,EntityDescriptor>();

	private final WeakReference<RoadNetwork> roadNetwork;

	/**
	 * @param roadNetwork
	 */
	public JasimSimulatorLayer1D5(RoadNetwork roadNetwork) {
		super(null, null);
		this.roadNetwork = new WeakReference<RoadNetwork>(roadNetwork);
	}

	/** Replies the count of agents in the simulation.
	 * 
	 * @return the count of agents in the simulation.
	 */
	@Override
	public int getAgentCount() {
		return this.entityIds.size();
	}

	@Override
	protected CombinableBounds2D calcBounds() {
		RoadNetwork network = this.roadNetwork.get();
		if (network!=null) return ((CombinableBounds2D)network.getBoundingBox().clone());
		return null;
	}

	@Override
	public void entitiesArrived(
			EntityLifeEvent<MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
		for(MobileEntity1D5<BoundingRect1D5<RoadSegment>> mobileEntity : event.getEntities()) {
			EntityDescriptor descriptor = new EntityDescriptor();
			descriptor.set(mobileEntity);
			UUID id = mobileEntity.getIdentifier();
			this.entityIds.add(id);
			this.entities.put(id, descriptor);
		}
		repaint();
	}

	@Override
	public void entitiesDisappeared(
			EntityLifeEvent<MobileEntity1D5<BoundingRect1D5<RoadSegment>>> event) {
		for(MobileEntity1D5<BoundingRect1D5<RoadSegment>> mobileEntity : event.getEntities()) {
			UUID id = mobileEntity.getIdentifier();
			this.entityIds.remove(id);
			this.entities.remove(id);
		}
		repaint();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void entityActionsApplied(
			EntityActionEvent<EnvironmentalAction1D5> event) {
		for(EnvironmentalAction1D5 action : event.getAppliedActions()) {
			MobileEntity1D5<BoundingRect1D5<RoadSegment>> entity = action.getEnvironmentalObject(MobileEntity1D5.class);
			if (entity!=null) {
				EntityDescriptor descriptor;
				descriptor = this.entities.get(entity.getIdentifier());
				if (descriptor!=null) {
					descriptor.set(entity);
				}
			}
		}
		repaint();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void simulationIddle(Clock clock) {
		//
	}

	/**
	 * @return the entities
	 */
	protected Iterator<EntityDescriptor> getEntities() {
		return new EntityIterator();
	}
	
	

	/**
	 * Iterator on entity descriptions. 
	 * 
	 * @author St&eacute;phane GALLAND &lt;stephane.galland@utbm.fr&gt;
	 * @version $Name$ $Revision$ $Date$
	 */
	protected class EntityIterator implements Iterator<EntityDescriptor> {

		private int idx;
		private EntityDescriptor next;

		/**
		 */
		public EntityIterator() {
			this.idx = 0;
			this.next = searchNext();
		}

		@SuppressWarnings("synthetic-access")
		private EntityDescriptor searchNext() {
			try {
				UUID id = JasimSimulatorLayer1D5.this.entityIds.get(this.idx);
				++this.idx;
				return JasimSimulatorLayer1D5.this.entities.get(id);
			}
			catch(Throwable _) {
				return null;
			}
		}

		@Override
		public boolean hasNext() {
			return this.next!=null;
		}

		@Override
		public EntityDescriptor next() {
			EntityDescriptor n = this.next;
			if (n==null) throw new NoSuchElementException();
			this.next = searchNext();
			return n;
		}

		@Override
		public void remove() {
			//
		}

	}

	/**
	 * Describe a 1.5D entity. 
	 * 
	 * @author St&eacute;phane GALLAND &lt;stephane.galland@utbm.fr&gt;
	 * @version $Name$ $Revision$ $Date$
	 */
	protected static class EntityDescriptor {

		private static int colorIndex = 0;
		private static Color[] colors = new Color[] {
			Color.YELLOW,
			Color.CYAN,
			Color.LIGHT_GRAY,
			Color.MAGENTA,
			Color.ORANGE,
			Color.PINK
		};

		/** Position 2D of the entity.
		 */
		private final Point2d position2d = new Point2d();

		/** Orientation of the entity.
		 */
		private double angle = 0.;

		/** Speed of the entity.
		 */
		private double speed = 0.;

		/** Acceleration of the entity.
		 */
		private double acceleration = 0.;

		/** Direction of the entity.
		 */
		private final Vector2d direction = new Vector2d();

		/** Entity dimension.
		 */
		private final Dimension2D dimension = new Dimension2DDouble();

		/** Position 1.5D of the entity.
		 */
		private Point1D5 position1d5 = null;

		/** Entity color.
		 */
		private final Color color;
		
		/** Description of the entity.
		 */
		private String label = null;

		/**
		 */
		public EntityDescriptor() {
			this.color = colors[colorIndex];
			colorIndex = (colorIndex+1) % colors.length;
		}

		/** Replies the color.
		 * 
		 * @return the color.
		 */
		public Color getColor() {
			return this.color;
		}

		/** Set this descriptor from the data of the given entity.
		 * 
		 * @param mobileEntity
		 */
		public final void set(MobileEntity1D5<BoundingRect1D5<RoadSegment>> mobileEntity) {
			this.label = mobileEntity.getIdentifier().toString();
			
			Point1D5 position = mobileEntity.getPosition1D5();
			Point2d p = mobileEntity.getPosition2D();
			Bounds1D5<RoadSegment> bounds = mobileEntity.getBounds1D5();

			this.position2d.set(p);
			this.position1d5 = new Point1D5(position);

			this.speed = mobileEntity.getLinearSpeed();
			this.acceleration = mobileEntity.getLinearAcceleration();

			this.dimension.setSize(
					bounds.getUpper().getCurvilineCoordinate() - bounds.getLower().getCurvilineCoordinate(),
					bounds.getLateralSize());

			RoadConnection entryPoint = mobileEntity.getRoadEntry();

			RoadPolyline road = (RoadPolyline)position.getSegment();
			double curvilineCoord = position.getCurvilineCoordinate();
			if (Double.isNaN(curvilineCoord)) {
				ErrorLogger.logWarning(this,
						"Curviline coordinate is NaN for entity " //$NON-NLS-1$
						+mobileEntity.getIdentifier().toString());
				curvilineCoord = 0;
			}
			else if (curvilineCoord<0) {
				ErrorLogger.logWarning(this,
						"Curviline coordinate is negative. curviline=" //$NON-NLS-1$
						+Double.toString(curvilineCoord)
						+" for entity " //$NON-NLS-1$
						+mobileEntity.getIdentifier().toString());
				curvilineCoord = 0;
			}
			else if (curvilineCoord>road.getLength()) {
				ErrorLogger.logWarning(this,
						"Curviline coordinate is too hight. curviline=" //$NON-NLS-1$
						+Double.toString(curvilineCoord)
						+">" //$NON-NLS-1$
						+Double.toString(road.getLength())
						+" on road " //$NON-NLS-1$
						+road.getUUID().toString()
						+" for entity " //$NON-NLS-1$
						+mobileEntity.getIdentifier().toString());
				curvilineCoord = road.getLength();
			}
			Segment1D subSegment = road.getSubSegmentForDistance(curvilineCoord);

			Point2d p1 = subSegment.getFirstPoint();
			Point2d p2 = subSegment.getLastPoint();

			this.direction.set(p2.x - p1.x, p2.y - p1.y);
			this.direction.normalize();

			if (!entryPoint.equals(road.getBeginPoint())) {
				this.direction.negate();
			}

			this.angle = GeometryUtil.signedAngle(
					JasimConstants.DEFAULT_VIEW_VECTOR_X,
					JasimConstants.DEFAULT_VIEW_VECTOR_Y,
					this.direction.x, this.direction.y);    		
		}

		/** Replies the bounds.
		 * 
		 * @return the bounds.
		 */
		public final Rectangle2D getBounds() {
			double maxsize = Math.max(this.dimension.getWidth(), this.dimension.getHeight());
			double demimaxsize = maxsize / 2.;
			return new Rectangle2D.Double(
					this.position2d.x - demimaxsize, this.position2d.y - demimaxsize,
					maxsize, maxsize);
		}

		/** Replies the position.
		 * 
		 * @return the position.
		 */
		public final Point2d getPosition() {
			return this.position2d;
		}

		/** Replies the curviline position.
		 * 
		 * @return the curviline position.
		 */
		public final double getCurvilinePosition() {
			return this.position1d5.getCurvilineCoordinate();
		}

		/** Replies the jutting distance.
		 * 
		 * @return the jutting distance.
		 */
		public final double getJuttingDistance() {
			return this.position1d5.getJuttingDistance();
		}

		/** Replies the dimension.
		 * 
		 * @return the dimension.
		 */
		public final Dimension2D getDimension() {
			return this.dimension;
		}

		/** Replies the angle.
		 * 
		 * @return the angle.
		 */
		public final double getAngle() {
			return this.angle;
		}

		/** Replies the speed.
		 * 
		 * @return the speed.
		 */
		public final double getSpeed() {
			return this.speed;
		}

		/** Replies the acceleration.
		 * 
		 * @return the acceleration.
		 */
		public final double getAcceleration() {
			return this.acceleration;
		}

		/** Replies the direction vector.
		 * 
		 * @return the direction.
		 */
		public final Vector2d getDirection() {
			return this.direction;
		}
		
		/** {@inheritDoc}
		 */
		@Override
		public String toString() {
			return this.label;
		}

	}

	/**
	 * Draw the content of a JaSIM layer. 
	 * 
	 * @author St&eacute;phane GALLAND &lt;stephane.galland@utbm.fr&gt;
	 * @version $Name$ $Revision$ $Date$
	 */
	protected static class JasimSimulatorDrawer extends AbstractGISDrawer<JasimSimulatorLayer1D5> {

		/**
		 */
		public JasimSimulatorDrawer() {
			super(JasimSimulatorLayer1D5.class);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void drawElement(JasimSimulatorLayer1D5 layer, GISGraphics2D gcontext, Color color) {
			Iterator<EntityDescriptor> iterator = layer.getEntities();
			EntityDescriptor descriptor;
			StringBuffer buffer = new StringBuffer();

			while (iterator.hasNext()) {
				descriptor = iterator.next();

				buffer.setLength(0);
				buffer.append(descriptor.toString());
				buffer.append("\n"); //$NON-NLS-1$
				buffer.append(TextUtil.formatDouble(descriptor.getCurvilinePosition(), 1));
				buffer.append(';');
				buffer.append(TextUtil.formatDouble(descriptor.getJuttingDistance(), 1));
				buffer.append("\n"); //$NON-NLS-1$
				buffer.append(TextUtil.formatDouble(descriptor.getSpeed(), 5));
				buffer.append("m/s\n"); //$NON-NLS-1$
				buffer.append(TextUtil.formatDouble(MeasureUnitUtil.ms2kmh(descriptor.getSpeed()), 5));
				buffer.append("km/h\n"); //$NON-NLS-1$
				buffer.append(TextUtil.formatDouble(descriptor.getAcceleration(), 5));
				buffer.append("m/s²"); //$NON-NLS-1$

				drawVehicle(gcontext, 
						descriptor.getBounds(),
						descriptor.getPosition(), 
						descriptor.getDimension(), 
						descriptor.getAngle(),
						descriptor.getDirection(),
						buffer.toString(),
						descriptor.getColor());
			}
		}

		private static void drawVehicle(
				GISGraphics2D gcontext, 
				Rectangle2D bounds,
				Point2d position, 
				Dimension2D dimension, 
				double angle, 
				Vector2d direction,
				String text,
				Color vehicleColor) {
			if (gcontext.logical2pixel_size(dimension.getWidth())<=DEFAULT_VEHICLE_DRAW_PRECISION) {
				// Draw a circle
				double radius = gcontext.pixel2logical_size(5); // DEFAULT_VEHICLE_RADIUS
				double diameter = gcontext.pixel2logical_size(10); // DEFAULT_VEHICLE_DIAMETER
				gcontext.setColor(vehicleColor);				
				gcontext.fillOval(
						position.getX()-radius, 
						position.getY()-radius,
						diameter,
						diameter);

				Vector2d v = new Vector2d(direction.getX(),direction.getY());
				v.normalize();
				v.scale(DEFAULT_VEHICLE_RADIUS);
				gcontext.setColor(VEHICLE_DIRECTION_COLOR);				
				gcontext.drawLine(position.getX(), position.getY(), position.getX()+v.x, position.getY()+v.y);
			}
			else {
				// Draw a car
				Shape shape = getNormalVehicleShape(
						position.getX(), position.getY(), angle, 
						dimension.getWidth(), dimension.getHeight());
				if (shape!=null && gcontext.hit(shape)) {
					gcontext.setColor(vehicleColor);				
					gcontext.fill(shape);

					gcontext.setColor(VEHICLE_BOUND_COLOR);
					gcontext.draw(shape);

					Vector2d v = new Vector2d(direction.getX(),direction.getY());
					v.normalize();
					v.scale(dimension.getWidth()/2.);
					gcontext.setColor(VEHICLE_DIRECTION_COLOR);				
					gcontext.drawLine(position.getX(), position.getY(), position.getX()+v.x, position.getY()+v.y);

					gcontext.drawCartridge(
							text, 
							position.getX()+bounds.getWidth(), position.getY()+bounds.getHeight(),
							LayerUIConstants.DEFAULT_CARTRIDGE_TEXT_COLOR,
							LayerUIConstants.DEFAULT_CARTRIDGE_BACKGROUND_COLOR,
							LayerUIConstants.DEFAULT_CARTRIDGE_BORDER_COLOR,
							TextAlignment.LEFT_ALIGN);
				}
			}
		}

		/** Replies the normal graphical representation of a vehicle.
		 * 
		 * @param x
		 * @param y
		 * @param angle
		 * @param width
		 * @param height
		 * @return the shape
		 */
		protected static Shape getNormalVehicleShape(double x, double y, double angle, double width, double height) {
			AffineTransform tr = new AffineTransform();
			Shape bodyShp = new Rectangle2D.Double(-width/2., -height/2., width, height);

			// Rotate
			tr.rotate(angle);
			bodyShp = tr.createTransformedShape(bodyShp);

			// Translate
			tr.setToIdentity();
			tr.translate(x, y);
			bodyShp = tr.createTransformedShape(bodyShp);

			return bodyShp;
		}

	}

}