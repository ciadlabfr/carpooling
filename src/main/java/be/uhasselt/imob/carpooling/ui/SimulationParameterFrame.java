/* 
 * $Id$
 * 
 * Copyright (C) 2012
 *               Transport Research Institute (IMOB),
 *               Laboratoire Systemes et Transports (IRTES-SET).
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of the Transport Research Institute (IMOB) and the
 * Laboratoire Systemes et Transports (IRTES-SET) of
 * the Institut de Recherche Transport Ernergie Société.
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * refered as NDA-MULTIAGEN-SET-IRTES-2012.
 * Contact for IMOB: Prof. dr. Davy JANSSENS.
 * Contact for IRTES-SET: Dr. Stephane GALLAND.
 */
package be.uhasselt.imob.carpooling.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import org.arakhne.afc.vmutil.FileSystem;
import org.arakhne.afc.vmutil.locale.Locale;

import be.uhasselt.imob.carpooling.util.SimulationParameters;
import fr.utbm.set.io.filefilter.CSVFileFilter;
import fr.utbm.set.io.shape.ShapeFileFilter;
import fr.utbm.set.ui.IconSize;
import fr.utbm.set.ui.PredefinedIcon;
import fr.utbm.set.ui.XBagConstraint;

/** Frame that permits to enter the simulation parameters.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class SimulationParameterFrame  extends JFrame implements SimulationParameters {

	private static final long serialVersionUID = 7195303470660168078L;

	private final JButton launchSimulationButton; 
	private boolean isCanceled = true;
	private File featherInputs = null;

	private final DirectoryPanel outputPane;
	private final SpinnerNumberModel populationModel = new SpinnerNumberModel(NB_PEOPLE, 1, 100000, 5000);
	private final SpinnerNumberModel fuelCostModel = new SpinnerNumberModel(FUEL_COST, 0.01f, 50f, 1f);
	private final SpinnerNumberModel simulationStepModel = new SpinnerNumberModel(SIMULATION_STEP, .1f, 3600f, 1f);
	private final SpinnerNumberModel arrivingDistanceModel = new SpinnerNumberModel(ARRIVING_DISTANCE, 1f, 400f, 10f);
	private final URLPanel roadNetworkPane;
	private final JCheckBox readDBase;
	private final URLPanel featherPane;
	private final SpinnerNumberModel cruiseSpeedModel = new SpinnerNumberModel(CRUISE_SPEED, 1f, 200f, 10f);
	private final SpinnerNumberModel dayCountModel = new SpinnerNumberModel(DAY_COUNT, 1, 7, 1);
	private final SpinnerNumberModel egocentricModel = new SpinnerNumberModel(EGO_CENTRIC_FACTOR, 0f, 1f, .01f);
	private final JCheckBox showUI;

	/**
	 * @throws BackingStoreException
	 * @throws MalformedURLException 
	 */
	public SimulationParameterFrame() throws BackingStoreException, MalformedURLException {
		super(Locale.getString("TITLE")); //$NON-NLS-1$

		setLayout(new BorderLayout());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		EventHandler eventHandler = new EventHandler();
		
		JPanel bottomPanel = new JPanel();
		add(BorderLayout.SOUTH, bottomPanel);
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));

		this.launchSimulationButton = new JButton(
				Locale.getString("LAUNCH_SIMULATION"), //$NON-NLS-1$
				PredefinedIcon.TASK_RUN.getIcon(IconSize.getButtonSize()));
		this.launchSimulationButton.addActionListener(new ActionListener() {
			@SuppressWarnings("synthetic-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				SimulationParameterFrame.this.isCanceled = false;
				SimulationParameterFrame.this.setVisible(false);
			}
		});
		this.launchSimulationButton.setEnabled(false);
		bottomPanel.add(this.launchSimulationButton);

		JButton cancelButton = new JButton(
				Locale.getString("CANCEL"), //$NON-NLS-1$
				PredefinedIcon.CANCEL.getIcon(IconSize.getButtonSize()));
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SimulationParameterFrame.this.setVisible(false);
			}
		});
		bottomPanel.add(cancelButton);

		JButton resetButton = new JButton(
				Locale.getString("RESET"), //$NON-NLS-1$
				PredefinedIcon.REFRESH.getIcon(IconSize.getButtonSize()));
		resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				resetToDefaults();
			}
		});
		bottomPanel.add(resetButton);

		JButton loadFeather = new JButton(
				Locale.getString("LOAD_FEATHER"), //$NON-NLS-1$
				PredefinedIcon.UNVISITED_DIRECTORY.getIcon(IconSize.getButtonSize()));
		loadFeather.setEnabled(false);
		loadFeather.addActionListener(new ActionListener() {
			@SuppressWarnings("synthetic-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				Preferences prefs = Preferences.userNodeForPackage(SimulationParameterFrame.class);
				File dir = null;
				try {
					dir = FileSystem.getUserHomeDirectory();
				}
				catch (FileNotFoundException _) {
					//
				}
				String p = prefs.get("lastDirectory", null); //$NON-NLS-1$
				if (p!=null && !p.isEmpty()) {
					dir = new File(p);
				}
				JFileChooser chooser = new JFileChooser(dir);
				chooser.setFileFilter(new SerializedFileFilter());
				if (chooser.showOpenDialog(SimulationParameterFrame.this)==
						JFileChooser.APPROVE_OPTION) {
					File selectedFile = chooser.getSelectedFile();
					prefs.put("lastDirectory", selectedFile.getParent()); //$NON-NLS-1$
					try {
						prefs.flush();
					}
					catch (BackingStoreException _) {
						//
					}
					SimulationParameterFrame.this.isCanceled = false;
					SimulationParameterFrame.this.featherInputs = selectedFile;
					SimulationParameterFrame.this.setVisible(false);
				}
			}
		});
		bottomPanel.add(loadFeather);

		JPanel centerPanel = new JPanel();
		add(BorderLayout.CENTER, centerPanel);
		centerPanel.setLayout(new GridBagLayout());

		XBagConstraint x = new XBagConstraint();
		JLabel label;
		JSpinner spinner;

		label = new JLabel(Locale.getString("POPULATION")); //$NON-NLS-1$
		centerPanel.add(label, x.pad());
		spinner = new JSpinner(this.populationModel);
		this.populationModel.addChangeListener(eventHandler);
		label.setLabelFor(spinner);
		centerPanel.add(spinner, x.nextCol().expandW().pad());

		label = new JLabel(Locale.getString("FUEL_COST")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		spinner = new JSpinner(this.fuelCostModel);
		this.fuelCostModel.addChangeListener(eventHandler);
		label.setLabelFor(spinner);
		centerPanel.add(spinner, x.nextCol().expandW().pad());

		label = new JLabel(Locale.getString("EGO_CENTRIC_FACTOR")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		spinner = new JSpinner(this.egocentricModel);
		this.egocentricModel.addChangeListener(eventHandler);
		label.setLabelFor(spinner);
		centerPanel.add(spinner, x.nextCol().expandW().pad());

		label = new JLabel(Locale.getString("SIMULATION_STEP")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		spinner = new JSpinner(this.simulationStepModel);
		this.simulationStepModel.addChangeListener(eventHandler);
		label.setLabelFor(spinner);
		centerPanel.add(spinner, x.nextCol().expandW().pad());

		label = new JLabel(Locale.getString("ROAD_NETWORK")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		this.roadNetworkPane = new URLPanel(
				new ShapeFileFilter(),
				IMOB_ROAD_NETWORK, IRTESSET_ROAD_NETWORK, TEST_ROAD_NETWORK);
		this.roadNetworkPane.addChangeListener(eventHandler);
		label.setLabelFor(this.roadNetworkPane);
		centerPanel.add(this.roadNetworkPane, x.nextCol().expandW().pad());

		label = new JLabel(Locale.getString("READ_DBASE")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		this.readDBase = new JCheckBox();
		this.readDBase.setSelected(false);
		this.readDBase.addChangeListener(eventHandler);
		centerPanel.add(this.readDBase, x.nextCol().pad());

		label = new JLabel(Locale.getString("FEATHER_DATA")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		this.featherPane = new URLPanel(
				new CSVFileFilter(),
				FEATHER_DATA);
		this.featherPane.addChangeListener(eventHandler);
		label.setLabelFor(this.featherPane);
		centerPanel.add(this.featherPane, x.nextCol().expandW().pad());

		label = new JLabel(Locale.getString("ARRIVING_DISTANCE")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		spinner = new JSpinner(this.arrivingDistanceModel);
		this.arrivingDistanceModel.addChangeListener(eventHandler);
		label.setLabelFor(spinner);
		centerPanel.add(spinner, x.nextCol().expandW().pad());

		label = new JLabel(Locale.getString("OUTPUT_DIRECTORY")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		this.outputPane = new DirectoryPanel();
		this.outputPane.addChangeListener(eventHandler);
		label.setLabelFor(this.outputPane);
		centerPanel.add(this.outputPane, x.nextCol().expandW().pad());

		label = new JLabel(Locale.getString("CRUISE_SPEED")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		spinner = new JSpinner(this.cruiseSpeedModel);
		this.cruiseSpeedModel.addChangeListener(eventHandler);
		label.setLabelFor(spinner);
		centerPanel.add(spinner, x.nextCol().expandW().pad());

		label = new JLabel(Locale.getString("DAY_COUNT")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		spinner = new JSpinner(this.dayCountModel);
		this.dayCountModel.addChangeListener(eventHandler);
		label.setLabelFor(spinner);
		centerPanel.add(spinner, x.nextCol().expandW().pad());

		label = new JLabel(Locale.getString("SHOW_UI")); //$NON-NLS-1$
		centerPanel.add(label, x.nextRow().pad());
		this.showUI = new JCheckBox();
		this.showUI.setSelected(true);
		centerPanel.add(this.showUI, x.nextCol().pad());

		readUserPreferences();

		pack();
		
		Dimension d = getSize();
		setLocation(-d.width/2,-d.height/2);
		setLocationRelativeTo(null);
	}
	
	/** {@inheritDoc}
	 */
	@Override
	public void setVisible(boolean b) {
		boolean o = isVisible();
		if (o!=b) {
			super.setVisible(b);
			if (b) {
				while (isVisible()) {
					Thread.yield();
				}
			}
		}
	}

	/** Reset the fields to the default values.
	 */
	public void resetToDefaults() {
		this.populationModel.setValue(NB_PEOPLE);
		this.fuelCostModel.setValue(FUEL_COST);
		this.simulationStepModel.setValue(SIMULATION_STEP);
		this.roadNetworkPane.resetToDefaults();
		this.readDBase.setSelected(false);
		this.featherPane.resetToDefaults();
		this.arrivingDistanceModel.setValue(ARRIVING_DISTANCE);
		this.outputPane.resetToDefaults();
		this.cruiseSpeedModel.setValue(CRUISE_SPEED);
		this.dayCountModel.setValue(DAY_COUNT);
		this.egocentricModel.setValue(EGO_CENTRIC_FACTOR);
		this.showUI.setSelected(true);
	}

	/** Read the parameters frmo the user preferences.
	 * 
	 * @throws BackingStoreException
	 */
	public void readUserPreferences() throws BackingStoreException {
		Preferences prefs = Preferences.userNodeForPackage(SimulationParameterFrame.class);
		prefs.sync();
		{
			int n = prefs.getInt("population", getPopulationSize()); //$NON-NLS-1$
			this.populationModel.setValue(n);
		}
		{
			float n = prefs.getFloat("fuelCost", getAverageFuelCost()); //$NON-NLS-1$
			this.fuelCostModel.setValue(n);
		}
		{
			float n = prefs.getFloat("simulationStep", getDrivingSimulationStep()); //$NON-NLS-1$
			this.simulationStepModel.setValue(n);
		}
		{
			URL u = getRoadNetwork();
			String p = prefs.get("roadNetworkUrl", null); //$NON-NLS-1$
			if (p!=null && !p.isEmpty()) {
				try {
					u = FileSystem.convertStringToURL(p, true);
				}
				catch(Throwable _) {
					u = getRoadNetwork();
				}
			}
			this.roadNetworkPane.setURL(u);
		}
		{
			boolean b = prefs.getBoolean("roadDBase", false); //$NON-NLS-1$
			this.readDBase.setSelected(b);
		}
		{
			URL u = getFeatherCSV();
			String p = prefs.get("featherUrl", null); //$NON-NLS-1$
			if (p!=null && !p.isEmpty()) {
				try {
					u = FileSystem.convertStringToURL(p, true);
				}
				catch(Throwable _) {
					u = getFeatherCSV();
				}
			}
			this.featherPane.setURL(u);
		}
		{
			float n = prefs.getFloat("arrivingDistance", getVehicleArrivingDistance()); //$NON-NLS-1$
			this.arrivingDistanceModel.setValue(n);
		}
		{
			File f = getOutputDirectory();
			String p = prefs.get("outputDirectory", null); //$NON-NLS-1$
			if (p!=null && !p.isEmpty()) {
				try {
					f = new File(p);
				}
				catch(Throwable _) {
					f = getOutputDirectory();
				}
			}
			this.outputPane.setFile(f);
		}
		{
			float n = prefs.getFloat("cruiseSpeed", getCruiseSpeed()); //$NON-NLS-1$
			this.fuelCostModel.setValue(n);
		}
		{
			int n = prefs.getInt("dayCount", getNumberOfDays()); //$NON-NLS-1$
			this.dayCountModel.setValue(n);
		}
		{
			float n = prefs.getFloat("egocentric", getEgocentricFactor()); //$NON-NLS-1$
			this.egocentricModel.setValue(n);
		}
		{
			boolean b = prefs.getBoolean("showUI", true); //$NON-NLS-1$
			this.showUI.setSelected(b);
		}
	}

	/** Save the parameters in the user preferences.
	 * 
	 * @throws BackingStoreException
	 */
	public void saveUserPreferences() throws BackingStoreException {
		Preferences prefs = Preferences.userNodeForPackage(SimulationParameterFrame.class);
		{
			Integer i = null;
			Object value = this.populationModel.getValue();
			if (value instanceof Number) {
				int n = ((Number)value).intValue();
				if (n!=NB_PEOPLE) i = Integer.valueOf(n);
			}
			if (i==null) prefs.remove("population"); //$NON-NLS-1$
			else prefs.putInt("population", i.intValue()); //$NON-NLS-1$
		}
		{
			Float f = null;
			Object value = this.fuelCostModel.getValue();
			if (value instanceof Number) {
				float n = ((Number)value).floatValue();
				if (n!=FUEL_COST) f = Float.valueOf(n);
			}
			if (f==null) prefs.remove("fuelCost"); //$NON-NLS-1$
			else prefs.putFloat("fuelCost", f.floatValue()); //$NON-NLS-1$
		}
		{
			Float i = null;
			Object value = this.simulationStepModel.getValue();
			if (value instanceof Number) {
				float n = ((Number)value).floatValue();
				if (n!=SIMULATION_STEP) i = Float.valueOf(n);
			}
			if (i==null) prefs.remove("simulationStep"); //$NON-NLS-1$
			else prefs.putFloat("simulationStep", i.floatValue()); //$NON-NLS-1$
		}
		{
			URL u = getRoadNetwork();
			if (u==null) prefs.remove("roadNetworkUrl"); //$NON-NLS-1$
			else prefs.put("roadNetworkUrl", u.toExternalForm()); //$NON-NLS-1$
		}
		{
			prefs.putBoolean("readDBase", this.readDBase.isSelected()); //$NON-NLS-1$
		}
		{
			URL u = getFeatherCSV();
			if (u==null) prefs.remove("featherUrl"); //$NON-NLS-1$
			else prefs.put("featherUrl", u.toExternalForm()); //$NON-NLS-1$
		}
		{
			Float f = null;
			Object value = this.arrivingDistanceModel.getValue();
			if (value instanceof Number) {
				float n = ((Number)value).floatValue();
				if (n!=ARRIVING_DISTANCE) f = Float.valueOf(n);
			}
			if (f==null) prefs.remove("arrivingDistance"); //$NON-NLS-1$
			else prefs.putFloat("arrivingDistance", f.floatValue()); //$NON-NLS-1$
		}
		{
			File f = getOutputDirectory();
			if (f==null) prefs.remove("outputDirectory"); //$NON-NLS-1$
			else prefs.put("outputDirectory", f.getAbsolutePath()); //$NON-NLS-1$
		}
		{
			Float f = null;
			Object value = this.cruiseSpeedModel.getValue();
			if (value instanceof Number) {
				float n = ((Number)value).floatValue();
				if (n!=CRUISE_SPEED) f = Float.valueOf(n);
			}
			if (f==null) prefs.remove("cruiseSpeed"); //$NON-NLS-1$
			else prefs.putFloat("cruiseSpeed", f.floatValue()); //$NON-NLS-1$
		}
		{
			Integer i = null;
			Object value = this.dayCountModel.getValue();
			if (value instanceof Number) {
				int n = ((Number)value).intValue();
				if (n!=DAY_COUNT) i = Integer.valueOf(n);
			}
			if (i==null) prefs.remove("dayCount"); //$NON-NLS-1$
			else prefs.putInt("dayCount", i.intValue()); //$NON-NLS-1$
		}
		{
			Float f = null;
			Object value = this.egocentricModel.getValue();
			if (value instanceof Number) {
				float n = ((Number)value).floatValue();
				if (n!=EGO_CENTRIC_FACTOR) f = Float.valueOf(n);
			}
			if (f==null) prefs.remove("egocentric"); //$NON-NLS-1$
			else prefs.putFloat("egocentric", f.floatValue()); //$NON-NLS-1$
		}
		{
			prefs.putBoolean("showUI", this.showUI.isSelected()); //$NON-NLS-1$
		}
		prefs.flush();
	}

	/** Replies if the dialog box was canceled or not.
	 * 
	 * @return <code>true</code> if the dialog box was canceled;
	 * <code>false</code> if not.
	 */
	public boolean isCanceled() {
		return this.isCanceled;
	}

	/** Replies the serialized FEATHER data.
	 * 
	 * @return the data file or <code>null</code>.
	 */
	public File getFeatherSerializedInputs() {
		return this.featherInputs;
	}
	
	@Override
	public URL getRoadNetwork() {
		return this.roadNetworkPane.getURL();
	}

	/** {@inheritDoc}
	 */
	@Override
	public boolean getReadDBase() {
		return this.readDBase.isSelected();
	}

	/** {@inheritDoc}
	 */
	@Override
	public boolean isUIDisplayed() {
		return this.showUI.isSelected();
	}

	@Override
	public URL getFeatherCSV() {
		return this.featherPane.getURL();
	}

	@Override
	public int getPopulationSize() {
		Object obj = this.populationModel.getValue();
		if (obj instanceof Number) {
			return ((Number)obj).intValue();
		}
		return NB_PEOPLE;
	}

	/** {@inheritDoc}
	 */
	@Override
	public int getNumberOfDays() {
		Object obj = this.dayCountModel.getValue();
		if (obj instanceof Number) {
			return ((Number)obj).intValue();
		}
		return DAY_COUNT;
	}

	@Override
	public float getDrivingSimulationStep() {
		Object obj = this.simulationStepModel.getValue();
		if (obj instanceof Number) {
			return ((Number)obj).floatValue();
		}
		return SIMULATION_STEP;
	}

	@Override
	public float getAverageFuelCost() {
		Object obj = this.fuelCostModel.getValue();
		if (obj instanceof Number) {
			return ((Number)obj).floatValue();
		}
		return FUEL_COST;
	}

	/** {@inheritDoc}
	 */
	@Override
	public float getEgocentricFactor() {
		Object obj = this.egocentricModel.getValue();
		if (obj instanceof Number) {
			return ((Number)obj).floatValue();
		}
		return EGO_CENTRIC_FACTOR;
	}

	/** {@inheritDoc}
	 */
	@Override
	public float getCruiseSpeed() {
		Object obj = this.cruiseSpeedModel.getValue();
		if (obj instanceof Number) {
			return ((Number)obj).floatValue();
		}
		return CRUISE_SPEED;
	}

	/** {@inheritDoc}
	 */
	@Override
	public float getVehicleArrivingDistance() {
		Object obj = this.arrivingDistanceModel.getValue();
		if (obj instanceof Number) {
			return ((Number)obj).floatValue();
		}
		return ARRIVING_DISTANCE;
	}

	/** {@inheritDoc}
	 */
	@Override
	public File getOutputDirectory() {
		return this.outputPane.getFile();
	}

	/**
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private class EventHandler implements ChangeListener {
		
		/**
		 */
		public EventHandler() {
			//
		}

		/** {@inheritDoc}
		 */
		@SuppressWarnings("synthetic-access")
		@Override
		public void stateChanged(ChangeEvent e) {
			boolean canLaunch =
					SimulationParameterFrame.this.roadNetworkPane.getURL()!=null
					&&
					SimulationParameterFrame.this.featherPane.getURL()!=null;
			SimulationParameterFrame.this.launchSimulationButton.setEnabled(canLaunch);
		}
		
	} // class EventHandler

	/** Panel to enter an URL.
	 * 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private class URLPanel extends JPanel {

		private static final long serialVersionUID = 5599472312634257584L;

		private final FileFilter fileFilter;
		private final URL[] defaultURLs;
		private final DefaultComboBoxModel urlList = new DefaultComboBoxModel();
		private final JComboBox urlListComponent = new JComboBox(this.urlList);

		/**
		 * @param filter
		 * @param defaultURL
		 * @throws MalformedURLException 
		 */
		public URLPanel(FileFilter filter, String... defaultURL) throws MalformedURLException {
			this.fileFilter = filter;
			this.defaultURLs = new URL[defaultURL.length];
			for(int i=0; i<defaultURL.length; ++i) {
				this.defaultURLs[i] = 
						FileSystem.toShortestURL(FileSystem.convertStringToURL(defaultURL[i], true));
			}
			setLayout(new BorderLayout());
			this.urlListComponent.setMinimumSize(new Dimension(100, 12));
			this.urlListComponent.setPreferredSize(new Dimension(200, 12));
			this.urlListComponent.setEditable(false);
			this.urlListComponent.setRenderer(new URLPanelRenderer());
			this.urlListComponent.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					fireChange();
				}
			});
			add(BorderLayout.CENTER, this.urlListComponent);
			JButton selectFile = new JButton(Locale.getString(SimulationParameterFrame.class, "SELECT_FILE")); //$NON-NLS-1$
			selectFile.addActionListener(new ActionListener() {
				@SuppressWarnings("synthetic-access")
				@Override
				public void actionPerformed(ActionEvent e) {
					Preferences prefs = Preferences.userNodeForPackage(SimulationParameterFrame.class);
					File dir = null;
					try {
						dir = FileSystem.getUserHomeDirectory();
					}
					catch (FileNotFoundException _) {
						//
					}
					String p = prefs.get("lastDirectory", null); //$NON-NLS-1$
					if (p!=null && !p.isEmpty()) {
						dir = new File(p);
					}
					JFileChooser chooser = new JFileChooser(dir);
					chooser.setFileFilter(URLPanel.this.fileFilter);
					if (chooser.showOpenDialog(SimulationParameterFrame.this)==
							JFileChooser.APPROVE_OPTION) {
						File selectedFile = chooser.getSelectedFile();
						prefs.put("lastDirectory", selectedFile.getParent()); //$NON-NLS-1$
						try {
							prefs.flush();
						}
						catch (BackingStoreException _) {
							//
						}
						setURL(FileSystem.convertFileToURL(selectedFile));
					}
				}
			});
			add(BorderLayout.EAST, selectFile);
			
			resetToDefaults();
		}
		
		/**
		 * @param listener
		 */
		public void addChangeListener(ChangeListener listener) {
			this.listenerList.add(ChangeListener.class, listener);
		}
		
		protected void fireChange() {
			ChangeEvent e = new ChangeEvent(this);
			for(ChangeListener l : this.listenerList.getListeners(ChangeListener.class)) {
				l.stateChanged(e);
			}
		}

		/** Reset the fields to the default values.
		 */
		public void resetToDefaults() {
			this.urlList.removeAllElements();
			for(URL d : this.defaultURLs) {
				this.urlList.addElement(d);
			}
			boolean hasUrl = this.urlList.getSize()>0;
			if (hasUrl) {
				this.urlListComponent.setSelectedIndex(0);
			}
			this.urlListComponent.setEnabled(hasUrl);
		}

		/** Change the url.
		 * 
		 * @param u
		 */
		public void setURL(URL u) {
			int index = this.urlList.getIndexOf(u);
			if (index<0 || index>=this.urlList.getSize()) {
				index = this.urlList.getSize();
				this.urlList.addElement(u);
			}
			this.urlListComponent.setSelectedIndex(index);
			this.urlListComponent.setEnabled(index>=0 && index<this.urlList.getSize());
		}

		/** Replies the url.
		 * 
		 * @return the url
		 */
		public URL getURL() {
			Object o = this.urlListComponent.getSelectedItem();
			if (o instanceof URL) {
				return (URL)o;
			}
			return null;
		}

	} // class URLPanel

	/** Renderer for the URLs in the URLPanel.
	 * 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 * @see URLPanel
	 */
	private static class URLPanelRenderer extends DefaultListCellRenderer {

		private static final long serialVersionUID = 3637876989395334803L;

		/**
		 */
		public URLPanelRenderer() {
			//
		}
		
		/** {@inheritDoc}
		 */
		@Override
		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			String label = null; 
			if (value instanceof URL) {
				URL u = (URL)value;
				label = FileSystem.basename(u)
						+" - " //$NON-NLS-1$
						+FileSystem.dirname(u);
			}
			else if (value!=null) {
				label = value.toString();
			}
			return super.getListCellRendererComponent(
					list, label, index, isSelected,
					cellHasFocus);
		}
		
	} // class URLPanelRenderer
	
	/** Panel to enter a directory.
	 * 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private class DirectoryPanel extends JPanel {

		private static final long serialVersionUID = -5669838705263434664L;
		
		private final FileFilter fileFilter = new DirectoryFilter();
		private final JTextField outputFile = new JTextField();

		/**
		 */
		public DirectoryPanel() {
			setLayout(new BorderLayout());
			this.outputFile.setEditable(false);
			add(BorderLayout.CENTER, this.outputFile);
			JButton selectFile = new JButton(Locale.getString(SimulationParameterFrame.class, "SELECT_FILE")); //$NON-NLS-1$
			selectFile.addActionListener(new ActionListener() {
				@SuppressWarnings("synthetic-access")
				@Override
				public void actionPerformed(ActionEvent e) {
					Preferences prefs = Preferences.userNodeForPackage(SimulationParameterFrame.class);
					File dir = null;
					try {
						dir = FileSystem.getUserHomeDirectory();
					}
					catch (FileNotFoundException _) {
						//
					}
					String p = prefs.get("lastDirectory", null); //$NON-NLS-1$
					if (p!=null && !p.isEmpty()) {
						dir = new File(p);
					}
					JFileChooser chooser = new JFileChooser(dir);
					chooser.setFileFilter(DirectoryPanel.this.fileFilter);
					chooser.setAcceptAllFileFilterUsed(false);
					chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					if (chooser.showOpenDialog(SimulationParameterFrame.this) 
							==
							JFileChooser.APPROVE_OPTION) {
						File selectedFile = chooser.getSelectedFile();
						prefs.put("lastDirectory", selectedFile.getAbsolutePath()); //$NON-NLS-1$
						try {
							prefs.flush();
						}
						catch (BackingStoreException _) {
							//
						}
						setFile(selectedFile);
					}
				}
			});
			add(BorderLayout.EAST, selectFile);
			
			resetToDefaults();
		}
		
		/**
		 * @param listener
		 */
		public void addChangeListener(ChangeListener listener) {
			this.listenerList.add(ChangeListener.class, listener);
		}
		
		protected void fireChange() {
			ChangeEvent e = new ChangeEvent(this);
			for(ChangeListener l : this.listenerList.getListeners(ChangeListener.class)) {
				l.stateChanged(e);
			}
		}

		/** Reset the fields to the default values.
		 */
		public void resetToDefaults() {
			this.outputFile.setText(FileSystem.getUserHomeDirectoryName());
		}

		/** Change the file.
		 * 
		 * @param f
		 */
		public void setFile(File f) {
			File old = getFile();
			if (f==null || !this.fileFilter.accept(f)) {
				if (old!=null) {
					this.outputFile.setText(null);
					fireChange();
				}
			}
			else {
				if (!f.equals(old)) {
					this.outputFile.setText(f.getAbsolutePath());
					fireChange();
				}
			}
		}

		/** Replies the file.
		 * 
		 * @return the file
		 */
		public File getFile() {
			String s = this.outputFile.getText();
			if (s!=null && !s.isEmpty()) {
				return new File(s);
			}
			return null;
		}

	} // class FilePanel

	/** 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private static class DirectoryFilter extends FileFilter {
		
		/**
		 */
		public DirectoryFilter() {
			super();
		}

		/** {@inheritDoc}
		 */
		@Override
		public boolean accept(File f) {
			return f.isDirectory();
		}

		/** {@inheritDoc}
		 */
		@Override
		public String getDescription() {
			return Locale.getString(SimulationParameterFrame.class, "DIRECTORY_FILTER"); //$NON-NLS-1$
		}
		
	} // class DirectoryFilter
	
	/** 
	 * @author $Author: sgalland$
	 * @version $FullVersion$
	 * @mavengroupid $GroupId$
	 * @mavenartifactid $ArtifactId$
	 */
	private static class SerializedFileFilter extends FileFilter {
		
		/**
		 */
		public SerializedFileFilter() {
			super();
		}

		/** {@inheritDoc}
		 */
		@Override
		public boolean accept(File f) {
			return f.isDirectory() || FileSystem.hasExtension(f, ".feather"); //$NON-NLS-1$
		}

		/** {@inheritDoc}
		 */
		@Override
		public String getDescription() {
			return Locale.getString(SimulationParameterFrame.class, "FEATHER_DATA_FILE"); //$NON-NLS-1$
		}
		
	} // class SerializedFileFilter

}
