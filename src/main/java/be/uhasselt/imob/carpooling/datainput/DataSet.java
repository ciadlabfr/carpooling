/* 
 * $Id$
 * 
 * Copyright (c) 2013, Multiagent Team,
 * Laboratoire Systemes et Transports,
 * Institut de Recherche sur le Transport, l'Energie et la Societe,
 * Universite de Technologie de Belfort-Montbeliard.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of the Laboratoire Systemes et Transports
 * of the Universite de Technologie de Belfort-Montbeliard ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with the SeT.
 *
 * http://www.multiagent.fr/
 */

package be.uhasselt.imob.carpooling.datainput;

import java.util.GregorianCalendar;
import java.util.List;

import fr.utbm.set.gis.road.StandardRoadNetwork;

/** Data set.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class DataSet {

	/** Description of the population.
	 */
	public final List<IndividualDailyDescription> population;
	
	/** Start date.
	 */
	public final GregorianCalendar startDate;
	
	/** End date.
	 */
	public final GregorianCalendar endDate;
	
	/** Road network.
	 */
	public final StandardRoadNetwork roadNetwork;

	/**
	 * @param startDate
	 * @param endDate
	 * @param roadNetwork
	 * @param population
	 */
	public DataSet(GregorianCalendar startDate, GregorianCalendar endDate, StandardRoadNetwork roadNetwork, List<IndividualDailyDescription> population) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.roadNetwork = roadNetwork;
		this.population = population;
	}

}
