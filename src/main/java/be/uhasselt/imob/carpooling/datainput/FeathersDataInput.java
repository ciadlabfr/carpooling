/* 
 * $Id$
 * 
 * Copyright (c) 2013, Multiagent Team,
 * Laboratoire Systemes et Transports,
 * Institut de Recherche sur le Transport, l'Energie et la Societe,
 * Universite de Technologie de Belfort-Montbeliard.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of the Laboratoire Systemes et Transports
 * of the Universite de Technologie de Belfort-Montbeliard ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with the SeT.
 *
 * http://www.multiagent.fr/
 */

package be.uhasselt.imob.carpooling.datainput;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;

import org.arakhne.afc.progress.Progression;
import org.arakhne.afc.util.OutputParameter;
import org.arakhne.afc.vmutil.FileSystem;
import org.arakhne.afc.vmutil.locale.Locale;

import au.com.bytecode.opencsv.CSVReader;
import be.uhasselt.imob.carpooling.environment.Location;
import be.uhasselt.imob.carpooling.kernel.time.DayOfWeek;
import be.uhasselt.imob.carpooling.kernel.time.Time;
import be.uhasselt.imob.carpooling.schedule.Trip;
import be.uhasselt.imob.carpooling.schedule.TripComponent;
import be.uhasselt.imob.carpooling.schedule.TripMode;
import be.uhasselt.imob.carpooling.util.Gender;
import be.uhasselt.imob.carpooling.util.SimulationParameters;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.gis.road.StandardRoadNetwork;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.gis.road.path.astar.RoadAStar;
import fr.utbm.set.gis.road.primitive.RoadNetwork;


/** Read data for the Carpooling simulation from FEATHERS.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class FeathersDataInput extends DataInput {

	/**
	 */
	public FeathersDataInput() {
		//
	}

	/** {@inheritDoc}
	 * @throws IOException 
	 */
	@Override
	protected void readPopulation(SimulationParameters parameters,
			StandardRoadNetwork roadNetwork,
			List<IndividualDailyDescription> population,
			OutputParameter<GregorianCalendar> startDate,
			OutputParameter<GregorianCalendar> endDate, Progression progress) throws IOException {
		URL featherData = parameters.getFeatherCSV();
		if (featherData==null) throw new IOException(Locale.getString("NO_FEATHER_CSV")); //$NON-NLS-1$

		int populationSize = parameters.getPopulationSize();

		if (progress!=null) {
			progress.setProperties(0, 0, populationSize, false);
			progress.setComment(Locale.getString("READING", FileSystem.basename(featherData))); //$NON-NLS-1$
		}

		CSVReader csvReader = new CSVReader(new InputStreamReader(featherData.openStream()), ';');
		// Read column names
		csvReader.readNext();
		// Read first line
		String[] columns = csvReader.readNext();

		Map<Integer,Point1D5> zones = new TreeMap<Integer,Point1D5>();
		Map<Integer,IndividualDailyDescription> people = new TreeMap<Integer,IndividualDailyDescription>();
		Set<DayOfWeek> days = new TreeSet<DayOfWeek>();
		Set<Integer> voyagers = new TreeSet<Integer>();

		int prevPersonId = -1;
		IndividualDailyDescription peopleData = null;

		while (columns!=null) {

			int personId = Integer.parseInt(columns[7]);
			DayOfWeek day = DayOfWeek.values()[Integer.parseInt(columns[13])-1];
			
			if (peopleData==null || personId!=prevPersonId) {
				prevPersonId = personId;
				peopleData = people.get(personId);
			}
			if (peopleData==null) {
				if (voyagers.size()<populationSize) {
					Gender gender = Integer.parseInt(columns[10])==1 ? Gender.MALE : Gender.FEMALE;
					int age = Integer.parseInt(columns[8]) * 10;
					boolean driver = Integer.parseInt(columns[11])==1;
					boolean car = Integer.parseInt(columns[6])>0;
					peopleData = new IndividualDailyDescription("Carpooler #"+personId, gender, age, driver, car); //$NON-NLS-1$
					people.put(personId, peopleData);
					population.add(peopleData);
				}
				else if (days.size()>=parameters.getNumberOfDays()) {
					break;// stop to read the csv
				}
			}
			if (peopleData!=null) {
				int activityLocationId = Integer.parseInt(columns[17]);
				if (activityLocationId!=-1) {
					float distance = Float.parseFloat(columns[20]) - peopleData.totalDistance;
					int duration = Integer.parseInt(columns[18]);
					if (distance>0f && duration>0) {
						Point1D5 zoneCentroid;
						RoadPath path = null;
						if (day!=peopleData.day) {
							// Reset the buffered data because a new day is encountered
							days.add(day);
							peopleData.totalDistance = 0;
							peopleData.day = day;
							peopleData.firstPosition = peopleData.lastPosition = null;
							zoneCentroid = zones.get(activityLocationId);
							if (zoneCentroid==null) {
								zoneCentroid = randomPosition1d5(roadNetwork);
								zones.put(activityLocationId, zoneCentroid);
							}
						}
						else {
							zoneCentroid = zones.get(activityLocationId);
							if (zoneCentroid==null) {
								// Compute the path from the current position to the activity's location
								OutputParameter<RoadPath> opath = new OutputParameter<RoadPath>();
								zoneCentroid = random(roadNetwork, peopleData.lastPosition, distance*1000, opath);
								path = opath.get();
								zones.put(activityLocationId, zoneCentroid);
							}
						}

						peopleData.totalDistance += distance;

						String strHour = columns[15];
						int mins = Integer.parseInt(strHour.substring(strHour.length()-2));
						int hours = Integer.parseInt(strHour)/100;
						Time startTime = new Time(day, hours, mins);
						Time endTime = startTime.clone();
						endTime.add(duration);
						
						if (endTime.before(startTime)) {
							System.err.println("Invalid time interval"); //$NON-NLS-1$
							endTime = startTime.clone();
							endTime.add(1);
						}

						Point1D5 source = peopleData.lastPosition;
						if (source==null) {
							// Source is unknown => compute the path from destination to home's centroid.
							int homeId = Integer.parseInt(columns[1]);
							source = zones.get(homeId);
							if (source==null) {
								OutputParameter<RoadPath> opath = new OutputParameter<RoadPath>();
								source = random(roadNetwork, zoneCentroid, distance*1000, opath);
								path = opath.get();
								if (path==null) throw new IllegalStateException(Locale.getString("PATH_NOT_FOUND", Integer.toString(personId))); //$NON-NLS-1$
								path.invert(); // Invert the path because the destination is the zone centroid! See below. 
								zones.put(homeId, source);
							}
						}
						Point1D5 destination = new Point1D5(zoneCentroid);
						if (peopleData.firstPosition==null)
							peopleData.firstPosition = source;
						peopleData.lastPosition = destination;

						TripMode mode;
						{
							int tm = Integer.parseInt(columns[13]);
							switch(tm) {
							case 1:
								mode = TripMode.CAR;
								break;
							case 3:
								mode = TripMode.SLOW;
								break;
							case 4:
								mode = TripMode.PUBLIC_TRANSPORT;
								break;
							case 6:
								mode = TripMode.CAR_PASSENGER;
								break;
							default:
								mode = TripMode.CAR;
								break;
							}
						}

						//TODO: Select the real location
						endTime.add(0);
						TripComponent tripComponent = new TripComponent(
								new Location(source),
								new Location(destination),
								startTime,
								endTime,
								mode,
								path);
						Trip trip = new Trip();
						trip.add(tripComponent);

						GregorianCalendar c = startTime.toCalendar();
						GregorianCalendar dt = startDate.get();
						if (dt==null || dt.compareTo(c)>0) {
							startDate.set(c);
						}
						c = endTime.toCalendar();
						dt = endDate.get();
						if (dt==null || dt.compareTo(c)<0) {
							endDate.set(c);
						}

						voyagers.add(personId);
						if (progress!=null) progress.setValue(voyagers.size());
						peopleData.trips.add(trip);
					}
				}
			}

			columns = csvReader.readNext();
		}

		csvReader.close();
		if (progress!=null) progress.end();
	}

	private static Point1D5 random(RoadNetwork roadNetwork, Point1D5 previous, double distance, OutputParameter<RoadPath> outputPath) {
		float[] factors = new float[] { 1, 1, -1, -1, 1, -1, -1, 1 };
		Point2d b = previous.toPoint2D();

		SortedMap<Double,Point1D5> lowPoints = new TreeMap<Double,Point1D5>(Collections.reverseOrder());
		SortedMap<Double,Point1D5> highPoints = new TreeMap<Double,Point1D5>();
		
		for(int nbVectors=0; nbVectors<10; ++nbVectors) {
			double x = (Math.random() * 2) - 1;
			double y = (Math.random() * 2) - 1;
	
			for(int i=0; i<factors.length; i+=2) {
	
				Vector2d v = new Vector2d(factors[i]*x, factors[i+1]*y);
				v.normalize();
				v.scale(distance);
				Point2d p2d = previous.toPoint2D();
				p2d.add(v);
				Point1D5 p = roadNetwork.getNearestPosition(p2d);
				if (p!=null && p.getSegment()!=null) {
					Point2d a = p.toPoint2D();
					double dist = a.distance(b);
					if (dist<distance) {
						lowPoints.put(dist, p);
					}
					else {
						highPoints.put(dist, p);
					}
				}
			}
		}
		
		RoadAStar astar = new RoadAStar();

		Point1D5 bestPoint = null;
		if (!highPoints.isEmpty()) {
			for(Point1D5 p : highPoints.values()) {
				if (bestPoint==null) bestPoint = p;
				RoadPath path = astar.solve(b, p.toPoint2D(), roadNetwork);
				if (path!=null) {
					if (outputPath!=null) {
						outputPath.set(path);
					}
					return p;
				}
			}
		}

		for(Point1D5 p : lowPoints.values()) {
			if (bestPoint==null) bestPoint = p;
			RoadPath path = astar.solve(b, p.toPoint2D(), roadNetwork);
			if (path!=null) {
				if (outputPath!=null) {
					outputPath.set(path);
				}
				return p;
			}
		}

		if (bestPoint==null) throw new IllegalStateException();
		if (outputPath!=null) {
			outputPath.set(null);
		}
		return bestPoint;
	}

}
