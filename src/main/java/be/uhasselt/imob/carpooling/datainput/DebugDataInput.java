/* 
 * $Id$
 * 
 * Copyright (c) 2013, Multiagent Team,
 * Laboratoire Systemes et Transports,
 * Institut de Recherche sur le Transport, l'Energie et la Societe,
 * Universite de Technologie de Belfort-Montbeliard.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of the Laboratoire Systemes et Transports
 * of the Universite de Technologie de Belfort-Montbeliard ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with the SeT.
 *
 * http://www.multiagent.fr/
 */

package be.uhasselt.imob.carpooling.datainput;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.List;

import javax.vecmath.Point2d;

import org.arakhne.afc.progress.Progression;
import org.arakhne.afc.util.OutputParameter;

import be.uhasselt.imob.carpooling.environment.Location;
import be.uhasselt.imob.carpooling.kernel.time.DayOfWeek;
import be.uhasselt.imob.carpooling.kernel.time.Time;
import be.uhasselt.imob.carpooling.schedule.Trip;
import be.uhasselt.imob.carpooling.schedule.TripComponent;
import be.uhasselt.imob.carpooling.schedule.TripMode;
import be.uhasselt.imob.carpooling.util.Gender;
import be.uhasselt.imob.carpooling.util.SimulationParameters;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.gis.road.StandardRoadNetwork;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.gis.road.path.astar.RoadAStar;


/** Generate a population for debugging purpose.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class DebugDataInput extends DataInput {

	/**
	 */
	public DebugDataInput() {
		//
	}

	/** {@inheritDoc}
	 */
	@Override
	protected void readPopulation(SimulationParameters parameters,
			StandardRoadNetwork roadNetwork,
			List<IndividualDailyDescription> population,
			OutputParameter<GregorianCalendar> startDate,
			OutputParameter<GregorianCalendar> endDate, Progression progress)
					throws IOException {
		/* Trip:
		 * {[(952393.6, 2312898.3)@MONDAY@07:02]->[(935541.2955489835, 2313812.7394528645)@MONDAY@07:14]}
		 */
		add(952393.6, 2312898.3, 7, 2, 935541.2955489835, 2313812.7394528645, 7, 14,
				roadNetwork, population, startDate, endDate);
	}
	
	private static void add(double sx, double sy, int sh, int sm, double dx, double dy, int dh, int dm,
			StandardRoadNetwork roadNetwork,
			List<IndividualDailyDescription> population,
			OutputParameter<GregorianCalendar> startDate,
			OutputParameter<GregorianCalendar> endDate) {
		IndividualDailyDescription description;
		Point2d home, work;
		Point1D5 home1d5, work1d5;
		Location homeLocation, workLocation;
		RoadAStar astar;
		RoadPath path1, path2;
		Time time1, time2;
		Trip trip;

		description = new IndividualDailyDescription("Debugger", Gender.MALE, 20, true, true); //$NON-NLS-1$
		home = new Point2d(sx, sy);
		work = new Point2d(dx, dy);

		home1d5 = roadNetwork.getNearestPosition(home);
		work1d5 = roadNetwork.getNearestPosition(work);

		astar = new RoadAStar();
		path1 = astar.solve(home, work, roadNetwork);
		path2 = astar.solve(work, home, roadNetwork);

		homeLocation = new Location(home1d5);
		workLocation = new Location(work1d5);

		description.firstPosition = homeLocation.getGISPosition();
		description.lastPosition = homeLocation.getGISPosition();
		description.day = DayOfWeek.MONDAY;
		description.totalDistance = (float)(path1.getLength() + path2.getLength());

		time1 = new Time(DayOfWeek.MONDAY, sh, sm);
		time2 = new Time(DayOfWeek.MONDAY, dh, dm);
		
		trip = new Trip();
		description.trips.add(trip);
		trip.add(new TripComponent(
				homeLocation, workLocation,
				time1,
				time2,
				TripMode.CAR,
				path1));
		
		population.add(description);
		startDate.set(time1.toCalendar());
		endDate.set(time2.toCalendar());
	}

}
