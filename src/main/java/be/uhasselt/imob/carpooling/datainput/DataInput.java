/* 
 * $Id$
 * 
 * Copyright (c) 2013, Multiagent Team,
 * Laboratoire Systemes et Transports,
 * Institut de Recherche sur le Transport, l'Energie et la Societe,
 * Universite de Technologie de Belfort-Montbeliard.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of the Laboratoire Systemes et Transports
 * of the Universite de Technologie de Belfort-Montbeliard ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with the SeT.
 *
 * http://www.multiagent.fr/
 */

package be.uhasselt.imob.carpooling.datainput;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.vecmath.Point2d;

import org.arakhne.afc.progress.Progression;
import org.arakhne.afc.util.OutputParameter;
import org.arakhne.afc.vmutil.FileSystem;
import org.arakhne.afc.vmutil.locale.Locale;

import be.uhasselt.imob.carpooling.util.SimulationParameters;
import fr.utbm.set.attr.AttributeProvider;
import fr.utbm.set.geom.bounds.bounds2d.Bounds2D;
import fr.utbm.set.geom.bounds.bounds2d.MinimumBoundingRectangle;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.gis.io.shape.GISShapeFileReader;
import fr.utbm.set.gis.mapelement.MapElement;
import fr.utbm.set.gis.road.RoadPolyline;
import fr.utbm.set.gis.road.StandardRoadNetwork;
import fr.utbm.set.gis.road.primitive.RoadNetwork;
import fr.utbm.set.gis.road.primitive.RoadSegment;
import fr.utbm.set.io.dbase.DBaseFileFilter;
import fr.utbm.set.io.dbase.DBaseFileReader;
import fr.utbm.set.io.shape.ESRIBounds;

/** Read data for the Carpooling simulation.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public abstract class DataInput {

	/** Compute a random position on a road network.
	 * 
	 * @param roadNetwork
	 * @return the position, never <code>null</code>.
	 */
	protected static Point1D5 randomPosition1d5(RoadNetwork roadNetwork) {
		Point1D5 p = roadNetwork.getNearestPosition(randomPosition2d(roadNetwork));
		if (p==null || p.getSegment()==null) throw new IllegalStateException();
		return p;
	}
	
	/** Compute a random position on a road network.
	 * 
	 * @param roadNetwork
	 * @return the position, never <code>null</code>.
	 */
	protected static Point2d randomPosition2d(RoadNetwork roadNetwork) {
		Bounds2D b = roadNetwork.getBoundingBox();
		double x = b.getMinX() + Math.random() * b.getSizeX();
		double y = b.getMinY() + Math.random() * b.getSizeY();
		return new Point2d(x, y);
	}

	/**
	 */
	public DataInput() {
		//
	}
	
	/** Read the data.
	 * 
	 * @param parameters are the simulation parameters.
	 * @param progress permits to notify about the reading progression.
	 * @return the descriptions extracted from the data set.
	 * @throws IOException
	 */
	public DataSet read(SimulationParameters parameters, Progression progress) throws IOException {
		Progression sp;
		if (progress==null) {
			sp = null;
		}
		else {
			progress.setProperties(0, 0, 100000, false);
			sp = progress.subTask(50000);
		}
		// Read the roads
		StandardRoadNetwork roadNetwork = readRoads(parameters, sp);
		assert(roadNetwork!=null);
		if (progress!=null) progress.ensureNoSubTask();
		// Read the population
		List<IndividualDailyDescription> population = new ArrayList<IndividualDailyDescription>();
		OutputParameter<GregorianCalendar> startDate = new OutputParameter<GregorianCalendar>();
		OutputParameter<GregorianCalendar> endDate = new OutputParameter<GregorianCalendar>();
		sp = (progress==null) ? null : progress.subTask(50000);
		readPopulation(parameters, roadNetwork, population, startDate, endDate, sp);
		if (progress!=null) progress.ensureNoSubTask();
		
		GregorianCalendar s = startDate.get();
		GregorianCalendar e = endDate.get();
		assert(s!=null);
		assert(e!=null);
		return new DataSet(
				s,
				e,
				roadNetwork,
				population);
	}
	
	/** Read the population data set.
	 * 
	 * @param parameters are the simulation parameters.
	 * @param roadNetwork is the road network of the environment related to the data.
	 * @param population is the list to fill with the population data.
	 * @param startDate is the start date extracted from the data set. 
	 * @param endDate is the end date extracted from the data set. 
	 * @param progress permits to notify about the reading progression.
	 * @throws IOException
	 */
	protected abstract void readPopulation(
			SimulationParameters parameters,
			StandardRoadNetwork roadNetwork,
			List<IndividualDailyDescription> population,
			OutputParameter<GregorianCalendar> startDate,
			OutputParameter<GregorianCalendar> endDate,
			Progression progress) throws IOException;
	
	private static StandardRoadNetwork readRoads(SimulationParameters parameters, Progression progress) throws IOException {
		StandardRoadNetwork roadNetwork;
		URL shapeFile = parameters.getRoadNetwork();
		if (shapeFile==null) throw new IOException(Locale.getString("NO_ROAD_NETWORK")); //$NON-NLS-1$
		System.out.println("roadNetworkFile: " + shapeFile.toString());
		DBaseFileReader dbfFileReader = null;
		if (parameters.getReadDBase()) {
			URL dbaseFile = FileSystem.replaceExtension(shapeFile, DBaseFileFilter.DEFAULT_EXTENSION);
			try {
				dbfFileReader = new DBaseFileReader(dbaseFile);
			}
			catch(Throwable _) {
				//
			}
		}
		GISShapeFileReader reader;
		if (dbfFileReader!=null) {
			reader = new GISShapeFileReader(shapeFile, RoadPolyline.class, dbfFileReader);
		}
		else {
			reader = new GISShapeFileReader(shapeFile, RoadPolyline.class);
		}
		try {
			ESRIBounds esriBounds = reader.getBoundsFromHeader();
			Bounds2D bounds = new MinimumBoundingRectangle(
					esriBounds.getMinX(),
					esriBounds.getMinY(),
					esriBounds.getMaxX(),
					esriBounds.getMaxY());
			roadNetwork = new StandardRoadNetwork(bounds);
			progress.setComment(Locale.getString("READING", FileSystem.basename(shapeFile))); //$NON-NLS-1$
			progress.setMaximum(reader.getFileSize());
			reader.setTaskProgression(progress);
			MapElement element;
			while ((element = reader.read())!=null) {
				if (element instanceof RoadSegment) {
					RoadSegment relem = (RoadSegment) element;
					//relem Set attributes Here.
					relem.setAttribute("segmentSpeed", 10);
					roadNetwork.addRoadSegment(relem);
				}
			}
		}
		finally {
			reader.close();
		}
		return roadNetwork;
	}

}
