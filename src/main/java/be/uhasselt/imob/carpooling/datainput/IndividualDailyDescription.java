/* 
 * $Id$
 * 
 * Copyright (c) 2013, Multiagent Team,
 * Laboratoire Systemes et Transports,
 * Institut de Recherche sur le Transport, l'Energie et la Societe,
 * Universite de Technologie de Belfort-Montbeliard.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of the Laboratoire Systemes et Transports
 * of the Universite de Technologie de Belfort-Montbeliard ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with the SeT.
 *
 * http://www.multiagent.fr/
 */

package be.uhasselt.imob.carpooling.datainput;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import fr.utbm.set.geom.object.Point1D5;
import be.uhasselt.imob.carpooling.kernel.time.DayOfWeek;
import be.uhasselt.imob.carpooling.schedule.Trip;
import be.uhasselt.imob.carpooling.util.Gender;

/** Description of an individual for a given day
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class IndividualDailyDescription implements Serializable {

	private static final long serialVersionUID = 3852632783827788245L;
	
	/** Name.
	 */
	public final String name;
	
	/** Gender.
	 */
	public final Gender gender;
	
	/** Age.
	 */
	public final int age;
	
	/** Indicates if the individual has a car driving license.
	 */
	public final boolean isDriver;
	
	/** Indicates if the individual has a car.
	 */
	public final boolean hasCar;

	/** List of the periodic trips of this individual.
	 */
	public final List<Trip> trips;

	/** Day of the week of the trips.
	 */
	DayOfWeek day = null;
	
	/** Total traveling distance for the day of the week.
	 */
	float totalDistance = 0f;
	
	/** Last position for the day of the week.
	 */
	Point1D5 firstPosition = null;

	/** Last position for the day of the week.
	 */
	Point1D5 lastPosition = null;

	/**
	 * @param name
	 * @param gender
	 * @param age
	 * @param isDriver
	 * @param hasCar
	 */
	public IndividualDailyDescription(String name, Gender gender, int age, boolean isDriver, boolean hasCar) {
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.isDriver = isDriver;
		this.hasCar = hasCar;
		this.trips = new LinkedList<Trip>();
	}
	
	/** Replies the day of the way.
	 * 
	 * @return the day of the trips.
	 */
	public DayOfWeek getDayOfWeek() {
		return this.day;
	}
	
	/** Replies the total travelling distance for the day.
	 *
	 * @return the travelling distance for the day.
	 */
	public float getTotalTravellingDistance() {
		return this.totalDistance;
	}
	
	/** Replies the first position during the day.
	 * 
	 * @return the first position during the day.
	 */
	public Point1D5 getFirstPosition() {
		return this.firstPosition;
	}

	/** Replies the last position during the day.
	 * 
	 * @return the last position during the day.
	 */
	public Point1D5 getLastPosition() {
		return this.lastPosition;
	}

}
