/* 
 * $Id$
 * 
 * Copyright (c) 2013, Multiagent Team,
 * Laboratoire Systemes et Transports,
 * Institut de Recherche sur le Transport, l'Energie et la Societe,
 * Universite de Technologie de Belfort-Montbeliard.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of the Laboratoire Systemes et Transports
 * of the Universite de Technologie de Belfort-Montbeliard ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with the SeT.
 *
 * http://www.multiagent.fr/
 */

package be.uhasselt.imob.carpooling.datainput;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.vecmath.Point2d;
import javax.vecmath.Point2i;

import org.arakhne.afc.progress.Progression;
import org.arakhne.afc.util.OutputParameter;

import be.uhasselt.imob.carpooling.environment.Location;
import be.uhasselt.imob.carpooling.kernel.time.DayOfWeek;
import be.uhasselt.imob.carpooling.kernel.time.Time;
import be.uhasselt.imob.carpooling.schedule.Trip;
import be.uhasselt.imob.carpooling.schedule.TripComponent;
import be.uhasselt.imob.carpooling.schedule.TripMode;
import be.uhasselt.imob.carpooling.util.Gender;
import be.uhasselt.imob.carpooling.util.SimulationParameters;
import fr.utbm.set.geom.bounds.bounds2d.Bounds2D;
import fr.utbm.set.geom.object.Point1D5;
import fr.utbm.set.gis.road.StandardRoadNetwork;
import fr.utbm.set.gis.road.path.RoadPath;
import fr.utbm.set.gis.road.path.astar.RoadAStar;
import fr.utbm.set.math.MeasureUnitUtil;

/**
 * Read data for the Carpooling simulation with a generic data input model.
 * 
 * @author $Author: sgalland$
 * @version $Name$ $Revision$ $Date$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class GenericDataInput extends DataInput {

    private static final float CELL_SIZE = 500f;
    private static final float DEMI_CELL_SIZE = CELL_SIZE / 2f;

    private static final int MAX_WORK_CELLS = 10;
    
    private static final float CAR_OWNERSHIP_PROBABILITY = .9f;
    private static final float DRIVER_LICENSE_PROBABILITY = .95f;
    
    /**
	 */
    public GenericDataInput() {
        //
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void readPopulation(final SimulationParameters parameters, final StandardRoadNetwork roadNetwork,
            final List<IndividualDailyDescription> population, final OutputParameter<GregorianCalendar> startDate,
            final OutputParameter<GregorianCalendar> endDate, final Progression progress)
            throws IOException {
        final Random random = new Random();

        // Create the cells on the map
        final Bounds2D box = roadNetwork.getBoundingBox();
        int width = (int) (box.getSizeX() / CELL_SIZE);
        int height = (int) (box.getSizeY() / CELL_SIZE);
        Point1D5[][] centroids = new Point1D5[width][height];
        for (int x = 0; x < width; ++x) {
            float cx = (float) box.getMinX() + x * CELL_SIZE + DEMI_CELL_SIZE;
            for (int y = 0; y < height; ++y) {
                float cy = (float) box.getMinY() + y * CELL_SIZE + DEMI_CELL_SIZE;
                Point1D5 p = roadNetwork.getNearestPosition(new Point2d(cx, cy));
                if (p != null) {
                    centroids[x][y] = p;
                }
            }
        }

        // Select the work cells
        final List<Point2i> workCells = new ArrayList<Point2i>(MAX_WORK_CELLS);
        for (int i = 0; i < MAX_WORK_CELLS; ++i) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            workCells.add(new Point2i(x, y));
        }

        if (progress != null) {
            progress.setProperties(0, 0, parameters.getPopulationSize(), false);
        }
        
        ThreadPoolExecutor executor = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(), Runtime.getRuntime()
                .availableProcessors(), 1, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        
        for (int j = 0; j < parameters.getPopulationSize(); ++j) {
            final int i = j;

            executor.execute(new Runnable() {
                @Override
                public void run() {
                    RoadPath path1, path2;
                    Time time1, time2;

                    String name = "Carpooler #" + i; //$NON-NLS-1$
                    Gender gender = random.nextBoolean() ? Gender.MALE : Gender.FEMALE;
                    int age = random.nextInt(40) + 20;
                    boolean hasDriverLicense = random.nextFloat() <= DRIVER_LICENSE_PROBABILITY;
                    boolean hasCar;
                    if (hasDriverLicense)
                    	hasCar = random.nextFloat() <= CAR_OWNERSHIP_PROBABILITY;
                    else
                    	hasCar = false;

                    Point2d work, home;
                    do {
	                    int wCell = random.nextInt(MAX_WORK_CELLS);
	                    work = randomPosition(box, workCells.get(wCell), random);
	
	                    int tests = 0;
	                    do {
	                        home = randomPosition2d(roadNetwork);
	                        RoadAStar astar = new RoadAStar();
	                        path1 = astar.solve(home, work, roadNetwork);
	                        ++tests;
	                    } while (path1 == null && tests<5);
                    }
                    while(path1==null);

                    Point1D5 home1d5 = roadNetwork.getNearestPosition(home);
                    Point1D5 work1d5 = roadNetwork.getNearestPosition(work);

                    assert (home1d5.getSegment().equals(path1.get(0))) : "Path not starting at home"; //$NON-NLS-1$
                    assert (work1d5.getSegment().equals(path1.get(path1.size() - 1))) : "Path not ending at work"; //$NON-NLS-1$

                    RoadAStar astar = new RoadAStar();
                    path2 = astar.solve(work, home, roadNetwork);
                    if (path2==null) {
                    	path2 = path1.clone();
                    	path2.invert();
                    }
                    
                    assert (work1d5.getSegment().equals(path2.get(0))) : "Path not starting at work"; //$NON-NLS-1$
                    assert (home1d5.getSegment().equals(path2.get(path1.size() - 1))) : "Path not ending at home"; //$NON-NLS-1$

                    Location homeLocation = new Location(home1d5);
                    Location workLocation = new Location(work1d5);

                    DayOfWeek currentDay = DayOfWeek.MONDAY;

                    for (int id = 0; id < parameters.getNumberOfDays(); ++id) {
                        IndividualDailyDescription data = new IndividualDailyDescription(name, gender, age, hasDriverLicense, hasCar);

                        data.firstPosition = homeLocation.getGISPosition();
                        data.lastPosition = homeLocation.getGISPosition();
                        data.day = currentDay;
                        data.totalDistance = (float) (path1.getLength() + path2.getLength());

                        // Morning travel
                        time1 = randomTime(random, currentDay, 8, 60);

                        time2 = moveTime(random, time1, path1.getLength(), parameters.getCruiseSpeed(), 10);

                        Trip trip = new Trip();
                        TripComponent tripComponent = new TripComponent(homeLocation, workLocation, time1, time2, TripMode.CAR, path1);
                        trip.add(tripComponent);
                        data.trips.add(trip);
                        updateDates(time1, time2, startDate, endDate);

                        // Evening travel
                        time1 = randomTime(random, currentDay, 17, 60);

                        time2 = moveTime(random, time1, path1.getLength(), parameters.getCruiseSpeed(), 10);

                        trip = new Trip();
                        tripComponent = new TripComponent(workLocation, homeLocation, time1, time2, TripMode.CAR, path2);
                        trip.add(tripComponent);
                        data.trips.add(trip);
                        updateDates(time1, time2, startDate, endDate);

                        synchronized (GenericDataInput.this) {
                            population.add(data);
                        }

                        currentDay = currentDay.next();
                    }

                    synchronized (GenericDataInput.this) {
                        if (progress != null) {
                            progress.increment("Creating individual #"+i+"/"+parameters.getPopulationSize()); //$NON-NLS-1$ //$NON-NLS-2$
                        }
                    }
                }
            });
        }
        
        executor.shutdown();

        boolean finished = false;
        do {
            try {
                finished = executor.awaitTermination(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (!finished);

        assert (parameters.getPopulationSize() * parameters.getNumberOfDays() == population.size());

        if (progress != null)
            progress.end();
    }

    private static Time randomTime(Random random, DayOfWeek day, int h, int deviation) {
        Time t = new Time(day, h, 0);
        return t.add((int) (random.nextGaussian() * deviation));
    }

    private static Time moveTime(Random random, Time t, double length, double speed, double deviation) {
        double ms = MeasureUnitUtil.kmh2ms(speed);
        double duration = length / ms;
        duration += random.nextGaussian() * (deviation * 60);
        duration = Math.max(30., duration);
        return t.add((int) (duration / 60));
    }

    private static Point2d randomPosition(Bounds2D box, Point2i p, Random random) {
        float x = (float) box.getMinX() + p.x * CELL_SIZE + random.nextFloat() * CELL_SIZE;
        float y = (float) box.getMinY() + p.y * CELL_SIZE + random.nextFloat() * CELL_SIZE;
        return new Point2d(x, y);
    }

    private static void updateDates(
            Time a, Time b,
            OutputParameter<GregorianCalendar> startDate,
            OutputParameter<GregorianCalendar> endDate) {
        GregorianCalendar c;
        c = startDate.get();
        if (c == null || a.toCalendar().before(c)) {
            startDate.set(a.toCalendar());
        }
        c = endDate.get();
        if (c == null || b.toCalendar().after(c)) {
            endDate.set(b.toCalendar());
        }
    }

}
